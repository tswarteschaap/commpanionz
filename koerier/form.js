$(document).ready(function() {

	// Input voor kenmerk wordt automatisch geselecteerd bij het starten van de pagina. 
	// Er kan direct een sessiecode worden gescand, zonder dat het veld eerst handmatig wordt geselecteerd. 
	var kenmerkinput = document.getElementById('sessiekenmerk');
	kenmerkinput.focus();
	kenmerkinput.select();


	var scanId = 0;
	var sessieObject = '';

	// Als het sessieveld wordt aangeklikt, wordt meteen alle tekst in het veld wordt geselecteerd
	// Binnen één klik kan dus een nieuwe sessie worden gescand
	$("#sessiekenmerk").click(function(){
		$(this).select();
	})

	//AJAX REQUEST VOOR CHECK OF DE KOERIER HEEFT GESCAND + VAR VOOR OPBOUW ARRAY (getMonsterScans.php)

	// Functie met AJAX request om barcode te versturen. Hierin wordt o.a. informatie opgehaald over de route en praktijk die bij de barcode hoort.
	var verstuurScan = function(){
		$.ajax({
			url: 'https://bvobaarmoederhals.nl/koerier/setMonsterScans.php',
			dataType: 'json',
			data: 'scan=' + $('#scan').val() + '&sessiekenmerk=' + $('#sessiekenmerk').val(),
			method: 'POST',
			success: function(response) {
				console.log(response.response);
				var response = response.response
				if(response == "Barcode ingevoerd"){
					$("#succesmelding").show(0).delay(500).hide(0);
				} else {
					swal({ title:'Let op!', text: "Deze barcode bestaat al in de database!"});
				}
			},
			error: function(response) {

			}
		});
	}


	// Informatie van barcode wordt opgehaald
	$('#stuurscan').click(function() {
		var lengteBarcode = $("#scan").val().length
		console.log(lengteBarcode);

		if(lengteBarcode == 11 || lengteBarcode == 12 || lengteBarcode == 15){
			verstuurScan();
		}else if(lengteBarcode == 0){
			swal({ title:'Geen geldige barcode', text: 'Er is geen barcode gescand.'});
		}else{
			swal({ title:'Geen geldige barcode', text: 'De gescande barcode is niet geldig.'});
		}
	});


	// BARCODE ACTIEF
	// Styles en attributen worden aangepast om te benadrukken dat er een barcode moet worden ingevuld
	barcodeActief = function(){
		// Barcode input wordt ontgrendeld
		document.getElementById("scan").disabled = false;

		// Tooltip bij kenmerk wordt inactief, tooltip bij barcode wordt actief
		$("#barcode-tooltip").addClass('active');
		$("#kenmerk-tooltip").removeClass('active');

		// Stap 1 wordt inactief, stap 2 & 3 worden actief
		$("#stap1").removeClass('stap-active');
		$("#stap2").addClass('stap-active');
		$("#stap3").addClass('stap-active');

		// Naam van sessie boven veld wordt actief 
		$("#sessieKenmerkTitel").addClass('active');

		// Handmatige verzendknop wordt actief
		$(".sendIcon").attr("data-balloon", "Geen geldige barcode gescand");
		$(".sendIcon").addClass("active");

		// Notitie onder barcode wordt actief
		$(".barcode-notitie").addClass("active");
	}

	// BARCODE INACTIEF
	// Styles worden aangepast om te benadrukken dat er een (geldige) barcode moet worden ingevuld
	barcodeInactief = function(){

		// Barcode input wordt vergrendeld
		document.getElementById("scan").disabled = true;

		// Tooltip bij kenmerk wordt actief, tooltip bij barcode wordt inactief
		$("#barcode-tooltip").removeClass('active');
		$("#kenmerk-tooltip").addClass('active');

		// Stap 1 wordt actief, stap 2 & 3 worden inactief
		$("#stap1").addClass('stap-active');
		$("#stap2").removeClass('stap-active');
		$("#stap3").removeClass('stap-active');

		// Naam van sessie boven veld wordt inactief
		$("#sessieKenmerkTitel").removeClass('active');

		// Handmatige verzendknop wordt inactief
		$(".sendIcon").attr("data-balloon", "Geen sessie gescand");
		$(".sendIcon").removeClass("active");

		// Notitie onder barcode wordt inactief
		$(".barcode-notitie").removeClass("active");
	}



	// EVENT LISTENERS
	// Functie wordt uitgevoerd wanneer de inhoud van het sessieveld verandert, dus na iedere nieuwe letter.
	document.getElementById("sessiekenmerk").addEventListener('input', function(evt){

		var sessiekenmerkLock =  $("#sessiekenmerk").val();
		// console.log(sessiekenmerkLock.length);


		// Wanneer er minder dan 4 tekens in het sessieveld staan, wordt het barcode veld uitgeschakeld.
		if (sessiekenmerkLock.length > 4){

			// Sessiekenmerk moet prefix SES bevatten
			if(sessiekenmerkLock.toLowerCase().startsWith('ses ')){
				barcodeActief();
				// var sessie = sessiekenmerkLock.toUpperCase().replace("SES", "");
				var sessie = sessiekenmerkLock.toUpperCase();
				$("#sessieKenmerkTitel").text(sessie); 

				// Tooltip dat code niet geldig is wordt inactief
				$("#error-tooltip").removeClass('active');

			}else{
				barcodeInactief();
				$("#sessieKenmerkTitel").text('SESSIE'); 

				// Tooltip dat code niet geldig is wordt actief
				$("#error-tooltip").addClass('active');
			}

		}
		else{
			barcodeInactief();
			$("#sessieKenmerkTitel").text('SESSIE'); 

			// Tooltip dat code niet geldig is wordt inactief
			$("#error-tooltip").removeClass('active');	
		}

	})


	// Tekenteller houdt aantal tekens in veld bij
	var tekenteller = 0;

	// Functie wordt uitgevoerd wanneer de inhoud van het barcode veld verandert, dus na iedere nieuwe letter.
	document.getElementById("scan").addEventListener('input', function (evt) {
	    
	    // Tekenteller wordt gelijk gesteld aan het aantal tekens in het veld
		tekenteller = $("#scan").val().length;
		// console.log(tekenteller);



		// Controle of het gaat om een code van PostNL. Deze beginnen namelijk met 2S, 3S, KG 
		var value = $("#scan").val();
		var x = value.startsWith("2S", 0);
		var y = value.startsWith("3S", 0);
		var z = value.startsWith("KG", 0);
    
    // Controle of het een tijdelijke TSS code is of een BVO zakje
    var tssCode = value.startsWith("TSS", 0);
    
		// Als het gaat om een PostNL code, wordt de code na 15 karakters verwerkt, omdat deze codes 15 karakters lang zijn.
		// In alle andere gevallen wordt de code na 12 karakters versuurd. De barcodes van Commpanionz moeten dus ALTIJD 12 karakters bevatten.
		if(x === true || y === true || z === true){
			if (tekenteller == 15){
				countAction();
			}
		} else {
      if (tssCode == true) {
        if (tekenteller == 12) {
          countAction();
        }
      }else{
        if (tekenteller == 11) {
          countAction();
        }
      }
			
		}    

	});


	// 
	function countAction(){

		// Informatie van barcode wordt opgehaald
		verstuurScan();

		// Barcode input wordt leeggemaakt
		document.getElementById("scan").value = '';

		// Tekenteller wordt gereset. Volgende code begint dus weer bij 0 te tellen
		tekenteller = 0;
	}

});