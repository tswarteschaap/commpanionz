$(document).ready(function() {

	// Input voor kenmerk wordt automatisch geselecteerd bij het starten van de pagina. 
	// Er kan direct een sessiecode worden gescand, zonder dat het veld eerst handmatig wordt geselecteerd. 
	var kenmerkinput = document.getElementById('sessiekenmerk');
	kenmerkinput.focus();
	kenmerkinput.select();

	var amountScanned = 0;


	var scanId = 0;
	var sessieObject = '';

	// Als het sessieveld wordt aangeklikt, wordt meteen alle tekst in het veld wordt geselecteerd
	// Binnen één klik kan dus een nieuwe sessie worden gescand
	$("#sessiekenmerk").click(function(){
		$(this).select();
	})

	//AJAX REQUEST VOOR CHECK OF DE KOERIER HEEFT GESCAND + VAR VOOR OPBOUW ARRAY (getMonsterScans.php)

	// Functie met AJAX request om barcode te versturen. Hierin wordt o.a. informatie opgehaald over de route en praktijk die bij de barcode hoort.
	var verstuurScan = function(){
		$.ajax({
			url: 'https://bvobaarmoederhals.nl/dev_scan/getMonsterScans.php',
			dataType: 'json',
			data: 'scan=' + $('#scan').val() + '&sessiekenmerk=' + $('#sessiekenmerk').val(),
			method: 'POST',
			success: function(response) {
				if(response.response == 'missend'){
					swal({ title:'Let op', icon: 'error', text: 'Deze barcode komt niet voor in de database! Gelieve direct contact op te nemen via +31 341 495 254'}).then((value) => {focusScan()});
				} else if(response.response['bestaat']) {
					var bestaat_kenmerk = response.response['bestaat'];
					var bestaat_barcode = response.response['bestaat_barcode'];
					// alert('bestaat, maar met ander sessiekenmerk' + bestaat_kenmerk);
					
					swal({
						icon: 'info',
						title: 'Let op!',
						text: 'Het monster bestaat al in de database, maar onder een ander sessiekenmerk: ' + bestaat_kenmerk + '. Wilt u het sessiekenmerk aanpassen?',
					  	buttons: {
						    cancel: 'nee',
						    confirm: 'ja, pas het sessiekenmerk aan'
					  	}
					}).then((value) => {
						focusScan();
						if(value === true){
		  					$.ajax({
								url: 'https://bvobaarmoederhals.nl/dev_scan/getMonsterScans.php',
								dataType: 'json',
								data: 'nieuwesessie=' + $('#sessiekenmerk').val() + '&bestaat_barcode=' + bestaat_barcode,
								method: 'POST',
								success: function(response) {
									
									$("#succesmelding").show(0).delay(500).hide(0);
									var route_id = response.response['route_id'];
									var bar_code = response.response['bar_code'];
									var praktijk_id = response.response['praktijk_id'];
									var sessie_kenmerk = response.response['sessie_kenmerk'];
									var scanned_result = {route_id, bar_code, praktijk_id, sessie_kenmerk}
									
									var lengteSessieObject = Object.keys(sessieObject).length;

									// DELETE BARCODE OBJECT IF IT OCCURS IN OBJECT LIST
									// for (i = 0; i < lengteSessieObject; i++){
									// 	if (sessieObject[i]['bar_code'] == bar_code ){
									// 		delete sessieObject[i];
									// 		console.log('hij heeft hem gepakt');
									// 	}
									// }			


									// sessie.push(scanned_result);
									sessieObject[scanId] = scanned_result;
									scanId += 1;
									
									addScanToList(bar_code, sessie_kenmerk);
									console.log(sessieObject);
									

								},
								error: function(response) {
									console.log(response);
									alert('Er is iets misgegaan.');
								}
							});
						} else {
							swal({ title:'Let op', icon: 'error', text: 'De barcode is niet aan de sessie toegevoegd aangezien deze afwijkt in de sessiebarcode!'}).then((value) => {focusScan()});
						}
					});


				} else {
				$("#succesmelding").show(0).delay(500).hide(0);
				var route_id = response.response['route_id'];
				var bar_code = response.response['bar_code'];
				var praktijk_id = response.response['praktijk_id'];
				var sessie_kenmerk = response.response['sessie_kenmerk'];
				var scanned_result = {route_id, bar_code, praktijk_id, sessie_kenmerk}

				// CONTROLE DUBBELE SCAN (BARCODE PLUS SESSIEKENMERK MATCHEN)
				var lengteSessieObject = Object.keys(sessieObject).length;
				var securityCount = 0;

				for (i = 0; i < lengteSessieObject; i++){
					if ((sessieObject[i]['bar_code'] == bar_code) && (sessieObject[i]['sessie_kenmerk'] == sessie_kenmerk)){
						securityCount++;
						swal({
							title: "Let op",
							icon: "error",
							text: "Deze barcode is reeds gescand!"
						});
					}
				}
				
				if(securityCount == 0){
					sessieObject[scanId] = scanned_result;
					scanId += 1;
					addScanToList(bar_code, sessie_kenmerk);
					console.log(sessieObject);
				}
				
				// console.log(sessieObject);
			}
			
		},
		error: function(response) {
			console.log(response);
		}
	});
}


	//AJAX REQUEST VOOR VERSTUREN GEMAAKTE ARRAY -> DEZE WORDT GELOOPT VOOR LAATSTE CONTROLE (loopMonsterScans.php)

	// Functie met AJAX request om alle informatie uit de sessie te versturen.
	// Hier wordt controle uitgevoerd of de barcodes met elkaar overeenkomen en wordt een mail gestuurd naar beheerders indien er fouten worden ontdekt.
	var beeindigSessie = function(){
		// console.log(sessieObject);
		if(sessieObject.length != ''){

			swal({
				icon: 'info',
				title: 'Let op!',
				text: 'Het is belangrijk dat ieder ontvangen monsters wordt gescand. Weet u zeker dat alle monsters zijn gescand?',
			  	buttons: {
				    cancel: 'nee',
				    confirm: 'ja, beëindig sessie'
			  	}
			}).then((value) => {
				focusScan();
				if(value === true){
  					$.ajax({
						url: 'https://bvobaarmoederhals.nl/dev_scan/loopMonsterScans.php',
						dataType: 'json',
						data: 'sessie=' + JSON.stringify(sessieObject),
						method: 'POST',
						success: function(response) {
							// console.log(response);
							// alert(response.response);
							swal({ title:'Sessie beëindigd', text: response.response});

						},
						error: function(response) {
							console.log(response);
							alert('Er is iets misgegaan.');
						}
					});
				}
			});

		} else {
			swal({ title:'Let op', icon: 'error', text: 'U heeft nog geen barcode gescand.'}).then((value) => {focusScan()});
		}
	}


	// Informatie van barcode wordt opgehaald
	$('#stuurscan').click(function() {
		var lengteBarcode = $("#scan").val().length
		console.log(lengteBarcode);

		if(lengteBarcode == 11 || lengteBarcode == 12 || lengteBarcode == 15){
			verstuurScan();
		}else if(lengteBarcode == 0){
			swal({ title:'Geen geldige barcode', text: 'Er is geen barcode gescand.'});
		}else{
			swal({ title:'Geen geldige barcode', text: 'De gescande barcode is niet geldig.'});
		}
	});


	// Sessie wordt beëindigt
	$('#eindesessie').click(function() {
		beeindigSessie();
	});

	$('#stuurscan').hover(function() {
		var lengteBarcode = $('#scan').val().length;

		if(lengtebarcode == 11 || lengteBarcode == 12 || lengteBarcode == 15){
			$('.sendIcon').attr("data-balloon", "Gebruik deze knop alleen wanneer de barcode niet vanzelf wordt verstuurd");
			$('.sendIcon').addClass("valid");
		}else{
			$('.sendIcon').attr("data-balloon", "Geen geldige barcode gescand");
			$('.sendIcon').removeClass("valid");
		}
	})




	// SESSIE ARRAY (DEZE WORDT OPGEBOUWD DOOR MIDDEL VAN DE SCANS)
	// var sessie = [];
	var sessieObject = new Object();



	// BARCODE ACTIEF
	// Styles en attributen worden aangepast om te benadrukken dat er een barcode moet worden ingevuld
	barcodeActief = function(){
		// Barcode input wordt ontgrendeld
		document.getElementById("scan").disabled = false;

		// Tooltip bij kenmerk wordt inactief, tooltip bij barcode wordt actief
		$("#barcode-tooltip").addClass('active');
		$("#kenmerk-tooltip").removeClass('active');

		// Stap 1 wordt inactief, stap 2 & 3 worden actief
		$("#stap1").removeClass('stap-active');
		$("#stap2").addClass('stap-active');
		$("#stap3").addClass('stap-active');

		// Naam van sessie boven veld wordt actief 
		$("#sessieKenmerkTitel").addClass('active');

		// Handmatige verzendknop wordt actief
		$(".sendIcon").attr("data-balloon", "Geen geldige barcode gescand");
		$(".sendIcon").addClass("active");

		// Notitie onder barcode wordt actief
		$(".barcode-notitie").addClass("active");
	}

	// BARCODE INACTIEF
	// Styles worden aangepast om te benadrukken dat er een (geldige) barcode moet worden ingevuld
	barcodeInactief = function(){

		// Barcode input wordt vergrendeld
		document.getElementById("scan").disabled = true;

		// Tooltip bij kenmerk wordt actief, tooltip bij barcode wordt inactief
		$("#barcode-tooltip").removeClass('active');
		$("#kenmerk-tooltip").addClass('active');

		// Stap 1 wordt actief, stap 2 & 3 worden inactief
		$("#stap1").addClass('stap-active');
		$("#stap2").removeClass('stap-active');
		$("#stap3").removeClass('stap-active');

		// Naam van sessie boven veld wordt inactief
		$("#sessieKenmerkTitel").removeClass('active');

		// Handmatige verzendknop wordt inactief
		$(".sendIcon").attr("data-balloon", "Geen sessie gescand");
		$(".sendIcon").removeClass("active");

		// Notitie onder barcode wordt inactief
		$(".barcode-notitie").removeClass("active");
	}



	// EVENT LISTENERS
	// Functie wordt uitgevoerd wanneer de inhoud van het sessieveld verandert, dus na iedere nieuwe letter.
	document.getElementById("sessiekenmerk").addEventListener('input', function(evt){

		var sessiekenmerkLock =  $("#sessiekenmerk").val();
		// console.log(sessiekenmerkLock.length);


		// Wanneer er minder dan 4 tekens in het sessieveld staan, wordt het barcode veld uitgeschakeld.
		if (sessiekenmerkLock.length > 4){

			// Sessiekenmerk moet prefix SES bevatten
			if(sessiekenmerkLock.toLowerCase().startsWith('ses ')){
				barcodeActief();
				// var sessie = sessiekenmerkLock.toUpperCase().replace("SES", "");
				var sessie = sessiekenmerkLock.toUpperCase();
				$("#sessieKenmerkTitel").text(sessie); 

				// Tooltip dat code niet geldig is wordt inactief
				$("#error-tooltip").removeClass('active');

			}else{
				barcodeInactief();
				$("#sessieKenmerkTitel").text('SESSIE'); 

				// Tooltip dat code niet geldig is wordt actief
				$("#error-tooltip").addClass('active');
			}

		}
		else{
			barcodeInactief();
			$("#sessieKenmerkTitel").text('SESSIE'); 

			// Tooltip dat code niet geldig is wordt inactief
			$("#error-tooltip").removeClass('active');	
		}

	})


	// Tekenteller houdt aantal tekens in veld bij
	var tekenteller = 0;

	// Functie wordt uitgevoerd wanneer de inhoud van het barcode veld verandert, dus na iedere nieuwe letter.
	document.getElementById("scan").addEventListener('input', function (evt) {
	    
	    // Tekenteller wordt gelijk gesteld aan het aantal tekens in het veld
		tekenteller = $("#scan").val().length;
		// console.log(tekenteller);



		// Controle of het gaat om een code van PostNL. Deze beginnen namelijk met 2S, 3S, KG 
		var value = $("#scan").val();
		var x = value.startsWith("2S", 0);
		var y = value.startsWith("3S", 0);
		var z = value.startsWith("KG", 0);
    
    // Controle of het een tijdelijke TSS code is of een BVO zakje
    var tssCode = value.startsWith("TSS", 0);
    
		// Als het gaat om een PostNL code, wordt de code na 15 karakters verwerkt, omdat deze codes 15 karakters lang zijn.
		// In alle andere gevallen wordt de code na 12 karakters versuurd. De barcodes van Commpanionz moeten dus ALTIJD 12 karakters bevatten.
		if(x === true || y === true || z === true){
			if (tekenteller == 15){
				countAction();
			}
		} else {
      if (tssCode == true) {
        if (tekenteller == 12) {
          countAction();
        }
      }else{
        if (tekenteller == 11) {
          countAction();
        }
      }
			
		}	    

	});


	// Nieuwe scans worden toegevoegd aan de lijst
	function addScanToList(barcode, kenmerk){

		// Verwijdert placeholder 
		$(".listPlaceholder").css("display", "none");

		// Haalt tijd op voor element in lijst.
		// Uren, minuten en seconden worden eerst naar functie 'addZero' gestuurd, die leading zeroes toevoegt.
			var d = new Date();
			var hours = addZero(d.getHours());
			var minutes = addZero(d.getMinutes());
		var seconds = addZero(d.getSeconds());
		
		// Time is samengesteld als template string van alle losse getallen
		var time = `${hours}:${minutes}:${seconds}`;

		// Voegt daadwerkelijk element toe aan de lijst.
		// Variabelen worden ingevuld met template string
			$("#listScans").prepend(`
				<div class="scan">
					<h3>${barcode}</h3>
					<div class="listKenmerk">${kenmerk}</div>
					<div class="listTime">gescand om ${time}</div>
				</div>
			`);


		// Titel boven barcode lijst geeft aantal gescande barcodes weer
			amountScanned++;
			if (amountScanned == 1) {
				$(".listTitle").html(`${amountScanned} barcode gescand`);
			}else{
				$(".listTitle").html(`${amountScanned} barcodes gescand`);
			}

	}


	// Indien meegegeven variabel kleiner is dan 10, wordt er een 0 aan toegevoegd voor tijdsnotatie
	function addZero(n){
		if (n < 10) {
			time = `0${n}`;
		}else{
			time = n;
		}

		return time;
	}

  // Maakt het barcodeveld leeg en legt de focus op dit veld. 
  // Na het wegklikken van een melding kan direct een nieuwe code worden gescand.
	function focusScan(){
		$("#scan").val('');
		$("#scan").focus();
	}


	function countAction(){

		// Informatie van barcode wordt opgehaald
		verstuurScan();

		// Barcode input wordt leeggemaakt
		document.getElementById("scan").value = '';

		// Tekenteller wordt gereset. Volgende code begint dus weer bij 0 te tellen
		tekenteller = 0;
	}

});