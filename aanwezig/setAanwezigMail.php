<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////////// MAILFUNCTIE - AANWEZIGHEID ///////////////////
//BY MARTIJN WENNEKES & NIELS KERSIC, 'T SWARTE SCHAAP, HEERLEN, NL//
////////////////////////14-2-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/
/*
echo "hallo";
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
require_once 'includes/init.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

//datum ophalen
$mail = new PHPMailer(true); 
$GLOBALS['mail'] = $mail;
$date = date('Ymd');
$dateTomorrow = new DateTime('+1 day');
$dateWeekend = new DateTime('+3 days');
$dateTomorrowDisplay = $dateTomorrow->format('Ymd');
$dateWeekendDisplay = $dateWeekend->format('Ymd');

// 1 = maandag, 2 = dinsdag, 3 = woensdag, 4 = donderdag, 5 = vrijdag, 6 = zaterdag, 7 = zondag
$day = date('N');
$naamDag = 'morgen';
$datumAfspraak = $dateTomorrow->format('d-m-y');

//op basis van dag zullen de routes voor de vólgende dag worden aangesproken
switch($day){

	case 1: 
		$results = $db->query("SELECT * FROM routes WHERE region_id = '4' AND vaste_dag = 'dinsdag'");
		$datum = $dateTomorrowDisplay;
		$datum_email = 'DINSDAG';

	break;

	case 2: 
		$results = $db->query("SELECT * FROM routes WHERE region_id = '4' AND vaste_dag = 'woensdag'");
		$datum = $dateTomorrowDisplay;
		$datum_email = 'WOENSDAG';

	break;

	case 3: 
		$results = $db->query("SELECT * FROM routes WHERE region_id = '4' AND vaste_dag = 'donderdag'");
		$datum = $dateTomorrowDisplay;
		$datum_email = 'DONDERDAG';

	break;

	case 4: 
		$results = $db->query("SELECT * FROM routes WHERE region_id = '4' AND vaste_dag = 'vrijdag'");
		$datum = $dateTomorrowDisplay;
		$datum_email = 'VRIJDAG';

	break;

	case 5: 
		$results = $db->query("SELECT * FROM routes WHERE region_id = '4' AND vaste_dag = 'maandag'");
		$datum = $dateWeekendDisplay;
		$datum_email = 'MAANDAG';
		$naamDag = 'maandag';
		$datumAfspraak = $dateWeekend->format('d-m-y');


	break;
}

$num_rows = $results->rowCount();
$row = $results->fetchAll(PDO::FETCH_ASSOC);



foreach($row as $value){
	$route_id = $value['route_id'];
	$results = $db->query("SELECT user_id FROM user_routes WHERE route_id = '$route_id'");
	$num_rows2 = $results->rowCount();
	$row2 = $results->fetchAll(PDO::FETCH_ASSOC);
	foreach($row2 as $value){
		$standaardWaarde = '';	
		$praktijk_id = $value['user_id'];
		$results_praktijk = $db->query("SELECT email, standaardWaarde, identity, telephone FROM users WHERE user_id = '$praktijk_id'");
		$row_praktijk = $results_praktijk->fetchAll(PDO::FETCH_ASSOC);
		$standaardWaarde = $row_praktijk[0]['standaardWaarde'];
		$praktijk_email = $row_praktijk[0]['email'];
		$praktijk_naam = $row_praktijk[0]['identity'];
		$praktijk_telefoon = $row_praktijk[0]['telephone'];
		echo "
			<b>".$praktijk_naam." </b></br>
			Email: ".$praktijk_email." </br>
			Telefoon: ".$praktijk_telefoon." </br>
      standaardWaarde: ".$standaardWaarde." </br></br>
      

    ";
    

		if($standaardWaarde == "nee"){

      $mail->ClearAddresses(); // remove previous email addresses
          
      $results_emails = $db->query("SELECT email FROM praktijk_email WHERE user_id = '$praktijk_id' AND confirmation = '1'");
      $row_emails = $results_emails->fetchAll(PDO::FETCH_ASSOC);
      var_dump($row_emails);

      foreach($row_emails as $email) {
        $email_address = $email['email'];
        echo $email_address;
        $mail->addAddress($email_address);  
      }

			$linkVoorAanmelding = "https://www.bvobaarmoederhals.nl/aanwezig/?user-id=".$praktijk_id."&route-id=".$route_id."&datum=".$datum;
			$message = "Geachte dokter/assistente, <br/><br/>

			    					Graag vernemen wij van u of er <b>". $naamDag ." (" . $datumAfspraak . ")</b> een koerier langs moet komen om monsters op te halen voor het bevolkingsonderzoek baarmoederhalskanker. Als u monsters heeft om op te halen, bevestigt u dat hier:<br /><br /><br />


			    					<a href=" . $linkVoorAanmelding . " style='width:200px; height: 75px; padding:10px 20px; text-decoration: none; background-color: #009999; border-radius: 5px; color:#ffffff;'>BEVESTIG KOERIER</a>


			    					<br /><br /><br />
			    					Zonder bevestiging komt de koerier dus niet langs. <br/>
			    					U kunt uw keuze handmatig corrigeren door in te loggen op: <a href='https://bvobaarmoederhals.nl'>bvobaarmoederhals.nl</a> (dit emailadres is uw loginnaam en als u nog geen wachtwoord hebt, klikt u op 'wachtwoord vergeten');
			    					daarna volgt u de instructies op pagina 13 van de korte handleiding die als eerste verschijnt.<br /><br />

			  						<b>Indien de link niet automatisch opent:</b><br />
			  						Als de bovenstaande knop niet automatisch uw webbrowser opent, kunt u onderstaande link kopi&euml;ren en zelf in uw webbrowser plakken. <br /><span style='font-size: 12px'> " . $linkVoorAanmelding . "</span><br /><br />

			  						Met vriendelijke groet, <br />
			  						Team Commpanionz";
			try {
			    //Server settings
			    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
          


			    $mail->IsSMTP(); // telling the class to use SMTP
				//$mail->Host       = "mail.yourdomain.com"; // SMTP server
				$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
														   // 1 = errors and messages
														   // 2 = messages only
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
				$mail->Host       = "mail.antagonist.nl";      // sets GMAIL as the SMTP server
				$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "noreply@bvobaarmoederhals.nl";  // GMAIL username
				$mail->Password   = "0Myjmk";            // GMAIL password
				$mail->SetFrom('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				$mail->AddReplyTo("noreply@bvobaarmoederhals.nl","BVO Baarmoederhalskanker Mailer (Commpanionz)");
				$mail->Subject    = $subject;
				$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				// $mail->addAddress('niels@tswarteschaap.nl');
				$mail->addAddress(''.$praktijk_email.'');     // Add a recipient
			    // $mail->addAddress('test2@tswarteschaaptest.nl');     // Add a recipient
			    // $mail->addCC('cc@example.com');
			    // $mail->addBCC('bcc@example.com');

			    //Attachments
			    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    //Content
			    $mail->Subject = 'Uw koerier voor ' . $naamDag . ' (' . $datumAfspraak . ')';
			    $mail->Body    = 		    					 '<!DOCTYPE html>
					<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
					<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
					<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
					<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
					<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

					<title>Commpanionz mailer</title>
					<meta name="description" content="Commpanionz mailer">
					<meta name="keywords" content="">

					<!-- Mobile viewport -->
					<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

					<link rel="shortcut icon" href="images/favicon.ico"  type="image/x-icon">

					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/normalize.css">
					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/js/flexslider/flexslider.css">
					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/basic-style.css">

					<!-- end CSS-->
					    
					<!-- JS-->
					<script src="js/libs/modernizr-2.6.2.min.js"></script>
					<!-- end JS-->
					</style>
					</head>

					<body id="home">
					  
					<!-- header area -->
					    <header class="wrapper clearfix">
							       
					        <div id="banner">        
					        	<div id="logo"><img src="http://www.bvobaarmoederhals.nl/img/logo.png" alt="logo"></div> 
					        </div>
					  
					    </header><!-- end header -->
					 
					<!-- main content area -->   
					<div id="main" class="wrapper">
					    
					<!-- content area -->    
						<section id="content" class="wide-content">
					      <div class="row">	
					        
					            <p>'.$message.'</p>
					        
						  </div><!-- end row -->
						</section><!-- end content area -->   
					<!-- footer area -->    
					<footer>
						<div id="colophon" class="wrapper clearfix">
					    	Contactgegevens: Commpanionz - Zomerkade 102 - 1273 SP - Huizen - The Netherlands - Telefoon: +31 341 495 254 - Website: www.commpanionz.org - Mail: info@commpanionz.org
					    </div>
					</footer><!-- #end footer area --> 
					<!-- jQuery -->
					<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
					<script>window.jQuery || document.write("<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>")</script>

					<script defer src="js/flexslider/jquery.flexslider-min.js"></script>

					<!-- fire ups - read this file!  -->   
					<script src="js/main.js"></script>

					</body>
					</html>';
			    $mail->isHTML(true);                                  // Set email format to HTML
			    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
			    $mail->send();
			} 
			    catch (Exception $e) {
			}
		}
	}


}






?>