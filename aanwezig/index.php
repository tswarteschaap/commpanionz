<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////////// MAILFUNCTIE - AANWEZIGHEID ///////////////////
//BY MARTIJN WENNEKES & NIELS KERSIC, 'T SWARTE SCHAAP, HEERLEN, NL//
////////////////////////20-2-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/

// LINKT DATABASE
require_once 'includes/init.php';


// HAALT VARIABELEN UIT DE URL
$route_id = $_GET['route-id'];
$user_id = $_GET['user-id'];
$datum = $_GET['datum'];


// $weergeefDatum = $afspraakDatum->format('d-m-y');
// echo '$weergeefDatum';

// TELT LENGTE VAN VARIABELEN
$route_id_count = strlen($route_id);
$user_id_count = strlen($user_id);
$datum_count = strlen($datum);
$dag = '';

// GEEFT FOUTMELDING INDIEN ÉÉN OF MEER VARIABELEN NIET (CORRECT) IN DE URL AANWEZIG ZIJN
if(($route_id_count > 0) && ($user_id_count > 0) && ($datum_count > 0)){

	$sql = "INSERT INTO aanwezig (user_id, datum, afwezig) VALUES ('$user_id', '$datum', '0')";
	$aanwezigheid = $db->prepare($sql);
	$aanwezigheid->execute() or die(msql_error());

	$success = true;

	$afspraakDatum = DateTime::createFromFormat('Ymd', $datum);
	$weergeefDatum = $afspraakDatum->format('d-m-y');
	$dagVanWeek = $afspraakDatum->format('N');

	switch ($dagVanWeek) {
		// MAANDAG
	    case 1:
	        $dag = 'maandag';
	        break;

      	// DINSDAG
	    case 2:
	        $dag = 'dinsdag';
	        break;

        // WOENSDAG
	    case 3:
	        $dag = 'woensdag';
	        break;

        // DONDERDAG
	    case 4:
	        $dag = 'donderdag';
	        break;

        // VRIJDAG
	    case 5:
	        $dag = 'vrijdag';
	        break;
	}


	echo '
		<html>
		<head>
			<title>Aanwezigheid</title>

			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://bvobaarmoederhals.nl/aanwezig/style.css" rel="stylesheet">


		</head>
		<body>

			<div id="page-container">
				<main>
					<span class="top-icon">
						<i class="material-icons">check</i>
						<h1> Gelukt </h1>
					</span>

					<div class="card-content">

						U bent succesvol aangemeld voor <b>' . $dag . ' ' . $weergeefDatum . '</b><br />
						Deze pagina kan nu worden gesloten.
						<hr />
						<img src="https://bvobaarmoederhals.nl/staging/app/img/logo.png">
					</div>
				</main>
			</div>

		</body>
		</html>
	';


} else {

	echo '
		<html>
		<head>
			<title>Aanwezigheid</title>

			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<link href="https://bvobaarmoederhals.nl/aanwezig/style.css" rel="stylesheet">

		</head>
		<body>

			<div id="page-container">
				<main>
					<span class="top-icon">
						<i class="material-icons warn">report</i>
						<h1> Er ging iets fout </h1>
					</span>

					<div class="card-content">

						Er is iets mis gegaan tijdens het aanmelden. U kunt uw aanwezigheid <a href="https://bvobaarmoederhals.nl">hier</a> alsnog handmatig doorgeven.
						<hr />
						<img src="https://bvobaarmoederhals.nl/staging/app/img/logo.png">
					</div>
				</main>
			</div>

		</body>
		</html>
	';

}


?>

