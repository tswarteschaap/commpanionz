angular.module('myApp', ['ionic','myControllers','ui.router','ui.sortable','chart.js','angular-sticky-kit'])

.run(['$ionicPlatform', '$rootScope', function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    // org.apache.cordova.statusbar required
    if (cordova.platformId == 'android') {
        StatusBar.backgroundColorByHexString("#1abc9c");
    }

  });
}])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	
	$stateProvider

    .state('menu', {
      url: '/menu',
      templateUrl: 'templates/menu.html',
      controller: 'Menu',
      abstract: true
    })

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'Login'
    })

    .state('forgot-password', {
      url: '/forgot-password',
      templateUrl: 'templates/forgot-password.html',
      controller: 'forgotPassword'
    })

    .state('menu.planning', {
      url: '/planning',
      views: {
      	menuContent: {
      		templateUrl: 'templates/planning.html',
      		controller: 'Planning',
      	}
      }
    })

    .state('menu.route', {
      url: '/route',
      views: {
        menuContent: {
          templateUrl: 'templates/route.html',
          controller: 'Route',
        }
      }
    })

    .state('menu.instellingen', {
      url: '/instellingen',
      views: {
      	menuContent: {
      		templateUrl: 'templates/instellingen.html',
      		controller: 'Instellingen',
      	}
      }
    })

    .state('menu.user-details', {
      url: '/user/:userDetails',
      views: {
        menuContent: {
          templateUrl: 'templates/user-details.html',
          controller: 'Instellingen'
        }
      }
    })

    .state('menu.user-changes', {
      url: '/user/:userDetails/user-changes',
      views: {
        menuContent: {
          templateUrl: 'templates/user-changes.html',
          controller: 'Instellingen'
        }
      }
    })

    .state('menu.route-details', {
      url: '/route/:routeDetails',
      views: {
        menuContent: {
          templateUrl: 'templates/route-details.html',
          controller: 'Instellingen'
        }
      }
    })

    .state('menu.region-details', {
      url: '/regio/:regionDetails',
      views: {
        menuContent: {
          templateUrl: 'templates/region-details.html',
          controller: 'Instellingen'
        }
      }
    })

    .state('menu.register-koerier', {
      url: '/register-koerier',
      views: {
        menuContent: {
          templateUrl: 'templates/register-koerier.html',
          controller: 'Registreer',
        }
      }
    })

    .state('menu.register-huisartsenpraktijk', {
      url: '/register-huisartsenpraktijk',
      views: {
        menuContent: {
          templateUrl: 'templates/register-huisartsenpraktijk.html',
          controller: 'Registreer',
        }
      }
    })

    .state('menu.register-beheerder', {
      url: '/register-beheerder',
      views: {
        menuContent: {
          templateUrl: 'templates/register-beheerder.html',
          controller: 'Registreer',
        }
      }
    })

    .state('menu.register-laboratorium', {
      url: '/register-laboratorium',
      views: {
        menuContent: {
          templateUrl: 'templates/register-laboratorium.html',
          controller: 'Registreer',
        }
      }
    })

    .state('menu.messages', {
      url: '/messages',
      views: {
        menuContent: {
          templateUrl: 'templates/messages.html',
          controller: 'Meldingen',
        }
      }
    })

    .state('menu.message-details', {
      url: '/message/:messageDetails',
      views: {
        menuContent: {
          templateUrl: 'templates/message-details.html',
          controller: 'Meldingen'
        }
      }
    })

     .state('menu.laboratorium', {
      url: '/laboratorium',
      views: {
        menuContent: {
          templateUrl: 'templates/laboratorium.html',
          controller: 'Laboratorium'
        }
      }
    })

     .state('menu.users', {
      url: '/users',
      views: {
        menuContent: {
          templateUrl: 'templates/users.html',
          controller: 'Gebruikers'
        }
      }
    })

     .state('menu.rapportage', {
      url: '/rapportage',
      views: {
        menuContent: {
          templateUrl: 'templates/rapportage.html',
          controller: 'Rapportage'
        }
      }
    })

     .state('menu.wijzigingen', {
      url: '/wijzigingen',
      views: {
        menuContent: {
          templateUrl: 'templates/wijzigingen.html',
          controller: 'Adreswijzigingen'
        }
      }
    })

     .state('menu.log', {
      url: '/log',
      views: {
        menuContent: {
          templateUrl: 'templates/log.html',
          controller: 'Systeemlog'
        }
      }
    })
    
    .state('menu.kit-instellingen', {
      url: '/kit-instellingen',
      views: {
        menuContent: {
          templateUrl: 'templates/kit-instellingen.html',
          controller: 'KitSettings'
        }
      }
    })


  $urlRouterProvider.otherwise('/login');

}])

.run(['$rootScope', function($rootScope) {

  //Scroll to top of page on view change so the user doesn't start a view in the middle of the page
    $rootScope.$on("$viewContentLoading",
        function (event) {
          $('body','html').animate({
              scrollTop: $('body','html')
          }, 0);

          //Show loading screen on route change
          $('.loading-overlay').fadeIn(0);
        });

    $rootScope.$on("$viewContentLoaded",
        function (event) {
            $('.loading-overlay').fadeOut(0);
        });

}])

//Filter double items from ng-repeat
.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
})

//Convert object to array
.filter('toArray', function () {
  return function (obj, addKey) {
    if (!angular.isObject(obj)) return obj;
    if ( addKey === false ) {
      return Object.keys(obj).map(function(key) {
        return obj[key];
      });
    } else {
      return Object.keys(obj).map(function (key) {
        var value = obj[key];
        return angular.isObject(value) ?
          Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
          { $key: key, $value: value };
      });
    }
  }
})

//Set math round filter
.filter('round', function() {
    return function(input) {
        return Math.round(input);
    };
})

//Capitalize filter
.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})

//Trusted sources
.filter('trusted', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
})

// Start from filter (for pagination)
.filter('startFrom', function() {
    return function(input, start) {         
        return input.slice(start);
  }
})

//When a form changes directive
.directive("formOnChange", function($parse){
  return {
    require: "form",
    link: function(scope, element, attrs){
       var cb = $parse(attrs.formOnChange);
       element.on("change", function(){
          cb(scope);
       });
    }
  }
})

//Make the 'select' box work on mobile browsers
.directive('select',function(){ //same as "ngSelect"
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, ele) {
            ele.on('touchmove touchstart',function(e){
                e.stopPropagation();
            })
        }
    }
})

;