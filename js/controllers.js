angular.module('myControllers', [])

/* ---------- MENU ---------- */
.controller('Menu', ['$rootScope', '$scope', '$state', '$window', '$filter', 'orderByFilter', '$timeout', function($rootScope, $scope, $state, $window, $filter, orderBy, $timeout) {

	//Help button function
	$scope.showHelp = function() {    
		swal({
		  title: 'Hulp',
		  type: 'question',
		  html: 'Heeft u hulp nodig bij het gebruik van de app? Raadpleeg dan onze handleiding. U kunt te allen tijde de handleiding raadplegen door linksboven in de app op de <span style="color:#fff;background:#0f7366;font-weight:bold;border-radius:100%;width:28px;height:25px;padding-top:2.5px;display:inline-block;">?</span> knop te klikken.',
		  confirmButtonColor: '#009999',
		  showCancelButton: true,
		  cancelButtonText: 'Sluiten',
		  confirmButtonText: 'Handleiding'
		}).then(function() {
			window.open("https://bvobaarmoederhals.nl/files/Handleiding-Commpanionz-App.pdf",'_blank');
		});

		//Set help message to 'true' in local storage so it won't automatically show on login
		localStorage.setItem('CommpanionzAppHelp',true);
	}

	//Show help on first login
	if (localStorage.getItem('CommpanionzAppHelp') === null) {
		$scope.showHelp();
	} else {
		//Do nothing if 'CommpanionzAppHelp' already exists in local storage, meaning it has already been shown before
	}

	//Check if user is logged in
	if ($rootScope.sessionData && $rootScope.loggedInUserId && $rootScope.loggedInUserToken) {

		//Get logged in user data
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {				

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {

					$timeout(function() {

						//Logged in user object
						$rootScope.loggedInUserObject = response.users;

						//Get logged in user role
						$rootScope.role = response.users.role;

						//Get logged in user name
						$rootScope.username = response.users.identity;

						//Get logged in user image
						$rootScope.profileImageCurrentUser = response.users.image;

            //Get logged in user route
						if ($rootScope.role == 'huisartsenpraktijk' || $rootScope.role == 'koerier') {

							//Get list of routes that are connected to the user
							$rootScope.loggedInUserRouteList = response.users.routeId;

							//Functions to execute once all routes are loaded
							$rootScope.$watch('routesDetails', function (newValue, oldValue, scope) {
								//Add route names and default drive dates to the user route list
								for (var r in $rootScope.loggedInUserRouteList) {
									for (var ro in $rootScope.routesDetails) {
										if ($rootScope.routesDetails[ro].routeId == $rootScope.loggedInUserRouteList[r].id) {
											$rootScope.loggedInUserRouteList[r]['routeName'] = $rootScope.routesDetails[ro].routeNaam;
											$rootScope.loggedInUserRouteList[r]['vasteDag'] = $rootScope.routesDetails[ro].vasteDag;
										}
									}
								}

								//Get the route that is driven today (by getting the visit day and comparing it with today (day as string)).
								//Set $rootScope variable for that route.
								for (var route in $rootScope.loggedInUserRouteList) {
									if ($rootScope.getWeekDay(new Date()) == $rootScope.loggedInUserRouteList[route].vasteDag) {
										$rootScope.loggedInUserRoute = $rootScope.loggedInUserRouteList[route].id;
									}
								}
							});

							if ($state.current.name == 'menu.route') {
								//Preselect current date
								$scope.currentDate = new Date($.date(Date.now()).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );
								//Click on date in calender
								$timeout(function() {
									$('.datepickerDays').find('span:contains("'+$scope.currentDate.getDate()+'")').parent('a').each(function() {
										if ($(this).parent().hasClass('datepickerNotInMonth')) {
											//Do nothing
										} else {
											$(this).click();
										}
									});
								});
							}
						}

            //Get logged in user region
            $rootScope.loggedInUserRegion = response.users.regionId;

            if ($rootScope.role == 'beheerder' || $rootScope.role == 'koerier') {
              $rootScope.tempRegion = '';
            } else {
              $rootScope.tempRegion = $rootScope.loggedInUserRegion;
            }

						//Get logged in user region
						$rootScope.loggedInUserRegion = response.users.regionId;

						//Get logged in user URL
						$rootScope.loggedInUserUrl = response.users.url;

						//Get logged in address
						$rootScope.huisartsenpraktijkDataAddress = response.users.address;

						//Get logged in city
						$rootScope.huisartsenpraktijkDataCity = response.users.city;

						//Get logged in postcode
						$rootScope.huisartsenpraktijkDataPostcode = response.users.postcode;

						//Get logged in phone
						$rootScope.huisartsenpraktijkDataTelephone = response.users.telephone;

						//Get logged in email
						$rootScope.huisartsenpraktijkDataEmail = response.users.email;

						//Get place in route
						$rootScope.loggedInUserRoutePlace = response.users.routePlace;

						//Get all regions
						$rootScope.getRegions();

						//Get all routes
						$rootScope.getRoutes();

						//Get all roles
            $rootScope.getRoles();

            if ($rootScope.role == 'huisartsenpraktijk') {
              $rootScope.getExtraEmailAddresses();
            }

						//Get all users
						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:$rootScope.tempRegion,routeId:'',roleId:''}}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								console.log('data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:$rootScope.tempRegion,routeId:'',roleId:''}}}));

								if (response.state.result == 'Fail') {
									swal({
									  title: 'Oops...',
									  text: response.state.message,
									  type: 'error',
									  confirmButtonColor: '#009999'
									});

									if (response.state.message == 'Session Expired') {
										$rootScope.removeSession();
									}

								} else if (response.state.result == 'Success') {
									$timeout(function() {
										$rootScope.usersDetails = response.users;
									});

								} else if (response.state.result == 'Empty') {
									swal({
									  title: 'Geen resultaten',
									  text: 'Er zijn geen resultaten gevonden...',
									  type: 'warning',
									  confirmButtonColor: '#009999'
									});
								}
							},
							error: function(response) {
								console.log(response);
							}
						});
						//End get all users

						//Get all unread meldingen
						$rootScope.getUnreadMeldingen();

						//Load all user changes (adreswijzigingen) if logged in user is 'beheerder'
						if ($rootScope.role == 'beheerder') {
							$rootScope.getAllUserChanges();
						}

						//Get all type meldingen
						$rootScope.getTypeMeldingen();

						//Get all klacht reasons
						$rootScope.getKlachtReasons();

						//Get all kittypes
						$rootScope.getKitTypes();

						//Get no materiaal opgehaald reasons
						$rootScope.getAllPlanningGeenMateriaalReden();

						//Get all verzendmethodes
						$rootScope.getAllPlanningVerzendMethodes();

						//Get reasons for Bezemwagen as verzendmethode
						$rootScope.getReasonsBezemwagen();

						//Get reasons for Spoedenvelop as verzendmethode
						$rootScope.getReasonsSpoedenvelop();

					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});

	} else {
		//If not logged in, navigate to login page
		$state.go('login');
	}

	//Log out user
	$('.logout-button').click(function() {
		$rootScope.removeSession();
	});

	//Change view title in top bar based on controller title
	$scope.dynamicActiveView = function() {
		$rootScope.viewTitle = $state.current.views.menuContent.controller;
	};

	//Do function when arriving on page
	$scope.dynamicActiveView();

	//Do function whenwhen state changes
	$rootScope.$on('$stateChangeSuccess', function(){
		$scope.dynamicActiveView();
	});

	$('.logged-in-user').click(function() {
		$('.logout-container').addClass('animated fadeInUp');
		$('.logout-container').removeClass('hide');
		$('.logout-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	});

	$('.close-logout').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});

	$('.logout-overlay').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});

	$('.logout-button').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});


	//Toggle hamburger menu
	$('.menu').click(function() {
		$('.menu').animate({
	        scrollTop: $('.menu ')
	    }, 0); //scroll to top of menu element because overflow is scroll, sometimes it will be scrolled down a bit
		$('.menu').toggleClass('full-height');
	});

}])































/* ---------- MAIN CONTROLLER ---------- */
.controller('MainCtrl', ['$rootScope', '$scope', '$state', '$timeout', '$filter', function($rootScope, $scope, $state, $timeout, $filter) {

	//Get ajax URL
	$rootScope.ajaxURL = 'https://bvobaarmoederhals.nl/backend/index.php';

	//Test url (staging url)
	// $rootScope.ajaxURL = 'https://bvobaarmoederhals.nl/staging/index.php';

	//Setup function for every AJAX request
	$.ajaxSetup({
	    beforeSend: function() {
	        //Show loading screen
			$('.loading-overlay').fadeIn(0);
	    },
	    complete: function() {
	        //Hide loading screen
			$('.loading-overlay').fadeOut(0);
	    }
	});

	//Get session data
	$rootScope.getSessionData = function() {
		if (localStorage.getItem('CommpanionzAppSessionData') === null) {
			$rootScope.loggedInUserId = null;
			$rootScope.loggedInUserToken = null;
		} else {
			$rootScope.sessionData = JSON.parse(localStorage.getItem('CommpanionzAppSessionData'));
			$rootScope.loggedInUserId = $rootScope.sessionData.userId;
			$rootScope.loggedInUserToken = $rootScope.sessionData.token;
		}
	}

	//Call get session data function
	$rootScope.getSessionData();

	//Remove session data
	$rootScope.removeSession = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DestroySession",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {				

				$rootScope.sessionData = null;
				$rootScope.loggedInUserId = null;
				$rootScope.loggedInUserToken = null;
				localStorage.removeItem('CommpanionzAppSessionData');
				location.reload();

				if (response.state.result == 'Fail') {
					//Sessie verwijderen mislukt

				} else if (response.state.result == 'Success') {
					//Sessie verwijderd
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End remove session data

	//Get all regions
	$rootScope.getRegions = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRegions",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.regionsDetails = response.regions;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all regions

	//Get all roles
	$rootScope.getRoles = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRoles",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.rolesDetails = response.roles;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all roles

	//Get all routes
	$rootScope.getRoutes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRoutes",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.routesDetails = response.routes;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all routes

	//Get all users (based on filter)
	$rootScope.getUsers = function(regioId, routeId, roleId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:regioId,routeId:routeId,roleId:roleId}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.usersDetails = response.users;
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all users (based on filter)

	//Get user details
	$rootScope.getSingleUser = function(singleUserId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:singleUserId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					$timeout(function() {

						$scope.$apply(function() {
							$scope.singleUserDetails = response.users;
						});

						console.log($scope.singleUserDetails);

						//Get huisartsen if user is huisartsenpraktijk
						if ($scope.singleUserDetails.role == 'huisartsenpraktijk') {
							$rootScope.getHuisartsen($scope.singleUserDetails.user_id);
						}
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get user details

	//Get user huisartsen
	$rootScope.getHuisartsen = function(huisartsId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetHuisartsen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{huisartsId:huisartsId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.huisartsen = response.huisartsen;
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get user huisartsen

	//Set user huisartsen
	$rootScope.setHuisartsen = function(huisartsId,huisartsen) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"SetHuisartsen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{huisartsId:huisartsId,huisartsen:huisartsen}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					//Huisartsen zijn toegevoegd (functie wordt gecombineerd met updateUser, succesmelding komt daarvandaan)
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End set user huisartsen

	//Update user
	$rootScope.updateUser = function(userId,email,oldemail,roleId,regionId,identity,name,lastname,routeId,routePlace,image,address,postcode,city,telephone,standaardWaarde,url,notitie,password,newpassword) {
		
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:email,oldemail:oldemail,roleId:roleId,regionId:regionId,identity:identity,name:name,lastname:lastname,routeId:routeId,routePlace:routePlace,image:image,address:address,postcode:postcode,city:city,telephone:telephone,standaardWaarde:standaardWaarde,url:url,notitie:notitie,password:password,newpassword:newpassword}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				console.log('data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:email,oldemail:oldemail,roleId:roleId,regionId:regionId,identity:identity,name:name,lastname:lastname,routeId:routeId,routePlace:routePlace,image:image,address:address,postcode:postcode,city:city,telephone:telephone,standaardWaarde:standaardWaarde,url:url,notitie:notitie,password:password,newpassword:newpassword}}));

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De wijzigingen zijn doorgevoerd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End update user

	//Get all adreswijzigingen
	$rootScope.getAllUserChanges = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUpdateRequests",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.userUpdates = response.data.updates;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all adreswijzigingen

	//Get all type messages
	$rootScope.getTypeMeldingen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetTypeMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.typeMeldingen = response.meldingen;
					});
				}
			},
			error: function(response) {
				console.log(response);		
			}
		});
	}//End get all type messages

	//Get all klacht reasons
	$rootScope.getKlachtReasons = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRedenKlacht",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.klachtReasons = response.redenKlacht;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all klacht reasons

	//Get total amount of unread notifications
	$rootScope.calculateUnread = function() {
		$rootScope.getUnreadMeldingen();
	}

	//Get unread meldingen
	$rootScope.getUnreadMeldingen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUnreadMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.unreadMessages = response.state.count;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}
	//End get unread meldingen

	//Send message
	$rootScope.sendMessage = function(userId,typemelding,bericht,anders,redenklacht,klacht_user_id,klacht_van_user_id,receivers,routeId2send,regionId2send,roleId2send,sendAllUsers) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"CreateMelding",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{ontvangers:receivers,userId:userId,typemelding:typemelding,bericht:bericht,anders:anders,redenklacht:redenklacht,klacht_user_id:klacht_user_id,klacht_van_user_id:klacht_van_user_id,routeId2send:routeId2send,regionId2send:regionId2send,roleId2send:roleId2send,sendAllUsers:sendAllUsers}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'Melding is verstuurd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Reload all sent messages (if they were already loaded)
					if ($rootScope.mySentMessages) {

						//Load dates now and three months ago, so messages can be downloaded from that period
						var currentDateMeldingen = $.reverseDate(Date.now());
						var d = new Date();
						var threeMonthsAgoDateMeldingen = $.reverseDate(d.setMonth(d.getMonth() - 3));

						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:$rootScope.loggedInUserId,ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:'',ShowOntvangers:''}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								if (response.state.result == 'Fail') {
									swal({
									  title: 'Oops...',
									  text: response.state.message,
									  type: 'error',
									  confirmButtonColor: '#009999'
									});

									if (response.state.message == 'Session Expired') {
										$rootScope.removeSession();
									}

								} else if (response.state.result == 'Success') {
									$timeout(function() {
										$rootScope.mySentMessages = response.data.meldingen;
										$rootScope.mySentMessagesFiltered = $rootScope.mySentMessages;
									});
								}
							},
							error: function(response) {
								console.log(response);
							}
						});
					}

					$('.send-message-overlay').fadeOut(0);
					$('.menuContent').addClass('nativeScroll');
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End send message

	//Get kittypes
	$rootScope.getKitTypes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.kitTypes = response.kits;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get kittypes

	//Get all bestellingen for user on selected date
	$rootScope.getAllUserBestellingen = function(datum, userId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetBestellingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.userBestellingen = response.bestellingen;
					});

				} else if (response.state.result == 'Empty') {
					$timeout(function() {
						$rootScope.userBestellingen = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all bestellingen for user on selected date

	//Delete specific bestelling for user on selected date
	$rootScope.deleteSelectedBestelling = function(bestellingId) {
		
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DeleteBestelling",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{bestellingId:bestellingId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//Delete specific bestelling for user on selected date

	//Set aanwezigheid for selected date and user
	$rootScope.setUserDateAanwezigheid = function(userId,datum,afwezig) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"SetAfwezigheid",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum,afwezig:afwezig}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End set aanwezigheid for selected date and user

	//Get aanwezigheid for selected date and user
	$rootScope.getUserDateAanwezigheid = function(userId,datum) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetAanwezig",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.userAanwezigheid = '0';
						$scope.praktijkAanwezigheid = response.aanwezigheid[0].afwezig;
						//Change select field to new value on 'Planning' page
						//$('#planningAanwezigheidSelect').val($scope.userAanwezigheid).trigger('change');
					});
				} else if (response.state.result == 'Empty') {
					$timeout(function() {
						$scope.userAanwezigheid = '0';
						delete $scope.praktijkAanwezigheid;
						//Change select field to new value on 'Planning' page
						//$('#planningAanwezigheidSelect').val($scope.userAanwezigheid).trigger('change');
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get aanwezigheid for selected date and user

	//Get planning
	$rootScope.getPlanning = function(startdatum,einddatum,routeId,planningId) {
		
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:startdatum,einddatum:einddatum,routeId:routeId,planningId:planningId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.planningDetails = response.planning;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get planning

	//Save planning
	$rootScope.updatePlanning = function(planningId,datum,routeId,praktijkId,afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason,huisartsenpraktijk,bestellingen,materiaal_opgehaald,kits_afgeleverd,noMateriaalGivenReason,anders,signatureKit,signatureMateriaal,koerier_id,koerier,notitie,regionId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdatePlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{planningId:planningId,datum:datum,routeId:routeId,praktijkId:praktijkId,afwijkendeVerzendmethode:afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason:afwijkendeVerzendmethodeReason,huisartsenpraktijk:huisartsenpraktijk,bestellingen:bestellingen,materiaal_opgehaald:materiaal_opgehaald,kits_afgeleverd:kits_afgeleverd,noMateriaalGivenReason:noMateriaalGivenReason,anders:anders,signatureKit:signatureKit,signatureMateriaal:signatureMateriaal,koerier_id:koerier_id,koerier:koerier,notitie:notitie,regio:regionId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {				

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					//Succes message
					swal({
					  title: 'Gelukt!',
					  text: 'De gegevens zijn opgeslagen.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//save

	//Get verzendmethodes
	$rootScope.getAllPlanningVerzendMethodes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningVerzendMethodes",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.verzendmethodes = response.verzendmethodes;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get verzendmethodes

	//Get no materiaal opgehaald reasons
	$rootScope.getAllPlanningGeenMateriaalReden = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningGeenMateriaalReden",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.geenmateriaalreden = response.geenmateriaalreden;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get no materiaal opgehaald reasons

	//Get reasons for Bezemwagen
	$rootScope.getReasonsBezemwagen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningBezemwagen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.bezemwagen = response.bezemwagen;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get reasons for Bezemwagen

	//Get reasons for Spoedenvelop
	$rootScope.getReasonsSpoedenvelop = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningSpoedEnvelop",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.spoedenvelop = response.spoedenvelop;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get reasons for Spoedenvelop

	//Delete notitie
    $rootScope.deleteNotitie = function(userId) {
    	$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DeleteNotitie",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{UserIdNotitie:userId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {
				console.log(response);

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De notitie is verwijderd!',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
    }//End delete notitie

    //Add specific kits to users (huisartsenpraktijken)
    $rootScope.authorizeKits = function(userId,regioId,routeId,kitId) {
    	$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"AssignUserKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,regioId:regioId,routeId:routeId,kitId:kitId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De kit is opgeslagen bij de gekozen gebruiker(s).',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
    }//End add specific kits to users (huisartsenpraktijken)

	//Convert profile image to Base64 for storing it as text in database
    File.prototype.convertToBase64 = function(callback){
        var reader = new FileReader();
        reader.onload = function(e) {
             callback(e.target.result)
        };
        reader.onerror = function(e) {
             callback(null);
        };        
        reader.readAsDataURL(this);
    };

	//Restrict image upload size on file inputs
    $(document).unbind().on('change', 'input[type=file]', function(event){
    	var fileTarget = event.currentTarget;
    	var selectedFile = fileTarget.files[0];
    	if(typeof fileTarget.files[0] !== 'undefined'){
            var maxSize = parseInt($(this).attr('max-size'),10);
            var size = fileTarget.files[0].size;
            if (maxSize > size) {
            	//If size is accepted, convert to base64 and show to user in .profile-image-container
            	selectedFile.convertToBase64(function(base64){
	        		$('.profile-image-container').css('background', 'url('+base64+')');
	        		$timeout(function() {
	        			$rootScope.profileImageInput = base64;
	        		});
	      		});
            } else {
            	//File size is not accepted
            	swal({
				  title: 'Oops...',
				  text: 'De foto mag maximaal maar 1mb groot zijn.',
				  type: 'error',
				  confirmButtonColor: '#009999'
				});
				//Reset file input
				$(fileTarget).val('');
				$('.profile-image-container').css('background','transparent');
            }
        } else {
        	//Reset file input
        	$(fileTarget).val('');
			$('.profile-image-container').css('background','transparent');
        }
    });

    //Convert date to DD-MM-YYYY
	$.date = function(dateObject) {
	    var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    var hours = d.getHours();
	    var minutes = d.getMinutes();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }

	    var dateString = day + "-" + month + "-" + year;
	    return dateString;
	};

	//Convert date to YYYYMMDD (for database)
	$.reverseDate = function(dateObject) {
	    var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    var hours = d.getHours();
	    var minutes = d.getMinutes();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }

	    var reverseDateString = year + "" + month + "" + day;
	    return reverseDateString;
	};

	//Convert YYYYMMDD to date object
	$rootScope.convertDatabaseDate = function(dateString) {
		var dateString  = dateString;
		var year = dateString.substring(0,4);
		var month = dateString.substring(4,6);
		var day = dateString.substring(6,8);

		var outputDate = $.date(new Date(year, month-1, day));

		return outputDate;
	}

    //Get days of the week
    $rootScope.getWeekDay = function(date) {
		var weekday = new Array(7);
		weekday[0]=  "zondag";
		weekday[1] = "maandag";
		weekday[2] = "dinsdag";
		weekday[3] = "woensdag";
		weekday[4] = "donderdag";
		weekday[5] = "vrijdag";
		weekday[6] = "zaterdag";

		return weekday[date.getDay()];
	}

	//Custom jQuery scroll to position function
	jQuery.fn.scrollTo = function(elem) { 
	    $(this).scrollTop($(this).scrollTop() - $(this).offset().top + $(elem).offset().top); 
	    return this; 
	}

}])






























/* ---------- NAVIGATION CONTROLLER ---------- */
.controller('NavigationCtrl', ['$scope', '$location', function ($scope, $location) {
    //Automatically select menu item that is the current page
    $scope.isCurrentPath = function (path) {
      return $location.path() == path;
    };
}])



































/* ---------- LOGIN ---------- */
.controller('Login', ['$rootScope', '$scope', '$state', '$timeout', function($rootScope, $scope, $state, $timeout) {

	//If already logged in, go to route page
	if ($rootScope.sessionData && $rootScope.loggedInUserId && $rootScope.loggedInUserToken) {
		$state.go('menu.route');
	}

	//Login form function being called on login.html
	$scope.login = function() {

		//Check if all fields are filled, if not:
		if(!$('#login-form input').val()) {
			swal({
			  title: 'Hmm...',
			  text: 'Je hebt niet alle velden ingevuld.',
			  type: 'warning',
			  confirmButtonColor: '#009999'
			});

		//If all fields are filled:
		} else {

			//Set variables for login
			var email = $('#email').val();
			var password = $('#password').val();

			//Call login function from backend
			$.ajax({
				method: 'POST',
				data: 'data=' + JSON.stringify({init:{module:"login",email:email,password:password}}),
				url: $rootScope.ajaxURL,
				dataType: 'json',
				success: function(response) {

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message + ". Voor vragen bel: 0341 495 254",
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					} else if (response.state.result == 'Success') {
						swal({
						  title: 'Gelukt!',
						  text: response.state.message,
						  type: 'success',
						  confirmButtonColor: '#009999'
						});
						
						//Set user details
						localStorage.setItem('CommpanionzAppSessionData',JSON.stringify(response.sessiondata));

						//Get user data
						$rootScope.getSessionData();

						//Go in app
						$state.go('menu.route');
					}

				},
				error: function(response) {
					console.log(response);
				}
			});//End call login function from backend
		};
	}
}])























/* ---------- PLANNING ---------- */
.controller('Planning', ['$rootScope', '$scope', '$state', '$timeout', function($rootScope, $scope, $state, $timeout) {

	//Get users from selected route
	$scope.getUsersWithRoute = function(routeId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:''}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.usersWithRoute = response.users;

						//Add route names to users route arrays
						$scope.addRouteNameToUserObjectPlanning();
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get users from selected route

	//Set the selected route on all fields for the koerier
	$scope.selectRouteKoerier = function(routeId) {
		$rootScope.planningRouteSelect = routeId;
	}

	//Get koeriers from the selected praktijk's route
	$scope.getKoeriersWithRoute = function(routeId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:'4'}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.koeriersWithRoute = response.users;
						console.log($rootScope.koeriersWithRoute);
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen koeriers bij deze route gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});

					//Reset koeriers if none are found
					$timeout(function() {
						$rootScope.koeriersWithRoute = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get koeriers from the selected praktijk's route


	//Get user object of logged in huisartsenpraktijk
	$scope.getLoggedInPraktijkObject = function() {
		if ($rootScope.role == 'huisartsenpraktijk') {
			$rootScope.usersWithRoute = [$rootScope.loggedInUserObject];
			$timeout(function() {
				$('#planningUserSelect').val($rootScope.loggedInUserId).trigger('change');
				$scope.planningUserSelect = $rootScope.loggedInUserId;

				//Add route names to users route arrays
				$scope.addRouteNameToUserObjectPlanning();
			});
		}
	}
	//End get user object of logged in huisartsenpraktijk

	//Get praktijken through search
	$scope.searchPraktijkenPlanningFunction = function() {

		//User search input
		$rootScope.usersPlanningSearchInput = $('.search-praktijken-planning').val();

		//If input is empty
		if ($rootScope.usersPlanningSearchInput.replace(/\s+/g, '') == '' || !$('.search-praktijken-planning').val()) {
			//Set search input to empty (in case the user entered several spaces, makes the placeholder show again)
			$rootScope.usersPlanningSearchInput = '';
			$('.search-praktijken-planning').val('');
		}

		//Reset selected praktijk
		$timeout(function() {
			$('#planningUserSelect').val('');
			$scope.planningUserSelect = '';
			//Remove selected praktijk rootScope variable, since there is a new search the earlier selected praktijk has to be unselected
			delete $rootScope.selectedPraktijkPlanning
		});

		//Set results array
		$rootScope.usersWithRoute = [];

		//Get each keyword from the search seperate and add them to an array
		var searchKeywords = $('.search-praktijken-planning').val().split(' ');

		//Strip empty spaces from keywords
		var keyword = searchKeywords.length;    
		while(keyword--) !/\S/.test(searchKeywords[keyword]) && searchKeywords.splice(keyword, 1);

		//Add space between keyword zipcode (9999AA, four numbers + two letters) to make 9999 AA (can find all postcodes, even when known in database as 9999AA)
		for (var i in searchKeywords) {
			var postcode = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
			if (postcode.test(searchKeywords[i])) {
				searchKeywords.push(searchKeywords[i][4] + searchKeywords[i][5]);
				searchKeywords[i] = searchKeywords[i].substr(0, searchKeywords[i].length - 2);
			}
		}

		//Set counter, to check if the amount of found results is equal to the amount of searched keywords
		var searchPraktijkenPlanningKeywordCounter = 0;

		//Add praktijken to array where search keywords are all found in the user object
		for (var i in $rootScope.usersDetails) {
			keywordloop:
			for (var j in searchKeywords) {
				userloop:
				for (var k in $rootScope.usersDetails[i]) {
					if ($rootScope.usersDetails[i][k] && $rootScope.usersDetails[i][k] != '' && $rootScope.usersDetails[i][k] != null && $rootScope.usersDetails[i][k] != undefined && typeof $rootScope.usersDetails[i][k] != 'array' && typeof $rootScope.usersDetails[i][k] != 'object') {
						var userObjectItem = $rootScope.usersDetails[i][k].toLowerCase();
						var searchKeyword = searchKeywords[j].toLowerCase();
						if ([k] != 'notitie' && userObjectItem.indexOf(searchKeyword) != -1) {
							searchPraktijkenPlanningKeywordCounter += 1;
							if (searchPraktijkenPlanningKeywordCounter === searchKeywords.length) {
								//Push praktijk with all keywords found to array
								$rootScope.usersWithRoute.push($rootScope.usersDetails[i]);
								//Reset keyword counter (for next praktijk in loop)
								searchPraktijkenPlanningKeywordCounter = 0;
								break userloop;
							}
							break;
						}
					}
				}
			}
			//Reset keyword counter on every praktijk
			searchPraktijkenPlanningKeywordCounter = 0;
		}
		//Add route names to users route arrays
		$scope.addRouteNameToUserObjectPlanning();
	}//End get praktijken through search

	//Set rootScope variable to selected praktijk, and get the koeriers from the praktijk's route
	$scope.getSelectedPraktijk = function(userId) {
		$rootScope.selectedPraktijkPlanning = userId;

		//Get details of selected user
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#0f7366'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$rootScope.selectedUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.selectedUserDetails = response.users;
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}

	//Add route names to users route arrays
	$scope.addRouteNameToUserObjectPlanning = function() {
		for (var r in $rootScope.usersWithRoute) {
			if ($rootScope.usersWithRoute[r].role == 'huisartsenpraktijk') {
				for (var ur in $rootScope.usersWithRoute[r].routeId) {
					for (var ro in $rootScope.routesDetails) {
						if ($rootScope.routesDetails[ro].routeId == $rootScope.usersWithRoute[r].routeId[ur].id) {
							$rootScope.usersWithRoute[r].routeId[ur]['routeName'] = $rootScope.routesDetails[ro].routeNaam;
						}
					}
				}
			}
		}
	}

	//Call datepicker on date-select input from datepicker.js
	$scope.callDatepicker = function() {

		//Reset (remove) datepicker
		$('.datepicker').remove();

		//Call datepicker
		$('#date-select-planning').DatePicker({
			flat: true,
			date: new Date(),
			current: new Date(),
			format: 'd-m-Y',
			calendars: 1,
			starts: 1,
			onChange: function(date) {

				$timeout(function() {

					//Get selected date
					$scope.chosenDate = date;

					//Get today's date
					$scope.dateNow = new Date().setHours(0,0,0,0);

					//Create date object from selected date
					$scope.chosenDateStripped = $scope.chosenDate.split('-');
					$scope.chosenDateObject = new Date($scope.chosenDateStripped[2], $scope.chosenDateStripped[1] - 1, $scope.chosenDateStripped[0]);

					//Databasedate
					$scope.dataBaseDate = $.reverseDate($scope.chosenDateObject);

					//Get day of selected date
					$scope.selectedDateDay = $rootScope.getWeekDay($scope.chosenDateObject);

					//User can not change the planning the same day or earlier, except if the user is a beheerder or laboratorium (planning has to be made max the day before).
					if ($rootScope.role != 'beheerder' && $rootScope.role != 'laboratorium') {
						if(parseInt($scope.dateNow - $scope.chosenDateObject) / (1000 * 60 * 60 * 24) < 0) {
							$scope.tooLate = false;
						} else {
							$scope.tooLate = true;
						}
					} else {
						$scope.tooLate = false;
					}

					//Save aanwezigheid
					$scope.saveAanwezigheid = function(userId,aanwezigheid) {
						$rootScope.setUserDateAanwezigheid(userId,$scope.dataBaseDate,aanwezigheid);
					}

					$scope.getUserBestellingenAndAanwezigheid = function(userId) {
						//Get bestellingen for selected user and date
						$rootScope.getAllUserBestellingen($scope.dataBaseDate,userId);
						//Get aanwezigheid
						$rootScope.getUserDateAanwezigheid(userId,$scope.dataBaseDate);
					}

					//Load bestellingen and aanwezigheid automatically if the logged in user is a huisartsenpraktijk
					if ($rootScope.role == 'huisartsenpraktijk') {
						$scope.planningUserSelect = $rootScope.loggedInUserId;
						$scope.getUserBestellingenAndAanwezigheid($rootScope.loggedInUserId);
					} else if ($rootScope.role != 'huisartsenpraktijk') {
						//If user is not a huisartsenpraktijk
						$scope.getUserBestellingenAndAanwezigheid($scope.planningUserSelect);
					}

					//Delete a bestelling
					$scope.deleteBestelling = function(userId,bestellingId) {
						swal({
							title: "Verwijderen", 
							text: "Weet je zeker dat je deze bestelling wilt verwijderen?", 
							type: "warning", 
							showCancelButton: true, 
							confirmButtonColor: "#e74c3c", 
							confirmButtonText: "Verwijder"
							}).then(function(){
								$rootScope.deleteSelectedBestelling(bestellingId);

								$timeout(function() {
									$rootScope.getAllUserBestellingen($scope.dataBaseDate,userId);
								});
							}
						);
					}

					//Add another kit
					$scope.addKit = function() {
						$('.orderKits:first').clone().find('.deleteBestellingButton').remove().end().appendTo('#order-kits-container');
					}

					//Save kit bestellingen
					$scope.saveKit = function(userId) {	

						//Start loop
						$('.orderKits').each(function() {			
						
							var kitType = $(this).find('.planningKitType').val();
							var aantal = $(this).find('.planningKitSelect').val();

							if (kitType !== '' && kitType !== null) {

								$.ajax({
									method: 'POST',
									data: 'data=' + JSON.stringify({init:{module:"AddBestelling",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,kitType:kitType,afleverDatum:$scope.dataBaseDate,aantal:aantal}}),
									url: $rootScope.ajaxURL,
									dataType: 'json',
									success: function(response) {

										if (response.state.result == 'Fail') {
											swal({
											  title: 'Oops...',
											  text: response.state.message,
											  type: 'error',
											  confirmButtonColor: '#009999'
											});
										} else if (response.state.result == 'Success') {
											swal({
											  title: 'Gelukt!',
											  text: response.state.message,
											  type: 'success',
											  confirmButtonColor: '#009999'
											});
										}

									},
									error: function(response) {
										console.log(response);
									}
								});//End AJAX call
							}

							//Remove select fields after updating
							$(this).remove();
						});//End loop

						//Reload bestellingen
						$timeout(function() {
							$rootScope.getAllUserBestellingen($scope.dataBaseDate,userId);
						});
					}////End kit bestellingen
				});
			}
		});
	};

	//Call datepicker if user was already selected (for example after navigating to another page and coming back)
	if ($rootScope.selectedPraktijkPlanning) {
		$scope.callDatepicker();
	}

	//Change user aanwezigheid automatically when a bestelling or material to pick up is added
	$scope.changeUserAanwezigheid = function() {
		$timeout(function() {
			$('#planningAanwezigheidSelect').val('0').change();
		});
	}

	//Show pop-up if someone from region 'Symbiant' orders BVO kit
	$(document).on('change', '.planningKitType', function() {
		if ($(this).val() == '1' && $rootScope.role == 'huisartsenpraktijk' && $rootScope.loggedInUserRegion == '3') {
			swal({
			  title: 'Let op',
			  type: 'warning',
			  html: 'De BVO kit bevat geen aanvraagformulieren. Deze kunt u bestellen via: <a href="https://huisartsenportaal.landelijkescreening.nl" target="_blank">https://huisartsenportaal.landelijkescreening.nl</a>.',
			  confirmButtonColor: '#009999',
			  confirmButtonText: 'Oké'
			})
		}
	});

}])




/* ---------- SETTINGS PAGE ---------- */
.controller('Instellingen', ['$rootScope', '$scope', '$state', '$timeout', function($rootScope, $scope, $state, $timeout) {

	//Reset selecterd profile image
  $rootScope.profileImageInput = '';
  
  // Array met alle emailadressen die momenteel aan een praktijk zijn toegewezen
  // TO-DO:
  // 1. 
  // 2. 

  
  // wanneer het om een huisartsenpraktijk gaat, kan deze meerdere email adressen hebben om aanwezigheid door te geven
  // Deze wordt meteen opgehaald, omdat de functie anders niet wordt uitgevoerd wanneer de instellingenpagina wordt geladen
  $rootScope.getExtraEmailAddresses = function(){
    // Leeg array
    $rootScope.accountEmails = [];

    // haalt emailadressen die bij de praktijk horen op.
    $.ajax({
      type: "POST",
      url:
        "https://bvobaarmoederhals.nl/beheer-email/emails.php",
      data: {
        'action': 'getUserEmails',
        'loggedInUser': $rootScope.loggedInUserId
      },
      success: function(response) {
        response = JSON.parse(response);
        // console.log(response)

        if (response.state == "success") {
          var userEmailAdresses = response.email;
          console.log(userEmailAdresses);

          for (let email of userEmailAdresses) {
            $rootScope.accountEmails.push(email.email);
          }

          console.log($rootScope.accountEmails);
        } else {
          console.log("Er ging iets mis...");
        }

        $scope.$apply();
      },
      error: function(response) {
        console.log(response);
      }
    });
  }

  $scope.removeExtraEmailAddress = function(e){
    
    $.ajax({
      type: "POST",
      url: "https://bvobaarmoederhals.nl/beheer-email/emails.php",
      data: {
        'action' : 'removeUserEmail',
        'loggedInUser' : $rootScope.loggedInUserId,
        'emailToDelete' : e
      },
      success: function(response) {
        swal({
          title: "Gelukt!",
          text: `Het emailadres ${e} is succesvol verwijderd`,
          type: "success",
          confirmButtonColor: "#009999"
        });
        $rootScope.getExtraEmailAddresses();
      },
      error: function(response) {
        console.log(response);
      }
    });

  }

  $scope.addExraEmailAddress = function(email){
    e = $("#addEmailInput").val();

    if($scope.validateEmail(e) == true){
      $.ajax({
        type: "POST",
        url: "https://bvobaarmoederhals.nl/beheer-email/emails.php",
        data: {
          'action': 'addUserEmail',
          'loggedInUser': $rootScope.loggedInUserId,
          'emailToAdd': e
        },
        success: function(response) {
          console.log(response)
          swal({
            title: "Gelukt!",
            text: `Het emailadres ${e} is succesvol toegevoegd`,
            type: "success",
            confirmButtonColor: "#009999"
          });
          
          $rootScope.getExtraEmailAddresses();
          $("#addEmailInput").val('');
        },
        error: function(response) {
          console.log(response);
        }
      });
    }else{
      swal({
        title: "Oops...",
        text: "Dat is geen geldig emailadres",
        type: "error",
        confirmButtonColor: "#009999"
      });
    }
  }

  $scope.validateEmail = function(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
      return true;
    } else {
      return false;
    }
  }

 

  

	//Update user
	$scope.updateOwnUserDetails = function() {
		var newEmail = $('#change-email').val();
		var newPassword = $('#change-password').val();
		var changeName = $('#change-name').val();
		var changeUrl = $('#change-url').val();
		var changeAddress = $('#change-address').val();
		var changeCity = $('#change-city').val();
		var changePostcode = $('#change-postcode').val();
		var changePhone = $('#change-phone').val();
		var image = encodeURIComponent($rootScope.profileImageInput);

		//Update logged in user details AJAX call
		$rootScope.updateUser($rootScope.loggedInUserId,newEmail,$rootScope.huisartsenpraktijkDataEmail,'','',changeName,'','','','',image,changeAddress,changePostcode,changeCity,changePhone,'',changeUrl,'','','');
	}

	//Update password
	$scope.updatePassword = function() {
		if($('#change-password').val() != $('#change-password2').val()) {						
			swal({
			  title: 'Oops...',
			  text: 'De wachtwoorden zijn niet gelijk aan elkaar.',
			  type: 'error',
			  confirmButtonColor: '#009999'
			});
		} else {
			var oldPassword = $('#change-password-old').val();
			var newPassword = $('#change-password').val();

			//Update password AJAX call
			$rootScope.updateUser($rootScope.loggedInUserId,'','','','','','','','','','','','','','','','','',oldPassword,newPassword);
		}
	}

	//Download users
	$rootScope.downloadUsers = function() {

		var downUsersRoleFilter = '2';
		var downUsersRegionFilter = $('#filter-download-regio-select').val();
		var downUsersRouteFilter = $('#filter-download-route-select').val();

		$.ajax({
			method:'POST',
			dataType:'text',
			data:'data=' + JSON.stringify({init:{module:"MakeCSV",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:downUsersRegionFilter,routeId:downUsersRouteFilter,roleId:downUsersRoleFilter}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				//Force download CSV data as CSV file
				csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
		        var anchor = document.createElement('a');
				$(anchor).attr({
		            "href": csvData,
		            "download": "gebruikers_"+$.date(Date.now())+".csv"
		        });
		       	anchor.click();
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End download users

	//Get link for user detail page
	$scope.detailLink = $state.params.userDetails;

	//Get link for huisarts detail page
	$scope.huisartsDetailLink = $state.params.huisartsDetails;

	//Get link for route detail page
	$scope.routeDetailLink = $state.params.routeDetails;

	//Get link for region detail page
	$scope.regionDetailLink = $state.params.regionDetails;

	//Go to register user page depending on which role is selected
	$(document).on('click', '#new-user-button', function() {
		if($('#register-user-select-role').find(':selected').val() == 'beheerder') {
			$state.go('menu.register-beheerder');
		} else if($('#register-user-select-role').find(':selected').val() == 'koerier') {
			$state.go('menu.register-koerier');
		} else if($('#register-user-select-role').find(':selected').val() == 'huisartsenpraktijk') {
			$state.go('menu.register-huisartsenpraktijk');
		} else if($('#register-user-select-role').find(':selected').val() == 'laboratorium') {
			$state.go('menu.register-laboratorium');
		} else {
			swal({
			  title: 'Hmm...',
			  text: 'Je hebt geen rol geselecteerd.',
			  type: 'warning',
			  confirmButtonColor: '#009999'
			});
		}
	});

}])


































/* ---------- REGISTER USER ---------- */
.controller('Registreer', ['$rootScope', '$scope', '$timeout', '$state', function($rootScope, $scope, $timeout, $state) {

	//Reset selecterd profile image
	$rootScope.profileImageInput = '';

    //Add huisarts to huisartsenpraktijk '+' button to add another huisarts
    $('#add-another-huisarts').click(function() {
    	$('#add-huisarts-inputs').append('<span class="add-huisarts-container"><input type="text" name="huisartsenpraktijkHuisarts" placeholder="Voeg een huisarts toe" class="add-huisarts"></span>');
    	$('#add-another-huisarts').appendTo('.add-huisarts-container:last-child');
    });

    //Add another route to user
	$scope.addAnotherRoute = function() {
		$('.route-select:first').clone().appendTo('.registerRouteSelect');
	}


	$scope.register = function() {

		//Make sure only a 'beheerder' can do this
		if($rootScope.role == 'beheerder') {

			//Show loading screen
			$('.loading-overlay').fadeIn(0);

			//Get role based on h2 title of page
			var role = $('.hero h3').text();

			//Set variables for register
			var name = encodeURIComponent($scope.registerName);
			var firstName = $scope.registerFirstName;
			var lastName = $scope.registerLastName;
			var url = $scope.registerUrl;
			var gender = $scope.registerGender;
			var image = encodeURIComponent($rootScope.profileImageInput);

			var email = $scope.registerEmail;
			var address = $scope.registerAddress;
			var city = $scope.registerCity;
			var telephone = $scope.registerTelephone;
			var region = $scope.registerRegion;
			var route = [];
			var laboratoriumUrl = $scope.laboratoriumUrl;
			var laboratoriumRegion = $scope.laboratoriumRegionSelect;
			var standaardWaarde = $scope.registerStandaardRoute;
			var postcode = $scope.registerPostcode;
			var routePlace = $scope.registerRoutePlace
			//var subregion = $scope.registerSubRegion;

			//Get all selected route id's
			$('.route-select').each(function() {
				//Check if route is selected (not empty)
				if ($(this).val()) {
					var selectedValue = $(this).val();
					var routeObject = {id:$(this).val(),place:'0'};
					//Check if route already exists in array, if so, ignore it (prevent duplicates)
				    var id = selectedValue;
					var found = route.some(function (el) {
					  return el.id === selectedValue;
					});
					if (!found) { route.push(routeObject);}
				}
			});
			
				$.ajax({
				method:'POST',
				dataType:'json',
				data:'data=' + JSON.stringify({init:{module:"CreateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{email:email,role_id:role,region_id:region,identity:name,name:firstName,lastname:lastName,geslacht:gender,route_id:route,route_place:$scope.registerRoutePlace,image:image,address:address,postcode:postcode,city:city,telephone:telephone,standaardWaarde:standaardWaarde,url:url}}),
				url:$rootScope.ajaxURL,
				success: function(response) {

					//Hide loading screen
					$('.loading-overlay').fadeOut(0);

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});

						if (response.state.message == 'Session Expired') {
							$rootScope.removeSession();
						}

					} else if (response.state.result == 'Success') {
						swal({
						  title: 'Gelukt!',
						  text: response.state.message,
						  type: 'success',
						  confirmButtonColor: '#009999'
						});
					}
				},
				error: function(response) {
					//Hide loading screen
					$('.loading-overlay').fadeOut(0);
					console.log(response);					
				}
			});

		} else {
			//if user is not a beheerder
			swal({
			  title: 'Oops...',
			  text: 'Je hebt niet de juiste rechten om deze actie uit te voeren!',
			  type: 'error',
			  confirmButtonColor: '#009999'
			});
		}


	};

}])


































/* ---------- FORGOT PASSWORD ---------- */
.controller('forgotPassword', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state) {

	$scope.forgotPassword = function(event) {
		//Check if emailaddress was filled in
		if(!$('#forgot-password-form input').val()) {
			swal({
			  title: 'Hmm...',
			  text: 'Je hebt niet alle velden ingevuld.',
			  type: 'warning',
			  confirmButtonColor: '#009999'
			});

		} else {

			//Remove button to prevent second press
			$(event.currentTarget).remove();

			//Hide loading screen
			$('.loading-overlay').fadeIn(0);

			var emailAddress = $('#forgot-password-form input').val();

			$.ajax({
				method:'POST',
				data:'data=' + JSON.stringify({init:{module:"GetNewPassword",email:emailAddress}}),
				url:$rootScope.ajaxURL,
				dataType:'json',
				success: function(response) {
					//Hide loading screen
					$('.loading-overlay').fadeOut(0);

					

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					} else if (response.state.result == 'Success') {
						swal({
						  title: 'Gelukt!',
						  text: response.state.message,
						  type: 'success',
						  confirmButtonColor: '#009999'
						});						

						//Go back to login
						$state.go('login');
					}
				},
				error: function(response) {
					//Hide loading screen
					$('.loading-overlay').fadeOut(0);
					console.log(response);
				}
			});
		}
	}

}])







































/* ---------- MANAGE USER ---------- */
.controller('manageUser', ['$rootScope', '$scope', '$timeout', '$window', '$state', function($rootScope, $scope, $timeout, $window, $state) {
	//Get user details
	$rootScope.getSingleUser($scope.detailLink);

	//Reset selecterd profile image
	$rootScope.profileImageInput = '';

	//Add another route to user
	$scope.addAnotherRoute = function() {
		$('.updateUserRoute:first').clone().appendTo('.userRoutes');
	}

	//Delete route from user
	$scope.deleteRoute = function(routeId) {
		swal({
			title: "Route loskoppelen", 
			text: "Weet je zeker dat je de gebruiker van deze route wilt loskoppelen? Deze wijziging wordt direct doorgevoerd en kan niet ongedaan worden gemaakt.", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#e74c3c", 
			confirmButtonText: "Verwijder route"
			}).then(function(){

				//Delete route from user AJAX
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteUserRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.detailLink,routeId:routeId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#0f7366'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De gebruiker is losgekoppeld van de route.',
							  type: 'success',
							  confirmButtonColor: '#0f7366'
							});

							//Remove route object from the user's routeId array
							for(var i = 0; i < $scope.singleUserDetails.routeId.length; i++) {
							    var obj = $scope.singleUserDetails.routeId[i];

							    if(routeId.indexOf(obj.id) !== -1) {
							        $scope.singleUserDetails.routeId.splice(i, 1);
							        i--;
							    }
							}
						}

					},
					error: function(response) {
						console.log(response);
					}
				});//End delete route from user AJAX
			});
	}

	//Delete route from koeriers
	$scope.deleteKoerierRoute = function(routeId) {
		swal({
			title: "Route loskoppelen", 
			text: "Weet je zeker dat je de gebruiker van deze route wilt loskoppelen? Deze wijziging wordt direct doorgevoerd en kan niet ongedaan worden gemaakt.", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#e74c3c", 
			confirmButtonText: "Verwijder route"
			}).then(function(){

				//Delete route from user AJAX
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteKoerierRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.detailLink,routeId:routeId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {
						
						console.log('data=' + JSON.stringify({init:{module:"DeleteKoerierRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.detailLink,routeId:routeId}}));

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#0f7366'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De gebruiker is losgekoppeld van de route.',
							  type: 'success',
							  confirmButtonColor: '#0f7366'
							});

							//Remove route object from the user's routeId array
							for(var i = 0; i < $scope.singleUserDetails.routeId.length; i++) {
							    var obj = $scope.singleUserDetails.routeId[i];

							    if(routeId.indexOf(obj.id) !== -1) {
							        $scope.singleUserDetails.routeId.splice(i, 1);
							        i--;
							    }
							}
						}

					},
					error: function(response) {
						console.log('data=' + JSON.stringify({init:{module:"DeleteKoerierRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.detailLink,routeId:routeId}}));
					}
				});//End delete route from user AJAX
			});
	}

	//Add another huisarts
	$scope.addAnotherHuisarts = function(event) {
		$(event.currentTarget).parent().find('.huisarts-name:first').clone().val('').appendTo('.huisartsen-container');
	}

	//Get DOM target for currentroute select field
	$scope.getDomTarget = function(event) {
		$scope.changeRouteTarget = event.currentTarget;
	}

	//Reset route place to 0 when changing an existing route
	$scope.resetRoutePlace = function() {
		if ($($scope.changeRouteTarget).parent().children('.updateUserRoutePlace').length) {
			$($scope.changeRouteTarget).parent().children('.updateUserRoutePlace').val('0');
		}
	}

	//Remove huisarts
	$scope.removeHuisarts = function(huisarts) {
		for(var i = 0; i < $scope.huisartsen.length; i++) {
		    var obj = $scope.huisartsen[i];

		    if(huisarts == obj.huisarts) {
		        $scope.huisartsen.splice(i, 1);
		        i--;

		        //Enable save button
		        $scope.removeDisabledClass();
		    }
		}
	}

	//Update user
	$scope.updateUserFunction = function(id) {

		if ($rootScope.role == 'beheerder' || $rootScope.role == 'koerier') {

			//Set updateable vars
			var image = encodeURIComponent($rootScope.profileImageInput);
			var updateUserName = $('#updateUserName').val();
			var updateUserUrl = $('#updateUserUrl').val();
			var updateUserTelephone = $('#updateUserTelephone').val();
			var updateUserAddress = $('#updateUserAddress').val();
			var updateUserCity = $('#updateUserCity').val();
			var updateNotitie = $('#updateNotitie').val();
			var updateUserRegion = $('#updateUserRegion').val();
			var updateUserRoute = [];
			var updateUserRouteStandaard = $('#updateUserRouteStandaard').val();
			var updateUserKoerier = $('#updateUserKoerier').val();
			var updateUserPostcode = $('#updateUserPostcode').val();
			var updateUserEmailOld = $('#updateUserEmailOld').val();
			var updateUserEmailNew = $('#updateUserEmailNew').val();
			var updateUserNotitie = $('#updateNotitie').val();
			var huisartsen = [];

			//Check if notitie is undefined or not (filled in or not)
			if (updateNotitie == undefined || updateNotitie == '') {
				updateNotitie = '';
			} else {
				//If notitie is filled in, first remove everything between parentheses (placed on date, by...), so this can be replaced with a new name and date
				updateNotitie = updateNotitie.replace(/\(.*?\)/g, '');
				updateNotitie = updateNotitie + ' (geplaatst op ' + $.date(new Date()) + ', door ' + $rootScope.username + ')';
			}

			//Get all selected route id's
			if ($('.updateUserRoute').length == 1 && $('.updateUserRoute').val('0')) {
				//If there are no routes selected
				//Add route to updateUserRoute array
				var routeObject = {id:"0",place:"0"};
				updateUserRoute.push(routeObject);
			} else {
				//If there are routes selected
				$('.updateUserRoute').each(function() {
					//Check if route is selected (not empty)
					if ($(this).val()) {
						var selectedValue = $(this).val();
						//Add route place to the object (for routes that already exist), for new routes use route place 0
						if ($(this).parent().children('.updateUserRoutePlace').length) {
							var routePlaceForObject = $(this).parent().children('.updateUserRoutePlace').val();
							var routeObject = {id:$(this).val(),place:routePlaceForObject};
						} else {
							var routeObject = {id:$(this).val(),place:'0'};
						}
						//Check if route already exists in array, if so, ignore it (prevent duplicates)
					  	var id = selectedValue;
						var found = updateUserRoute.some(function (el) {
						return el.id === selectedValue;
					});
						if (!found) { updateUserRoute.push(routeObject);}
					}
				});
			}

			//Add huisartsen to array
			$('.huisarts-name').each(function() {
				if ($(this).val()) {
					var huisartsObject = {
						huisarts: $(this).val()
					}
					huisartsen.push(huisartsObject);
				}
			});

			//Update user AJAX call
			$rootScope.updateUser(id,updateUserEmailNew,updateUserEmailOld,'',updateUserRegion,updateUserName,'','',updateUserRoute,'',image,updateUserAddress,updateUserPostcode,updateUserCity,updateUserTelephone,updateUserRouteStandaard,updateUserUrl,updateNotitie,'','');

			//Update kits for user
			if ($('#addKitToUser').val() != '') {
				$rootScope.authorizeKits(id,'','',$('#addKitToUser').val());
			}

			//Add huisartsen to huisartsen table in database
			$rootScope.setHuisartsen($scope.singleUserDetails.user_id,huisartsen);

			//Refresh user details
			$timeout(function() {
				$rootScope.getSingleUser($scope.detailLink);
			}, 1000);

		} else {
		//if user is not a beheerder
			swal({
			  title: 'Oops...',
			  text: 'Je hebt niet de juiste rechten om deze actie uit te voeren!',
			  type: 'error',
			  confirmButtonColor: '#009999'
			});
		}
	}//End update user

    $scope.removeDisabledClass = function() {
    	$('#manage-user-details-form').find('button').removeClass('disabled');
    }

    //Delete user
	$scope.deleteUser = function(id) {
		if ($rootScope.role == 'beheerder') {
	    	swal({
				title: "Verwijderen", 
				text: "Weet je zeker dat je deze gebruiker wilt verwijderen?", 
				type: "warning", 
				showCancelButton: true, 
				confirmButtonColor: "#e74c3c", 
				confirmButtonText: "Verwijder"
				}).then(function(){
					//Delete user AJAX
					$.ajax({
						method:'POST',
						dataType:'json',
						data:'data=' + JSON.stringify({init:{module:"DeleteUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:id}}),
						url:$rootScope.ajaxURL,
						success: function(response) {

							if (response.state.result == 'Fail') {
								swal({
								  title: 'Oops...',
								  text: response.state.message,
								  type: 'error',
								  confirmButtonColor: '#009999'
								});

								if (response.state.message == 'Session Expired') {
									$rootScope.removeSession();
								}

							} else if (response.state.result == 'Success') {
								swal({
								  title: 'Gelukt!',
								  text: 'De gebruiker is verwijderd.',
								  type: 'success',
								  confirmButtonColor: '#009999'
								});

								//Go back one page (since the user doesn't exist anymore (only in deleted_users table, there is no reason to be on their profile page)
								$window.history.back();
							}

						},
						error: function(response) {
							console.log(response);
						}
					});//End delete user AJAX

				});
   		} else {
	   		swal({
			  title: 'Oops...',
			  text: 'Je hebt niet de juiste rechten om deze handeling uit te voeren.',
			  type: 'error',
			  confirmButtonColor: '#009999'
			});
	   	}
   	}//End delete user


   	//Delete kit from user(s)
    $scope.deleteUserKits = function(userId,regioId,routeId,kitId) {
    	swal({
			title: "Verwijderen", 
			text: "Weet je zeker dat je deze kit van deze huisartsenpraktijk wilt loskoppelen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#e74c3c", 
			confirmButtonText: "Verwijder"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteUserKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,regioId:regioId,routeId:routeId,kitId:kitId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De kit is verwijderd bij de gebruiker(s).',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});

							//Remove user from kitUsers array
							for(var i = 0; i < $scope.singleUserDetails.kits.length; i++) {
							    var obj = $scope.singleUserDetails.kits[i];

							    if(kitId.indexOf(obj.kitid) !== -1) {
							    	$scope.$apply(function() {
							        	$scope.singleUserDetails.kits.splice(i, 1);
							        	i--;
							        });
							    }
							}

						}

					},
					error: function(response) {
						console.log(response);
					}
				});
			}
		);
    }//End delete kit from user(s)

}])












































/* ---------- MANAGE ROUTE ---------- */
.controller('manageRoute', ['$rootScope', '$scope', '$state', '$filter', 'orderByFilter', '$timeout', function($rootScope, $scope, $state, $filter, orderBy, $timeout) {

	//Update route
	$scope.updateRoute = function(routeId,routeNaam,routeVasteDag) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{routeId:routeId,routeName:routeNaam,vasteDag:routeVasteDag}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End update route

	//Make sure the sortable routes are only sortable vertically
	$scope.sortableRoutes = {
		placeholder: "app-ph",
    	connectWith: ".routes-list"
	};

	//Users from route
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:$scope.routeDetailLink,roleId:'2'}}}),
		url:$rootScope.ajaxURL,
		success: function(response) {


			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.usersDetailsManageRoute = response.users;
				});

			} else if (response.state.result == 'Empty') {
				swal({
				  title: 'Geen resultaten',
				  text: 'Er zijn geen resultaten gevonden...',
				  type: 'warning',
				  confirmButtonColor: '#009999'
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});

	//Update user route place
	$rootScope.updateUserRoute = function(userId,email,oldemail,roleId,regionId,identity,name,lastname,routeId,routePlace,image,address,postcode,city,telephone,standaardWaarde,url,notitie,password,newpassword) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:email,oldemail:oldemail,roleId:roleId,regionId:regionId,identity:identity,name:name,lastname:lastname,routeId:routeId,routePlace:routePlace,image:image,address:address,postcode:postcode,city:city,telephone:telephone,standaardWaarde:standaardWaarde,url:url,notitie:notitie,password:password,newpassword:newpassword}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End update user route place

	//Change the order of the route
	$scope.changeRouteOrder = function() {
		$('.route-item').each(function() {
			var routeUserId = $(this).find('.route-user-id').text();
			var routeUserPlace = $(this).find('.route-number').text();

			//AJAX call update user route places
			$rootScope.updateUserRoute(routeUserId,'','','','','','','',[{id:$scope.routeDetailLink,place:routeUserPlace}],routeUserPlace,'','','','','','','','','','');
		});
	}

	//Delete route (only deletes all users from route, route stays in system)
	$scope.deleteRoute = function(routeId) {
		swal({
			title: "Route legen", 
			text: "Weet je zeker dat je alle gebruikers uit deze route wilt verwijderen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#0f7366", 
			confirmButtonText: "Ja"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteRoute",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{routeId:routeId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De route is geleegd.',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			});
	}//End delete route

}])







































/* ---------- MANAGE REGION ---------- */
.controller('manageRegion', ['$rootScope', '$scope', '$state', function($rootScope, $scope, $state) {

	//Change region name
	$scope.changeRegionNameFunction = function(id,regionName) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateRegion",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{regionId:id,regionName:regionName}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				//Hide loading screen
				$('.loading-overlay').fadeOut(0);

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				//Hide loading screen
				$('.loading-overlay').fadeOut(0);
				console.log(response);				
			}
		});
	}//End change region name

}])










































/* ---------- MESSAGES ---------- */
.controller('Meldingen', ['$rootScope', '$scope', '$timeout', '$state', '$filter', function($rootScope, $scope, $timeout, $state, $filter) {

	//Set active messages page automatically on inbox
	if ($rootScope.activeMessages) {
		//
	} else {
		$rootScope.activeMessages = 'inbox';
	}

	//Load dates now and three months ago, so messages can be downloaded from that period
	var currentDateMeldingen = $.reverseDate(Date.now());
	var d = new Date();
	var threeMonthsAgoDateMeldingen = $.reverseDate(d.setMonth(d.getMonth() - 3));

	//Get all Inbox messages if not yet received and the messages page is opened
	if ($state.current.name == 'menu.messages') {
		if (!$rootScope.myInbox) {
			$.ajax({
				method:'POST',
				dataType:'json',
				data:'data=' + JSON.stringify({init:{module:"GetMyMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:$rootScope.loggedInUserId,routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:''}}),
				url:$rootScope.ajaxURL,
				success: function(response) {

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});

						if (response.state.message == 'Session Expired') {
							$rootScope.removeSession();
						}

					} else if (response.state.result == 'Empty') {
						swal({
						  title: 'Oops...',
						  text: 'Je inbox is leeg.',
						  type: 'warning',
						  confirmButtonColor: '#0f7366'
						}); 

					} else if (response.state.result == 'Success') {
						$timeout(function() {
							$rootScope.myInbox = response.data.meldingen;
							$rootScope.myInboxFiltered = $rootScope.myInbox;
						});
					}
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
	}

	//iOS button bar script
	$rootScope.setIosButtonActive = function(event) {
		if ($(event.currentTarget).hasClass('inboxButton')) {
			$rootScope.activeMessages = 'inbox';

			//Get all Inbox messages if not yet received
			if (!$rootScope.myInbox) {
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"GetMyMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:$rootScope.loggedInUserId,routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:''}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Empty') {
							swal({
							  title: 'Oops...',
							  text: 'Je inbox is leeg.',
							  type: 'warning',
							  confirmButtonColor: '#0f7366'
							}); 

						} else if (response.state.result == 'Success') {
							$timeout(function() {
								$rootScope.myInbox = response.data.meldingen;
								$rootScope.myInboxFiltered = $rootScope.myInbox;
							});
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			}
		} else if ($(event.currentTarget).hasClass('verstuurdButton')) {
			$rootScope.activeMessages = 'verstuurd';

			//Get all Verstuurd messages if not yet received
			if (!$rootScope.mySentMessages) {
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:$rootScope.loggedInUserId,ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:'',ShowOntvangers:''}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Empty') {
							swal({
							  title: 'Oops...',
							  text: 'Je hebt geen verstuurde meldingen.',
							  type: 'warning',
							  confirmButtonColor: '#0f7366'
							});

						} else if (response.state.result == 'Success') {
							$timeout(function() {
								$rootScope.mySentMessages = response.data.meldingen;
								$rootScope.mySentMessagesFiltered = $rootScope.mySentMessages;
							});
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			}
		} else if ($(event.currentTarget).hasClass('klachtenButton')) {
			$rootScope.activeMessages = 'klachten';

			//Set JSON data to use when downloading klachten, based on user role
			if ($rootScope.role == 'beheerder') {
				var messagesKlachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}})
			} else if ($rootScope.role == 'laboratorium') {
				var messagesKlachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:$rootScope.loggedInUserRegion,datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'1',melding:'',ShowOntvangers:''}})
			} else if ($rootScope.role == 'huisartsenpraktijk') {
				var messagesKlachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:$rootScope.loggedInUserId,typemelding:'1',meldingId:'',ShowOntvangers:''}})
			} else if ($rootScope.role == 'koerier') {
				var messagesKlachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:$rootScope.loggedInUserId,klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}})
			}

			//Get all Klachten messages if not yet received
			if (!$rootScope.myKlachten) {
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + messagesKlachtenData,
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Empty') {
							swal({
							  title: 'Oops...',
							  text: 'Er zijn geen klachten om weer te geven.',
							  type: 'warning',
							  confirmButtonColor: '#0f7366'
							});

						} else if (response.state.result == 'Success') {
							$timeout(function() {
								$rootScope.myKlachten = response.data.meldingen;
								$rootScope.myKlachtenFiltered = $rootScope.myKlachten;
							});
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			}
		}
	}

	//Get link for message detail page
	$scope.messageLink = $state.params.messageDetails;

	//////////////////////////////////////////////
	//////////// INBOX PAGINATION ///////////////
	////////////////////////////////////////////
	$rootScope.inboxPageSize = 20;
	if ($rootScope.currentInboxPage) {
		//Do nothing if the current page is already set
	} else {
		$rootScope.currentInboxPage = 0;
	}
	$scope.setCurrentPageInbox = function(event,currentPage) {
	    $rootScope.currentInboxPage = currentPage;
	    $('.pagination-button-inbox').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of users
		$(".menuContent").scrollTo(".inbox");
	}
	$scope.getNumberAsArrayInbox = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPagesInbox = function() {
	    return Math.ceil($rootScope.myInbox.length / $rootScope.inboxPageSize);
	};

	//Get amount of pagination buttons needed
	$scope.getPaginationAmountInbox = function() {
		$rootScope.inboxPaginationAmount = $scope.getNumberAsArrayInbox(Math.ceil($rootScope.myInbox.length / $rootScope.inboxPageSize));
	}

	//Set the pagination button active based on the current page variable
	$scope.getCurrentPaginationPageInbox = function() {
		$('.pagination-button-inbox').removeClass('active');
		$timeout(function() {
			$('.pagination-button-inbox').each(function() {
				if ($(this).text() == ($rootScope.currentInboxPage + 1)) {
					$(this).addClass('active');
				}
			});
		});
	}

	//Recalculate amount of pages needed on search, so the pagination doesn't show too many buttons
	$scope.recalculatePaginationInbox = function() {
		//Set active pagination button to first
		$rootScope.currentInboxPage = 0;
		$('.pagination-button-inbox').removeClass('active');
		$('.pagination-button-inbox').first().addClass('active');
		//Get search input
		$rootScope.inboxSearchInput = $('.inbox-search-bar').val();
		//Filter inbox based on search
		$rootScope.myInboxFiltered = $filter('filter')($rootScope.myInbox,$rootScope.inboxSearchInput);
		//Get pagination amount based on loaded (filtered) inbox
		$rootScope.inboxPaginationAmount = $scope.getNumberAsArrayInbox(Math.ceil($rootScope.myInboxFiltered.length / $rootScope.inboxPageSize));
	}
	//////////////////////////////////////////////
	//////////// END INBOX PAGINATION ///////////
	////////////////////////////////////////////

	//////////////////////////////////////////////
	///////////// SENT PAGINATION ///////////////
	////////////////////////////////////////////
	$rootScope.sentMessagesPageSize = 20;
	if ($rootScope.currentSentMessagesPage) {
		//Do nothing if the current page is already set
	} else {
		$rootScope.currentSentMessagesPage = 0;
	}
	$scope.setCurrentPageSentMessages = function(event,currentPage) {
	    $rootScope.currentSentMessagesPage = currentPage;
	    $('.pagination-button-sent-messages').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of users
		$(".menuContent").scrollTo(".verstuurd");
	}
	$scope.getNumberAsArraySentMessages = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPagesSentMessages = function() {
	    return Math.ceil($rootScope.mySentMessages.length / $rootScope.sentMessagesPageSize);
	};

	//Get amount of pagination buttons needed
	$scope.getPaginationAmountSentMessages = function() {
		$rootScope.sentMessagesPaginationAmount = $scope.getNumberAsArraySentMessages(Math.ceil($rootScope.mySentMessages.length / $rootScope.sentMessagesPageSize));
	}

	//Set the pagination button active based on the current page variable
	$scope.getCurrentPaginationPageSentMessages = function() {
		$('.pagination-button-sent-messages').removeClass('active');
		$timeout(function() {
			$('.pagination-button-sent-messages').each(function() {
				if ($(this).text() == ($rootScope.currentSentMessagesPage + 1)) {
					$(this).addClass('active');
				}
			});
		});
	}

	//Recalculate amount of pages needed on search, so the pagination doesn't show too many buttons
	$scope.recalculatePaginationSentMessages = function() {
		//Set active pagination button to first
		$rootScope.currentSentMessagesPage = 0;
		$('.pagination-button-sent-messages').removeClass('active');
		$('.pagination-button-sent-messages').first().addClass('active');
		//Get search input
		$rootScope.sentMessagesSearchInput = $('.sent-messages-search-bar').val();
		//Filter inbox based on search
		$rootScope.mySentMessagesFiltered = $filter('filter')($rootScope.mySentMessages,$rootScope.sentMessagesSearchInput);
		//Get pagination amount based on loaded (filtered) inbox
		$rootScope.sentMessagesPaginationAmount = $scope.getNumberAsArraySentMessages(Math.ceil($rootScope.mySentMessagesFiltered.length / $rootScope.sentMessagesPageSize));
	}
	//////////////////////////////////////////////
	//////////// END SENT PAGINATION ////////////
	////////////////////////////////////////////

	//////////////////////////////////////////////
	/////////// KLACHTEN PAGINATION /////////////
	////////////////////////////////////////////
	$rootScope.klachtenPageSize = 20;
	if ($rootScope.currentKlachtenPage) {
		//Do nothing if the current page is already set
	} else {
		$rootScope.currentKlachtenPage = 0;
	}
	$scope.setCurrentPageKlachten = function(event,currentPage) {
	    $rootScope.currentKlachtenPage = currentPage;
	    $('.pagination-button-klachten').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of users
		$(".menuContent").scrollTo(".klachten");
	}
	$scope.getNumberAsArrayKlachten = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPagesKlachten = function() {
	    return Math.ceil($rootScope.myKlachten.length / $rootScope.klachtenPageSize);
	};

	//Get amount of pagination buttons needed
	$scope.getPaginationAmountKlachten = function() {
		$rootScope.klachtenPaginationAmount = $scope.getNumberAsArrayKlachten(Math.ceil($rootScope.myKlachten.length / $rootScope.klachtenPageSize));
	}

	//Set the pagination button active based on the current page variable
	$scope.getCurrentPaginationPageKlachten = function() {
		$('.pagination-button-klachten').removeClass('active');
		$timeout(function() {
			$('.pagination-button-klachten').each(function() {
				if ($(this).text() == ($rootScope.currentKlachtenPage + 1)) {
					$(this).addClass('active');
				}
			});
		});
	}

	//Recalculate amount of pages needed on search, so the pagination doesn't show too many buttons
	$scope.recalculatePaginationKlachten = function() {
		//Set active pagination button to first
		$rootScope.currentKlachtenPage = 0;
		$('.pagination-button-klachten').removeClass('active');
		$('.pagination-button-klachten').first().addClass('active');
		//Get search input
		$rootScope.klachtenSearchInput = $('.klachten-search-bar').val();
		//Filter inbox based on search
		$rootScope.myKlachtenFiltered = $filter('filter')($rootScope.myKlachten,$rootScope.klachtenSearchInput);
		//Get pagination amount based on loaded (filtered) inbox
		$rootScope.klachtenPaginationAmount = $scope.getNumberAsArrayKlachten(Math.ceil($rootScope.myKlachtenFiltered.length / $rootScope.klachtenPageSize));
	}
	//////////////////////////////////////////////
	////////// END KLACHTEN PAGINATION //////////
	////////////////////////////////////////////

	//toggle message overlay
	//close
	$scope.closeMessageOverlay = function() {
		$('.send-message-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	}
	//open
	$scope.openMessageOverlay = function() {
		$('.send-message-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	}

	//SEND MESSAGE
	$(document).on('click', '.send-message', function(){
	
		//Show loading screen
		$('.loading-overlay-extra').fadeIn(0);

		//Set message vars
		var receivers = [];
		var meldingMessage = $('.meldingMessage').val();
		var messageType = $('.meldingTypeSelect').val();
		var routeId2send = '';
		var regionId2send = '';
		var roleId2send = '';
		var sendAllUsers = '';
		if ($('.sendMessageTo:checked').val() == 'region') {
			regionId2send = $('.sendMessageToResult').val();
		}
		if ($('.sendMessageTo:checked').val() == 'route') {
			routeId2send = $('.sendMessageToResult').val();
		}
		if ($('.sendMessageTo:checked').val() == 'role') {
			roleId2send = $('.sendMessageToResult').val();
		}
		if ($('.sendMessageTo:checked').val() == 'all') {
			sendAllUsers = 'all';
		}

		//Send message
		//If message is not a klacht
		if (messageType != '1') {
			//Send message to all beheerders if the logged in user is not a beheerder
			if ($rootScope.role != 'beheerder') {
				//Get beheerders and add them to receivers
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:'1'}}}),
					url:$rootScope.ajaxURL,
					success: function(response) {					

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success' || response.state.result == 'Empty') {
							$timeout(function() {
								$scope.messageBeheerders = response.users;

								//Add beheerders to 'receivers' array
								for (var beheerder in $scope.messageBeheerders) {
									receivers.push({id: $scope.messageBeheerders[beheerder].user_id});
								}

								//Send message
								$rootScope.sendMessage(receivers[0].id,messageType,meldingMessage,'','','','',receivers,'','','','');

								//Hide loading screen
								$('.loading-overlay-extra').fadeOut(0);
							});

						}
					},
					error: function(response) {
						console.log(response);
					}
				});//End get beheerders and add them to receivers
			} else if ($rootScope.role == 'beheerder') {
				$rootScope.sendMessage('',messageType,meldingMessage,'','','','',receivers,routeId2send,regionId2send,roleId2send,sendAllUsers);

				//Hide loading screen
				$('.loading-overlay-extra').fadeOut(0);
			}
		} else if (messageType == '1') {
			var redenKlacht = $('.reasonKlacht').val();
			var anders = '';
			var klacht_user_id = $('.selectKlachtKoerier').val();
			var klacht_van_user_id = $('.selectKlachtHuisartsenpraktijk').val();
			if ($('.reasonKlachtAnders').val()) {
				anders = $('.reasonKlachtAnders').val();
			}

			//Send message
			$rootScope.sendMessage('',messageType,meldingMessage,anders,redenKlacht,klacht_user_id,klacht_van_user_id,'','','','');

			//Hide loading screen
			$('.loading-overlay-extra').fadeOut(0);
		}
	});

	//Get all users from route when message type to send is 'klacht'
	$(document).on('change', '.selectKlachtRoute', function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:$('.selectKlachtRoute').val(),roleId:''}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {					

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.usersFromRouteForKlacht = response.users;
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen gebruikers in deze route gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	});
	
}])
































/* ---------- GET SINGLE MESSAGE ---------- */
.controller('getSingleMessage', ['$rootScope', '$scope', '$state', '$timeout', '$window', function($rootScope, $scope, $state, $timeout, $window) {
	//Get single message
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:'',einddatum:'',klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:$scope.messageLink,ShowOntvangers:'true'}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Empty') {
				swal({
				  title: 'Oops...',
				  text: 'Melding bestaat niet (of is mogelijk verwijderd).',
				  type: 'warning',
				  confirmButtonColor: '#0f7366'
				});

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$rootScope.singleMessage = response.data.meldingen;
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}])
































/* ---------- MESSAGE DETAILS ---------- */
.controller('manageMessage', ['$rootScope', '$scope', '$state', '$timeout', '$window', function($rootScope, $scope, $state, $timeout, $window) {

	//Set message to read if it is the logged in user and read was 0
	if ($scope.message.gelezen == '0') {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"SetMeldingRead",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$rootScope.loggedInUserId,meldingId:$scope.message.meldingId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {
				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});
					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}
				} else if (response.state.result == 'Success') {
					//Message is set to 'gelezen' in database

					//Recalculate total amount of unread notifications
					$rootScope.calculateUnread();
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End set message read

	//Get klacht follow-ups
	if ($scope.message.typemelding == '1') {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetFollowup",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{meldingId:$scope.message.meldingId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.followUps = response.data.followups
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get klacht follow-ups

	//Add klacht follow-up
	$scope.addKlachtFollowUp = function(followup,meldingId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"CreateFollowup",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{meldingId:$scope.message.meldingId,userId:$rootScope.loggedInUserId,bericht:followup}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De follow-up is toegevoegd aan het bericht.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Reload view to refresh follow-ups
					$state.reload();
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End add klacht follow-up

	//Delete melding
	$scope.deleteMelding = function(meldingId) {
		swal({
			title: "Melding verwijderen", 
			text: "Weet je zeker dat je deze melding wilt verwijderen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#0f7366", 
			confirmButtonText: "Ja"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteMelding",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{meldingId:meldingId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De melding is verwijderd.',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});

							//Remove message object from the messages array
							/*for(var i = 0; i < $scope.meldingenDetails.length; i++) {
							    var obj = $scope.meldingenDetails[i];

							    if(meldingId.indexOf(obj.meldingId) !== -1) {
							        $scope.meldingenDetails.splice(i, 1);
							        i--;
							    }
							}*/

							//Leave message page since it doesn't exists anymore
							$window.history.back(); 
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			});
	}//End add delete melding

	//Delete follow-up
	$scope.deleteFollowup = function(followupId) {
		swal({
			title: "Follow-up verwijderen", 
			text: "Weet je zeker dat je deze follow-up wilt verwijderen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#0f7366", 
			confirmButtonText: "Ja"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteFollowup",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{followupId:followupId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De follow-up is verwijderd.',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});

							//Remove message object from the messages array
							$timeout(function() {
								for(var i = 0; i < $scope.followUps.length; i++) {
								    var obj = $scope.followUps[i];

								    if(followupId.indexOf(obj.followupId) !== -1) {
								        $scope.followUps.splice(i, 1);
								        i--;
								    }
								}
							});
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			});
	}//End delete follow-up

}])









































/* ---------- ROUTE ---------- */
.controller('Route', ['$scope', '$timeout', '$rootScope', '$filter', function($scope, $timeout, $rootScope, $filter) {

	//Get users with selected route
	$scope.getUsersAndPlanningWithRoute = function(routeId) {

		//Users from route
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:''}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				console.log('data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:''}}}));

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.usersDetailsRouteNoFilter = response.users;

						//Get aanwezigheid for selected date
						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetAanwezig",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:'',datum:$scope.dataBaseDate}}),
							url:$rootScope.ajaxURL,
							success: function(response) {				
								
								if (response.state.result == 'Success' || response.state.result == 'Empty') {
									$timeout(function() {
										$scope.userAanwezigheid = response.aanwezigheid;										

										for (var i in $scope.usersDetailsRouteNoFilter) {
											for (var j in $scope.userAanwezigheid) {
												if ($scope.userAanwezigheid[j].userId == $scope.usersDetailsRouteNoFilter[i].user_id) {   
													//Set user afwezigheid in users array
													$scope.usersDetailsRouteNoFilter[i]['afwezig'] = $scope.userAanwezigheid[j].afwezig;
													break;
												} else {
													//Set user afwezigheid to null if doesn't exists in aanwezigheidstabel
													$scope.usersDetailsRouteNoFilter[i]['afwezig'] = null;
												}
											}
										}//End loop

										//Check aanwezigheid for each user in users array
										var afwezig = '1';
										var aanwezig = '0';
										var standaardWaarde = 'nee';
										var roleKoerier = 'koerier';

										for(var i = 0; i < $scope.usersDetailsRouteNoFilter.length; i++) {
										    var obj = $scope.usersDetailsRouteNoFilter[i];

										    //Remove user from array if set to 'afwezig' or if 'standaardWaarde' is 'nee' and user is not set to aanwezig (afwezig = 0), excluding 'koeriers'
										    if(((afwezig.indexOf(obj.afwezig) !== -1) || (standaardWaarde.indexOf(obj.standaardWaarde) !== -1 && aanwezig.indexOf(obj.afwezig) == -1)) && roleKoerier.indexOf(obj.role) == -1) {
										        $scope.usersDetailsRouteNoFilter.splice(i, 1);
										        i--;
										    }
										}

										//Set new variable for route
										$timeout(function() {
											$scope.usersDetailsRoute = $scope.usersDetailsRouteNoFilter;
											$rootScope.praktijkenGefilterd = $scope.usersDetailsRoute;
										});

										//Reset pagination to first page
										$scope.currentPage = 0;
									});
								}

							},
							error: function(response) {
								console.log(response);
							}
						});//End Get aanwezigheid

					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});

					//Reset values, so the old data doesn't still show
					$timeout(function() {
						$rootScope.planningDetails = '';
						$scope.usersDetailsRoute = '';
						$scope.userAanwezigheid = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});

		//Get planning
		$rootScope.getPlanning($scope.dataBaseDate,'',routeId,'');
	}
	

	// ZOEKFUNCTIE PRAKTIJKEN
	// FILTERT DE PRAKTIJKEN IN DE GELADEN ROUTE

	// zoekfunctie  
	$scope.zoekPraktijkInRouteFunctie = function() {
		// $rootScope.praktijkenGefilterd = $scope.usersDetailsRoute;
		$scope.allePraktijkenInRoute = $scope.usersDetailsRoute;
		$rootScope.praktijkenGefilterd = $scope.allePraktijkenInRoute;


		$rootScope.praktijkGezocht = false;
		$rootScope.zoekPraktijkInput = $('.zoek-praktijk-route').val();
		
		// check of veld leeg is 
		if($rootScope.zoekPraktijkInput.replace(/\s+/g, '') == '' || !$('.zoek-praktijk-route').val()){

			// Als 'praktijkGezocht' variabel bestaat, wordt het verwijderd. Dit verandert de tekst op de zoekknop
			$rootScope.praktijkGezocht = false;

			// reset variabel en inputveld zodat placeholder weer verschijnt (indien er alleen spaties in het veld staan)
			$rootScope.usersSearchInput = "";
			$(".zoek-praktijk-route").val("");



			$scope.numberOfPages();
			$rootScope.currentUsersPage = 0;
			$(".pagination-button").removeClass("active");
			$(".pagination-button").first().addClass("active");
			$scope.currentPage = 0;

		}else{

			$rootScope.praktijkGezocht = true;

			$rootScope.praktijkenGefilterd = [];

           //Get each keyword from the search seperate and add them to an array
			var searchKeywords = $(".zoek-praktijk-route").val().split(" ");

			//Strip empty spaces from keywords
			var keyword = searchKeywords.length;
			while (keyword--) !/\S/.test(searchKeywords[keyword]) && searchKeywords.splice(keyword, 1);

			//Add space between keyword zipcode (9999AA, four numbers + two letters) to make 9999 AA (can find all postcodes, even when known in database as 9999AA)
			for (var i in searchKeywords) {
				var postcode = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
				if (postcode.test(searchKeywords[i])) {
				searchKeywords.push(searchKeywords[i][4] + searchKeywords[i][5]);
				searchKeywords[i] = searchKeywords[i].substr(0, searchKeywords[i].length - 2);
				}
			}

			//Set counter, to check if the amount of found results is equal to the amount of searched keywords
			var searchUsersKeywordCounter = 0;

			//Add praktijken to array where search keywords are all found in the user object
			for (var i in $scope.allePraktijkenInRoute) {
				keywordloop: 
				for (var j in searchKeywords) {
					userloop: 
					for (var k in $scope.allePraktijkenInRoute[i]) {

						if ($scope.allePraktijkenInRoute[i][k] && $scope.allePraktijkenInRoute[i][k] != "" && $scope.allePraktijkenInRoute[i][k] != null && $scope.allePraktijkenInRoute[i][k] != undefined && typeof $scope.allePraktijkenInRoute[i][k] != "array" && typeof $scope.allePraktijkenInRoute[i][k] != "object") {
							var userObjectItem = $scope.allePraktijkenInRoute[i][k].toLowerCase();
							var searchKeyword = searchKeywords[j].toLowerCase();

							if ([k] != "notitie" && userObjectItem.indexOf(searchKeyword) != -1) {
								searchUsersKeywordCounter += 1;
								if (searchUsersKeywordCounter === searchKeywords.length) {
									//Push praktijk with all keywords found to array
									$rootScope.praktijkenGefilterd.push($scope.allePraktijkenInRoute[i]);
									//Reset keyword counter (for next praktijk in loop)
									searchUsersKeywordCounter = 0;
									break userloop;
								}
								break;
							}
						}
					}
				}
				//Reset keyword counter on every praktijk
				searchUsersKeywordCounter = 0;
			}

			//Reset user list
			$scope.numberOfPages();
			$rootScope.currentUsersPage = 0;
			$(".pagination-button").removeClass("active");
			$(".pagination-button").first().addClass("active");
			$scope.currentPage = 0;
         }

    };  

	//Automatically load the route for the current day for huisartsen and koeriers if a route they are connected to is driven on the same day as the current day
	//This will be executed once the '$rootScope.loggedInUserRoute' has been loaded before manually selecting a route
	$rootScope.$watch('loggedInUserRoute', function (newValue, oldValue, scope) {
		if (!$scope.usersDetailsRouteNoFilter && $rootScope.loggedInUserRoute) {
			$scope.getUsersAndPlanningWithRoute($rootScope.loggedInUserRoute);
		}
	});

	//Route pagination
	$scope.currentPage = 0; 
	$scope.pageSize = 10;

	$scope.setCurrentPage = function(event,currentPage) {
	    $scope.currentPage = currentPage;
	    $('.pagination-button').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of route
		$(".menuContent").scrollTo("#top-of-route");
	}
	$scope.getNumberAsArray = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPages = function() {
		var targetRole = 'koerier';
		var roleCounter = 0;

		//Get amount or koeriers and exclude them from pagination, since they do not have to be visited
		for(var i = 0; i < $scope.praktijkenGefilterd.length; i++) {
		    var obj = $scope.praktijkenGefilterd[i];
		    if((targetRole.indexOf(obj.role) !== -1)) {
		    	roleCounter += 1;
		    }
		}

	    return Math.ceil(($scope.praktijkenGefilterd.length - roleCounter) / $scope.pageSize);
	};

	//Preselect current date
	$scope.currentDate = new Date($.date(Date.now()).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );
	//Click on date in calender
	$timeout(function() {
		$('.datepickerDays').find('span:contains("'+$scope.currentDate.getDate()+'")').parent('a').each(function() {
			if ($(this).parent().hasClass('datepickerNotInMonth')) {
				//Do nothing
			} else {
				$(this).click();
			}
		});
	});

	//Call datepicker on date-select input from datepicker.js
	$('#date-select-route').DatePicker({
		flat: true,
		date: new Date(),
		current: new Date(),
		format: 'd-m-Y',
		calendars: 1,
		starts: 1,
		onChange: function(date) {


			$timeout(function() {

				//Selected date
				$scope.chosenDate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );

				//Get day of selected date
				$scope.selectedDateDay = $rootScope.getWeekDay($scope.chosenDate);

				//Get weeknumber of selected date
				$scope.selectedWeekNumber = $filter('date')($scope.chosenDate, 'ww');

				//Get current weeknumber
				$scope.currentWeekNumber = $filter('date')(new Date(), 'ww');
				
				//Show warning if selected date is not in the current week
				if ($scope.selectedWeekNumber != $scope.currentWeekNumber) {
					swal({
					  title: 'Let op!',
					  text: 'De geselecteerde datum valt niet binnen de huidige week.',
					  type: 'warning',
					  confirmButtonColor: '#0f7366'
					});
				}

				//Databasedate
				$scope.dataBaseDate = $.reverseDate($scope.chosenDate);

				if ($scope.loggedInUserRegion != '' && $scope.loggedInUserRoute) {
					$rootScope.planningDetails = '';
					$scope.usersDetailsRoute = '';
					$scope.userAanwezigheid = '';
					$scope.getUsersAndPlanningWithRoute($scope.loggedInUserRoute);
				} else if ($scope.loggedInUserRegion == '' && $scope.loggedInUserRoute == '') {
					//Reset form on date change
					$('#routeForm')[0].reset();
					$rootScope.planningDetails = '';
					$scope.usersDetailsRoute = '';
					$scope.userAanwezigheid = '';					
				}

			});
		}
	});

	//Reset all route cards and scroll to top
	$rootScope.closeRouteCards = function() {
		$('.route-card').removeClass('full-height');
		$('.toggle-route span').text('+');
		//$('.toggle-route').qtip('destroy');
		window.scrollTo(0, 0);
	}

	//toggle message overlay
	//close
	$scope.closeMessageOverlay = function() {
		$('.send-message-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	}
	//open
	$scope.openMessageOverlay = function() {
		$('.send-message-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	}

	//Send message
	$scope.sendMessageFunctionCall = function(routeId,regionId) {
		$timeout(function() {
		
			//Show loading screen
			$('.loading-overlay-extra').fadeIn(0);

			//Set message vars
			var meldingMessage = $('.melding-message').val();
			var messageType = '2';
			var routeId2send = routeId;

			//Get beheerders and add them to receivers
			$.ajax({
				method:'POST',
				dataType:'json',
				data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:'1'}}}),
				url:$rootScope.ajaxURL,
				success: function(response) {					

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});

						if (response.state.message == 'Session Expired') {
							$rootScope.removeSession();
						}

					} else if (response.state.result == 'Success' || response.state.result == 'Empty') {
						$timeout(function() {
							$scope.vertragingBeheerders = response.users;

							//Add beheerders to 'receivers' array
							var receivers = [];
							for (var beheerder in $scope.vertragingBeheerders) {
								receivers.push({id: $scope.vertragingBeheerders[beheerder].user_id});
							}

							//Send message
							$rootScope.sendMessage(receivers[0].id,messageType,meldingMessage,'','','','',receivers,routeId2send,'','','');

							//Hide loading screen
							$('.loading-overlay-extra').fadeOut(0);
						});

					}
				},
				error: function(response) {
					console.log(response);
				}
			});//End get beheerders and add them to receivers

		});
	}//End send message

	//Export route to CSV
	$scope.exportRouteToCsv = function() {
		$.ajax({
			method:'POST',
			dataType:'text',
			data:'data=' + JSON.stringify({init:{module:"MakeCSV",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:$scope.loggedInUserRoute,roleId:'2'}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {
				//Force download CSV data as CSV file
				csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
		        var anchor = document.createElement('a');
				$(anchor).attr({
		            "href": csvData,
		            "download": "gebruikers_"+$.date(Date.now())+".csv"
		        });
		       	anchor.click();
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End export route to CSV


	/* ---------- HANDTEKENING FUNCTIE ----------
	//Close signature box
	$scope.closeSignature = function() {
		$('.signature-overlay').fadeOut(0);
		$('.signature-container').fadeOut(0);
	}
	
	$(document).on('click', '.signature-overlay', function() {
		$scope.closeSignature();
	});


	$(document).on('click', '.close-signature-overlay', function() {
		$scope.closeSignature();
	});

	//Create canvas function
	$scope.createCanvas = function() {

		// Set up the canvas
		var canvas = document.getElementById($scope.canvasId);
		var ctx = canvas.getContext('2d');
		ctx.strokeStyle = "#222222";
		ctx.lineWith = 2;

		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;

		// Set up mouse events for drawing
		var drawing = false;
		var mousePos = { x:0, y:0 };
		var lastPos = mousePos;
		canvas.addEventListener("mousedown", function (e) {

		  drawing = true;
		  lastPos = getMousePos(canvas, e);
		}, false);
		canvas.addEventListener("mouseup", function (e) {
		  drawing = false;
		}, false);
		canvas.addEventListener("mousemove", function (e) {
		  mousePos = getMousePos(canvas, e);
		}, false);

		// Get the position of the mouse relative to the canvas
		function getMousePos(canvasDom, mouseEvent) {
		  var rect = canvasDom.getBoundingClientRect();
		  return {
		    x: mouseEvent.clientX - rect.left,
		    y: mouseEvent.clientY - rect.top
		  };
		}

		// Get a regular interval for drawing to the screen
		window.requestAnimFrame = (function (callback) {
	        return window.requestAnimationFrame || 
	           window.webkitRequestAnimationFrame ||
	           window.mozRequestAnimationFrame ||
	           window.oRequestAnimationFrame ||
	           window.msRequestAnimaitonFrame ||
	           function (callback) {
	        window.setTimeout(callback, 1000/60);
	           };
		})();

		// Draw to the canvas
		function renderCanvas() {
		  if (drawing) {
		    ctx.moveTo(lastPos.x, lastPos.y);
		    ctx.lineTo(mousePos.x, mousePos.y);
		    ctx.stroke();
		    lastPos = mousePos;
		  }
		}

		// Allow for animation
		(function drawLoop () {
		  requestAnimFrame(drawLoop);
		  renderCanvas();
		})();

		// Set up touch events for mobile, etc
		canvas.addEventListener("touchstart", function (e) {

		  mousePos = getTouchPos(canvas, e);
		  var touch = e.touches[0];
		  var mouseEvent = new MouseEvent("mousedown", {
		    clientX: touch.clientX,
		    clientY: touch.clientY
		  });
		  canvas.dispatchEvent(mouseEvent);
		}, false);
		canvas.addEventListener("touchend", function (e) {
		  var mouseEvent = new MouseEvent("mouseup", {});
		  canvas.dispatchEvent(mouseEvent);
		}, false);
		canvas.addEventListener("touchmove", function (e) {
		  var touch = e.touches[0];
		  var mouseEvent = new MouseEvent("mousemove", {
		    clientX: touch.clientX,
		    clientY: touch.clientY
		  });
		  canvas.dispatchEvent(mouseEvent);
		}, false);

		// Get the position of a touch relative to the canvas
		function getTouchPos(canvasDom, touchEvent) {
		  var rect = canvasDom.getBoundingClientRect();
		  return {
		    x: touchEvent.touches[0].clientX - rect.left,
		    y: touchEvent.touches[0].clientY - rect.top
		  };
		}

		// Prevent scrolling when touching the canvas
		document.body.addEventListener("touchstart", function (e) {
		  if (e.target == canvas) {
		    e.preventDefault();
		  }
		}, false);
		document.body.addEventListener("touchend", function (e) {
		  if (e.target == canvas) {
		    e.preventDefault();
		  }
		}, false);
		document.body.addEventListener("touchmove", function (e) {
		  if (e.target == canvas) {
		    e.preventDefault();
		  }
		}, false);

		//Create image and cut out empty space
		$scope.cropImageFromCanvas = function() {
			var w = $scope.canvasCrop.width,
			h = $scope.canvasCrop.height,
			pix = {x:[], y:[]},
			imageData = $scope.ctxCrop.getImageData(0,0,$scope.canvasCrop.width,$scope.canvasCrop.height),
			x, y, index;

			for (y = 0; y < h; y++) {
			    for (x = 0; x < w; x++) {
			        index = (y * w + x) * 4;
			        if (imageData.data[index+3] > 0) {

			            pix.x.push(x);
			            pix.y.push(y);

			        }   
			    }
			}
			pix.x.sort(function(a,b){return a-b});
			pix.y.sort(function(a,b){return a-b});
			var n = pix.x.length-1;

			w = pix.x[n] - pix.x[0];
			h = pix.y[n] - pix.y[0];
			var cut = $scope.ctxCrop.getImageData(pix.x[0], pix.y[0], w, h);

			$scope.canvasCrop.width = w;
			$scope.canvasCrop.height = h;
			$scope.ctxCrop.putImageData(cut, 0, 0);

			var image = $scope.canvasCrop.toDataURL();

			//Get huisartsenpraktijk or koerier depending on class of canvas
			if ($($scope.canvasCrop).hasClass('materiaal')) {

				//Loading screen while posting signature
				$('.loading-overlay').fadeIn(0);

				//Put koerier signature in database
				firebase.database().ref('planning/' + $scope.chosenDate + '/' + $rootScope.loggedInUserRoute.$value + '/' + $scope.huisartsenpraktijkSignatureId + '/').update({	     
				    signatureMateriaal: image,
				    materiaalGiven: $scope.materiaalGiven,
				    noMateriaalGivenReason: $scope.noMateriaalGivenReason
				}).then(function() {
					$scope.closeSignature();

					//Hide loading screen when signature is posted
					$('.loading-overlay').fadeOut(300, function() {
						swal({
						  title: 'Gelukt!',
						  text: 'De handtekening is opgeslagen!',
						  type: 'success',
						  confirmButtonColor: '#009999'
						});
					});

				}).catch(function(error) {

					//Hide loading screen if signature error
					$('.loading-overlay').fadeOut(300, function() {
						swal({
						  title: 'Oops...',
						  text: error.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					});

				});
			} else if ($($scope.canvasCrop).hasClass('kit')) {

				//Loading screen while posting signature
				$('.loading-overlay').fadeIn(0);
				
				//Put huisartsenpraktijk signature in database
				firebase.database().ref('planning/' + $scope.chosenDate + '/' + $rootScope.loggedInUserRoute.$value + '/' + $scope.huisartsenpraktijkSignatureId + '/').update({	     
				    signatureKit: image
				}).then(function() {
					$scope.closeSignature();

					//Hide loading screen when signature is posted
					$('.loading-overlay').fadeOut(0, function() {
						swal({
						  title: 'Gelukt!',
						  text: 'De handtekening is opgeslagen!',
						  type: 'success',
						  confirmButtonColor: '#009999'
						});
					});

				}).catch(function(error) {

					//Hide loading screen if signature error
					$('.loading-overlay').fadeOut(0, function() {
						swal({
						  title: 'Oops...',
						  text: error.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					});

				});
			}
		}

		//Close signature box
		$('.signature-close').click(function() {
			$scope.closeSignature();
		});
	}

	//Open signature box on button
	$(document).on('click', '.signature-button', function() {
		$('.signature-overlay').fadeIn(0);

		if ($rootScope.role.$value == 'koerier') {
			//Remove signature image when signature drawing starts
			$(document).on('mousedown touchstart', '.canvas-span', function() {
				$('.signature-image').addClass('hide');
			});
		}

		$(this).parent().parent().children('.signature-container').fadeIn(0);

		//Get name of huisartsenpraktijk so we know where to put the signature in the database
		$scope.huisartsenpraktijkSignatureId = $(this).parent().children('.chosenHuisartsId').text();

		$scope.canvasId = $(this).parent().parent().find('canvas').attr('id');
		$scope.materiaalGiven = $(this).parent().find('.no-materiaal-given').val();
		if($(this).parent().find('.no-materiaal-given-reason').val()) {
			$scope.noMateriaalGivenReason = $(this).parent().find('.no-materiaal-given-reason').val();
		} else {
			$scope.noMateriaalGivenReason = '';
		}

		$scope.createCanvas();
	});

	//Change title and class of canvas depending on koerier/huisartsenpraktijk button
	$(document).on('click', '.signature-button.signature-button-materiaal', function() {
		$(this).parent().parent().find('canvas').addClass('materiaal').removeClass('kit');
		$('.kit-signature-image').css('display', 'none');
		$('.materiaal-signature-image').css('display', 'block');
		$(this).parent().parent().find('.signature-container').find('h3').text('HANDTEKENING MATERIAAL');
		$(this).parent().parent().find('.signature-container').find('.canvas-span').children('.materiaal-signature-image').removeClass('hide');
	});

	$(document).on('click', '.signature-button.signature-button-kit', function() {
		$(this).parent().parent().find('canvas').addClass('kit').removeClass('materiaal');
		$('.materiaal-signature-image').css('display', 'none');
		$('.kit-signature-image').css('display', 'block');
		$(this).parent().parent().find('.signature-container').find('h3').text('HANDTEKENING KIT');
		$(this).parent().parent().find('.signature-container').find('.canvas-span').children('.kit-signature-image').removeClass('hide');
	});

	//Save signature
	$(document).on('click', '.signature-done', function() {
		$scope.canvasCrop = document.getElementById($scope.canvasId);
		$scope.ctxCrop = $scope.canvasCrop.getContext('2d');
		$scope.cropImageFromCanvas();
	});*/
}])

//Single huisartsenpraktijk details inroute
.controller('singleUserRouteDetails', ['$scope', '$timeout', '$rootScope', function($scope, $timeout, $rootScope) {

	//Get index of praktijk in route
	$scope.routeIndex = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize + $scope.pageSize;

	//Get bestellingen for users and date in current route (per card)
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetBestellingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.user.user_id,datum:$scope.dataBaseDate}}),
		url:$rootScope.ajaxURL,
		success: function(response) {	


			if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.userBestellingenRoute = response.bestellingen;
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});//Get bestellingen for users and date in current route (per card)

	//toggle route item
	$scope.toggleRouteItemHeight = function(event,userId,routeCardIndex) {
		$('.route-card').removeClass('full-height');
		$('.toggleRouteIcon').text('+');
		$(event.currentTarget).parents('.route-card').toggleClass('active-card');
		$(event.currentTarget).parents('.route-card').addClass('full-height');
		if ($(event.currentTarget).parents('.route-card').hasClass('active-card')) {
			$(event.currentTarget).find('span').text('-');
		} else {
			$(event.currentTarget).parents('.route-card').removeClass('full-height');
			$(event.currentTarget).find('span').text('+');
		}
		//Check if there is any check missing (geen vinkje, dus wanneer een praktijk nog niet bezocht is)
		//Geef in dat geval een melding
		$('.route-card').each(function(index) {
			if (index < routeCardIndex && routeCardIndex > 0) {
				if (!$(this).find('.check-mark')[0]) {
					swal({
					  title: 'Let op!',
					  text: 'Niet alle voorgaande praktijken zijn bezocht. Controleer of elk bezoek is doorgevoerd in de app.',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
					return false;
				}
			} else {
				return false;
			}
		});
	}//End toggle route item

	//Save planning details for given huisartsenpraktijk
	$scope.savePlanning = function(event,kitsAfgeleverd,planningId,materiaalOpgehaald,noMateriaalGivenReason,noMateriaalGivenReasonElse,afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason,notitie,praktijkId,praktijkName,routeId,regionId) {
		//Get all kits delivered
		if ($(event.currentTarget).parent().find('.kitsAfgeleverdType').val()) {
			var kitsAfgeleverdArray = [];
			$(event.currentTarget).parent().find('.kitsAfgeleverd').each(function() {
				var kitTypeAfgeleverd = $(this).find('.kitsAfgeleverdType').val();

				if ($(this).find('.kitsAfgeleverdType').val()) {

					if ($(this).find('.kitsAfgeleverdAantal').val()) {
						var kitTypeAantalAfgeleverd = $(this).find('.kitsAfgeleverdAantal').val();
					} else {
						var kitTypeAantalAfgeleverd = '0';
					}

					var kitsAfgeleverdObject = {
						KitId:kitTypeAfgeleverd,
						aantal:kitTypeAantalAfgeleverd
					}

					//Add given kits to array
					kitsAfgeleverdArray.push(kitsAfgeleverdObject);
				}
			});
		} else {
			var kitsAfgeleverdArray = '';
		}

		//Check if notitie is undefined or not (filled in or not)
		if (notitie == undefined || notitie == '') {
			notitie = '';
		} else {
			//If notitie is filled in, first remove everything between parentheses (placed on date, by...), so this can be replaced with a new name and date
			notitie = notitie.replace(/\s+\(.*?\)/g, '').replace(/\(.*?\)/g, '');
			notitie = notitie + ' (geplaatst op ' + $.date(new Date()) + ', door ' + $rootScope.username + ')';
		}

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdatePlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{planningId:planningId,datum:$.reverseDate($scope.chosenDate),routeId:routeId,praktijkId:praktijkId,afwijkendeVerzendmethode:afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason:afwijkendeVerzendmethodeReason,huisartsenpraktijk:praktijkName,bestellingen:kitsAfgeleverdArray,materiaal_opgehaald:materiaalOpgehaald,kits_afgeleverd:kitsAfgeleverd,noMateriaalGivenReason:noMateriaalGivenReason,anders:noMateriaalGivenReasonElse,signatureKit:'',signatureMateriaal:'',koerier_id:$rootScope.loggedInUserId,koerier:$rootScope.username,notitie:notitie,regio:regionId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {	

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					//Succes message
					swal({
					  title: 'Gelukt!',
					  text: 'De gegevens zijn opgeslagen.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Close route card and scroll back to the top of that route card
					$(event.currentTarget).parents('.route-card').toggleClass('full-height');
					$(event.currentTarget).parents('.route-card').find('.toggleRouteIcon').text('+');
					$('.menuContent').scrollTop($('.menuContent').scrollTop() - $('.menuContent').offset().top + $(event.currentTarget).parents('.route-card').offset().top - 100);

					//Reset planning
					$timeout(function() {
						$rootScope.planningDetails = '';

						$timeout(function() {
							$rootScope.getPlanning($scope.dataBaseDate,'',routeId,'');
						});
					});
				}
			},
			error: function(response) {
				console.log(response);
				swal({
				  title: 'Oops...',
				  text: 'Kon de gegevens niet opslaan. Ververs de pagina en probeer het nogmaals.',
				  type: 'error',
				  confirmButtonColor: '#0f7366'
				});
			}
		});
	}//End save planning details for given huisartsenpraktijk

	//Add another kit to user
	$scope.addKitToRoute = function(event) {
		$(event.currentTarget).parent().find('.kitsAfgeleverd:first').clone().find('.removeKitFromRouteButton').remove().end().appendTo($(event.currentTarget).parent().find('.kitsAfgeleverdContainer'));
	}

	//Delete route
	$scope.removeKitFromRoute = function(event) {
		$(event.currentTarget).parent().remove();
	}

	//Edit user inside a route card
	$scope.editUserInRoute = function(event) {
		$(event.currentTarget).parent().find('.change-praktijk-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	};

	//Edit user inside a route card
	$scope.closeEditUserInRoute = function(event) {
		$('.change-praktijk-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	};

	$scope.saveUserInRoute = function(updateUserName,updateUserEmail,updateUserEmailNew,updateUserTelephone,updateUserAddress,updateUserCity,updateUserPostcode,notitie,userId) {
		
		//Check if notitie is undefined or not (filled in or not)
		if (notitie == undefined || notitie == '') {
			notitie = '';
		} else {
			//If notitie is filled in, first remove everything between parentheses (placed on date, by...), so this can be replaced with a new name and date
			notitie = notitie.replace(/\(.*?\)/g, '');
			notitie = notitie + ' (geplaatst op ' + $.date(new Date()) + ', door ' + $rootScope.username + ')';
		}
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:updateUserEmailNew,oldemail:updateUserEmail,roleId:'',regionId:'',identity:updateUserName,name:'',lastname:'',routeId:'',routePlace:'',image:'',address:updateUserAddress,postcode:updateUserPostcode,city:updateUserCity,telephone:updateUserTelephone,standaardWaarde:'',url:'',notitie:notitie,password:'',newpassword:''}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'U zult de route opnieuw moeten inladen om de wijzigingen te kunnen bekijken. Indien u geen beheerdersrol hebt zullen eventuele adreswijzigingen eerst door een beheerder moeten worden goedgekeurd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Close user edit screen
					$scope.closeEditUserInRoute();
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	};//End edit user inside route card
}])































/* ---------- LABORATORIUM ---------- */
.controller('Laboratorium', ['$scope', '$rootScope', '$timeout', function($scope, $rootScope, $timeout) {

	//Get laboratorium if user is not a beheerder
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:$rootScope.loggedInUserRegion,routeId:'',roleId:'3'}}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$rootScope.laboratoriumDetails = response.users;
				});

			} else if (response.state.result == 'Empty') {
				swal({
				  title: 'Geen resultaten',
				  text: 'Er zijn geen resultaten gevonden...',
				  type: 'warning',
				  confirmButtonColor: '#009999'
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});//End get laboratorium if user is not a beheerder

	//Get laboratorium if user IS a beheerder
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:'3'}}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$rootScope.laboratoriumDetailsBeheerder = response.users;
				});

			} else if (response.state.result == 'Empty') {
				swal({
				  title: 'Geen resultaten',
				  text: 'Er zijn geen resultaten gevonden...',
				  type: 'warning',
				  confirmButtonColor: '#009999'
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});//End get laboratorium if user IS a beheerder

	//Get all huisartsenpraktijken from selected route
	$scope.getHuisartsenpraktijkenFromRoute = function(routeId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:''}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.huisartsenpraktijkenDetails = response.users;
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all huisartsenpraktijken from selected route
}])































/* ---------- USERS ---------- */
.controller('Gebruikers', ['$scope', '$rootScope', '$timeout', '$filter', function($scope, $rootScope, $timeout, $filter) {

	//Get users (based on filter)
	$('.getUsersButton').click(function() {

		//Set filter variables
		//If role selection is a beheerder, than ignore the route or region filter
		$rootScope.getUsersRoleId = $('#get-users-role-select').val();
		if ($rootScope.getUsersRoleId == '1') {
			$rootScope.getUsersRegioId = '';
			$rootScope.getUsersRouteId = '';

		//if both region and route are selected, disable de region (causes bug in back-end)
		} else if ($('#get-users-region-select').val() && $('#get-users-route-select').val()) {
			$rootScope.getUsersRouteId = $('#get-users-route-select').val();
			$rootScope.getUsersRegioId = '';
		} else {
			$rootScope.getUsersRegioId = $('#get-users-region-select').val();
			$rootScope.getUsersRouteId = $('#get-users-route-select').val();
		}
		
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:$rootScope.getUsersRegioId,routeId:$rootScope.getUsersRouteId,roleId:$rootScope.getUsersRoleId}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.allUsersDetails = response.users;

						//Reset pagination/search
						$timeout(function() {
							//Set new users variable
							$rootScope.usersFiltered = $rootScope.allUsersDetails;

							//Set search input to empty + reset search
							$rootScope.usersSearchInput = '';
							$('.search-all-users').val('');
							if ($rootScope.userHasSearched) {
								delete $rootScope.userHasSearched;
							}

							//Generate/reset user list
							$scope.recalculatePagination();
						});
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	});//End get users

	//Get pagination numbers
	$rootScope.usersPageSize = 50;
	$scope.setCurrentPage = function(event,currentPage) {
	    $rootScope.currentUsersPage = currentPage;
	    $('.pagination-button').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of users
		$(".menuContent").scrollTo(".allUsersList");
	}
	$scope.getNumberAsArray = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPages = function() {
	    return Math.ceil($rootScope.usersFiltered.length / $rootScope.usersPageSize);
	};

	//Set the pagination button active based on the current page variable
	$scope.getCurrentPaginationPage = function() {
		$('.pagination-button').removeClass('active');
		$timeout(function() {
			$('.pagination-button').each(function() {
				if ($(this).text() == ($rootScope.currentUsersPage + 1)) {
					$(this).addClass('active');
				}
			});
		});
	}

	//Recalculate amount of pages needed on loaded users/search, so the pagination doesn't show too many buttons
	$scope.recalculatePagination = function() {
		//Set active pagination button to first
		$rootScope.currentUsersPage = 0;
		$('.pagination-button').removeClass('active');
		$('.pagination-button').first().addClass('active');
		
		//Get pagination amount based on loaded (filtered) users
		$rootScope.usersPaginationAmount = $scope.getNumberAsArray($scope.getNumberAsArray(Math.ceil($rootScope.usersFiltered.length / $rootScope.usersPageSize)));	
	}

	//Get users through search
	$scope.searchUsersFunction = function() {
		//Get search input
		$rootScope.usersSearchInput = $('.search-all-users').val();

		//If input is empty
		if ($rootScope.usersSearchInput.replace(/\s+/g, '') == '' || !$('.search-all-users').val()) {
			//Get full users list (still based on filter)
			$rootScope.usersFiltered = $rootScope.allUsersDetails;

			//Set search input to empty (in case the user entered several spaces, makes the placeholder show again)
			$rootScope.usersSearchInput = '';
			$('.search-all-users').val('');

			//Reset user list
			$scope.recalculatePagination();
		} else if ($rootScope.usersSearchInput.replace(/\s+/g, '') != '' || $('.search-all-users').val()) {
			//If input is not empty
			//Set search to active
				$rootScope.userHasSearched = true;

				//Set results array
				$rootScope.usersFiltered = [];

				//Get each keyword from the search seperate and add them to an array
				var searchKeywords = $('.search-all-users').val().split(' ');

				//Strip empty spaces from keywords
				var keyword = searchKeywords.length;    
				while(keyword--) !/\S/.test(searchKeywords[keyword]) && searchKeywords.splice(keyword, 1);

				//Add space between keyword zipcode (9999AA, four numbers + two letters) to make 9999 AA (can find all postcodes, even when known in database as 9999AA)
				for (var i in searchKeywords) {
					var postcode = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
					if (postcode.test(searchKeywords[i])) {
						searchKeywords.push(searchKeywords[i][4] + searchKeywords[i][5]);
						searchKeywords[i] = searchKeywords[i].substr(0, searchKeywords[i].length - 2);
					}
				}

				//Set counter, to check if the amount of found results is equal to the amount of searched keywords
				var searchUsersKeywordCounter = 0;

				//Add praktijken to array where search keywords are all found in the user object
				for (var i in $rootScope.allUsersDetails) {
					keywordloop:
					for (var j in searchKeywords) {
						userloop:
						for (var k in $rootScope.allUsersDetails[i]) {
							if ($rootScope.allUsersDetails[i][k] && $rootScope.allUsersDetails[i][k] != '' && $rootScope.allUsersDetails[i][k] != null && $rootScope.allUsersDetails[i][k] != undefined && typeof $rootScope.allUsersDetails[i][k] != 'array' && typeof $rootScope.allUsersDetails[i][k] != 'object') {
								var userObjectItem = $rootScope.allUsersDetails[i][k].toLowerCase();
								var searchKeyword = searchKeywords[j].toLowerCase();
								if ([k] != 'notitie' && userObjectItem.indexOf(searchKeyword) != -1) {
									searchUsersKeywordCounter += 1;
									if (searchUsersKeywordCounter === searchKeywords.length) {
										//Push praktijk with all keywords found to array
										$rootScope.usersFiltered.push($rootScope.allUsersDetails[i]);
										//Reset keyword counter (for next praktijk in loop)
										searchUsersKeywordCounter = 0;
										break userloop;
									}
									break;
								}
							}
						}
					}
					//Reset keyword counter on every praktijk
					searchUsersKeywordCounter = 0;
				}

				//Reset user list
				$scope.recalculatePagination();
			}
	}//End get users through search

}])



































 






/* ---------- RAPPORTAGE ---------- */
.controller('Rapportage', ['$scope', '$rootScope', '$timeout', '$state', function($scope, $rootScope, $timeout, $state) {

	/* ========== Password function for Symbiant zekenhuizen ========== */
	$scope.$watch('loggedInUserRegion', function() {
		if ($rootScope.role == 'laboratorium' && $rootScope.loggedInUserRegion == '3') {
			swal({
			  title: 'Voer uw unieke pincode in',
			  input: 'password',
			  confirmButtonText: 'Bevestig',
			  showCancelButton: true,
			  showLoaderOnConfirm: true,
			  confirmButtonColor: '#007868',
			  cancelButtonClass: 'cancel-sym-password',
			  cancelButtonText: 'Annuleer',
			  preConfirm: function (password) {
			    return new Promise(function (resolve, reject) {
			      setTimeout(function() {
			        if (password !== '4823' && password !== '3806' && password !=='5729' ) {
			          reject('Pincode onjuist.')
			        } else {
			          resolve()
			        }
			      }, 2000)
			    })
			  },
			  allowOutsideClick: false
			}).then(function (password) {
			  //If correct password
			  //Set selected ziekenhuis for Amsterdamse Ziekenhuizen
			  if (password === '4823') {
			  	$scope.selectSymZiekenhuis = 'SPA ';
			  } else if (password === '3806') {
			  	$scope.selectSymZiekenhuis = 'SYM';
			  } else if (password === '5729') {
			  	$scope.selectSymZiekenhuis = '';
			  }
			})
		}
	});

	$(document).on('click', '.cancel-sym-password', function() {
		//Go back to route page if password is canceled
		$state.go('menu.route');
	});
	
	/* ========== End password function for Symbiant Ziekenhuizen ========== */

	//Hide pagination by default
	$rootScope.showRapportagePagination = false;
	$rootScope.showKlachtenPagination = false;

	//Reset total koerier scans count (opgehaald)
	$rootScope.koerierScansTotal = 0;

	//Reset total laboratorium scans count (afgeleverd)
	$rootScope.laboratoriumScansTotal = 0;

	//Save route/region filter in rootScope
	$rootScope.$watch('loggedInUserRegion', function (newValue, oldValue, scope) {
		if (!$rootScope.rapportageRegionFilter) {
			$rootScope.rapportageRegionFilter = $rootScope.loggedInUserRegion;
			$scope.loggedInUserRegion = $rootScope.rapportageRegionFilter;
		}
	});

	//Remove the route variable when no route is selected ('Alle routes'). This will prevent the koerier list from being empty
	$scope.removeRouteVariable = function() {
		if ($('.rapportage-route-filter').val() == '') {
			delete $scope.selectRouteRapportage;
		}
	}

	//Remove the koerier variable when no koerier is selected
	$scope.removeKoerierVariable = function() {
		if ($('.rapportage-koerier-filter').val() == '') {
			delete $scope.selectKoerierRapportage;
		}
	}

	//Get all koeriers
	if (!$rootScope.allKoeriers) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:'4'}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.allKoeriers = response.users;
					});

				} else if (response.state.result == 'Empty') {
					//Reset koeriers if none are found
					$timeout(function() {
						$rootScope.allKoeriers = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}
	//End get all koeriers

	//Get the date range and set scope vars for the start and end date, then show the rapportage result
	$scope.getExport = function(region,route,koerier) {
		//Scroll generated tables back to top (reset position)
		$('.datagrid').scrollTop(0);

		//Start total koerier scans count (opgehaald)
		$rootScope.koerierScansTotal = 0;

		//Start total laboratorium scans count (afgeleverd)
		$rootScope.laboratoriumScansTotal = 0;

		//Save route/region filter in rootScope
		if ($('.rapportage-route-filter').val()) {
			$rootScope.rapportageRouteFilter = $('.rapportage-route-filter').val();
		} else {
			delete $rootScope.rapportageRouteFilter;
		}
		if ($('.rapportage-region-filter').val()) {
			$rootScope.rapportageRegionFilter = $('.rapportage-region-filter').val();
		} else {
			delete $rootScope.rapportageRegionFilter;
		}
		if ($('.rapportage-koerier-filter').val()) {
			$rootScope.rapportageKoerierFilter = $('.rapportage-koerier-filter').val();
		} else {
			delete $rootScope.rapportageKoerierFilter;
		}

		//Reset show-pagination buttons
		$('.down-arrow-button span').removeClass('active');
		$('.down-arrow-button span').html('&#x2193;');

		//Reset rapportage data
		delete $rootScope.planningExportDetails;
		delete $rootScope.klachtenRapportage;
		delete $rootScope.rapportageHuisartsenpraktijken;

		//Rootscope date, for returning visit to the page
		$rootScope.selectRapportageDateStart = $scope.selectRapportageDateStart;
		$rootScope.selectRapportageDateEnd = $scope.selectRapportageDateEnd;

		//Parse input date
		$scope.dateStart = $.reverseDate(Date.parse($scope.selectRapportageDateStart));
		$scope.dateEnd = $.reverseDate(Date.parse($scope.selectRapportageDateEnd));

		//Set variable for JSON data to get from database (for GetPlanning)
		if (koerier && koerier.length) {
			var rapportageData = JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:$scope.dateStart,einddatum:$scope.dateEnd,routeId:'',planningId:'',koerierId:koerier,regionId:''}});
		} else if (route && route.length) {
			var rapportageData = JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:$scope.dateStart,einddatum:$scope.dateEnd,routeId:route,planningId:'',koerierId:'',regionId:''}});
		} else if (region && region.length) {
			var rapportageData = JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:$scope.dateStart,einddatum:$scope.dateEnd,routeId:'',planningId:'',koerierId:'',regionId:region}});
		} else {
			var rapportageData = JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:$scope.dateStart,einddatum:$scope.dateEnd,routeId:'',planningId:'',koerierId:'',regionId:''}});
		}

		//Get planning details
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + rapportageData,
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.planningExportDetails = response.planning;

						//Add items to array loop
						for (var i in $rootScope.planningExportDetails) {

							//Add route names
							for (var r in $rootScope.planningExportDetails[i].route_id) {
								for (var ro in $rootScope.routesDetails) {
									if ($rootScope.routesDetails[ro].routeId == $rootScope.planningExportDetails[i].route_id) {
										$rootScope.planningExportDetails[i]['routeNaam'] = $rootScope.routesDetails[ro].routeNaam;
									}
								}
							}

							//Set empty object items to null
							for (var n in $rootScope.planningExportDetails[i]) {
								if (n == 'bestellingen' && $rootScope.planningExportDetails[i][n] == '') {
									$rootScope.planningExportDetails[i][n] = 'none';
								} else if (n != 'bestellingen' && $rootScope.planningExportDetails[i][n] == '') {
									$rootScope.planningExportDetails[i][n] = null;
								}
							}

							//Set kits delivered amount per praktijk per date var
							var kitsDeliveredAmount = 0;

							//Get total amount kits delivered per praktijk per date
							if ($rootScope.planningExportDetails[i].bestellingen != 'none') {
								for (var k in $rootScope.planningExportDetails[i].bestellingen) {
									for (var kObject in $rootScope.planningExportDetails[i].bestellingen[k]) {
										if ($rootScope.planningExportDetails[i].bestellingen[k][kObject] != '') {
											kitsDeliveredAmount = kitsDeliveredAmount + parseInt($rootScope.planningExportDetails[i].bestellingen[k][kObject].aantal);
										}
									}
								}
							}

							//Add amount of kits delivered to export array per praktijk per date
							$rootScope.planningExportDetails[i]['kitsTotal'] = kitsDeliveredAmount;


							//Get region id from route id and put it in the planning export array objects
							var targetRoute = $rootScope.planningExportDetails[i].route_id;
							for (var j in $rootScope.routesDetails) {
								if ($rootScope.routesDetails[j].routeId == targetRoute) {
									$rootScope.planningExportDetails[i]['regionId'] = $rootScope.routesDetails[j].regionId;
									//Add region name to planningExportDetails array
									for (var re in $rootScope.regionsDetails) {
										if ($rootScope.regionsDetails[re].id == $rootScope.routesDetails[j].regionId) {
											$rootScope.planningExportDetails[i]['regionName'] = $rootScope.regionsDetails[re].region;
										}
									}
								}
							}

							//Add no material reason to planningExportDetails array
							for (var r in $rootScope.planningExportDetails[i].noMateriaalGivenReason) {
								for (var ro in $rootScope.geenmateriaalreden) {
									if ($rootScope.geenmateriaalreden[ro].reden_id == $rootScope.planningExportDetails[i].noMateriaalGivenReason) {
										$rootScope.planningExportDetails[i]['noMateriaalGivenReason_string'] = $rootScope.geenmateriaalreden[ro].reden;
									}
								}
							}

							//Add afwijkende verzendmethode to planningExportDetails array
							for (var r in $rootScope.planningExportDetails[i].afwijkendeVerzendmethode) {
								for (var ro in $rootScope.verzendmethodes) {
									if ($rootScope.verzendmethodes[ro].verzendmethode_id == $rootScope.planningExportDetails[i].afwijkendeVerzendmethode) {
										$rootScope.planningExportDetails[i]['afwijkendeVerzendmethode_string'] = $rootScope.verzendmethodes[ro].verzendmethode;
									}
								}

								//Add afwijkende verzendmethode reasons to planningExportDetails
								if ($rootScope.planningExportDetails[i].afwijkendeVerzendmethode == '2') {
									//Spoedenvelop
									for (var sp in $rootScope.spoedenvelop) {
										if ($rootScope.spoedenvelop[sp].spoedenvelop_id == $rootScope.planningExportDetails[i].afwijkendeVerzendmethodeReason) {
											$rootScope.planningExportDetails[i]['afwijkendeVerzendmethodeReason_string'] = $rootScope.spoedenvelop[sp].reden;
										}
									}
								} else if ($rootScope.planningExportDetails[i].afwijkendeVerzendmethode == '3') {
									//Bezemwagen
									for (var be in $rootScope.bezemwagen) {
										if ($rootScope.bezemwagen[be].bezemwagen_id == $rootScope.planningExportDetails[i].afwijkendeVerzendmethodeReason) {
											$rootScope.planningExportDetails[i]['afwijkendeVerzendmethodeReason_string'] = $rootScope.bezemwagen[be].reden;
										}
									}
								} else {
									$rootScope.planningExportDetails[i]['afwijkendeVerzendmethodeReason_string'] = '-';
								}
							}

							//Convert ordered kits to string
							for (var u in $rootScope.planningExportDetails[i].bestellingen) {
								$rootScope.planningExportDetails[i]['bestellingen_string'] = '';
								for (var l in $rootScope.planningExportDetails[i].bestellingen[u]) {
									for (var r in $rootScope.kitTypes) {
										if ($rootScope.planningExportDetails[i].bestellingen[u][l].KitId == $rootScope.kitTypes[r].KitId) {
											$rootScope.planningExportDetails[i]['bestellingen_string'] += 'Kittype: ' + $rootScope.kitTypes[r].kitType + '\nAantal: ' + $rootScope.planningExportDetails[i].bestellingen[u][l].aantal + '\n\n';
										}
									}
								}
							}
						}//End loop

						//Add route names to users' routeId array
						for (var u in $rootScope.usersDetails) {
							for (var r in $rootScope.usersDetails[u].routeId) {
								for (var ro in $rootScope.routesDetails) {
									if ($rootScope.routesDetails[ro].routeId == $rootScope.usersDetails[u].routeId[r].id) {
										$rootScope.usersDetails[u].routeId[r]['routeNaam'] = $rootScope.routesDetails[ro].routeNaam;
									}
								}
							}
						}

						/* ========== PLANNING RAPPORTAGE PAGINATION ========== */
						$rootScope.planningRapportagePageSize = 50;
						//Hide pagination by default
						$rootScope.showRapportagePagination = false;
						$rootScope.setCurrentPagePlanningRapportage = function(event,currentPage) {
						    $rootScope.currentPlanningRapportagePage = currentPage;
						    $('.pagination-button-planning-rapportage').removeClass('active');
							$(event.currentTarget).addClass('active');

							//Scroll back to top of users
							$(".datagrid").scrollTop(0);
						}
						$rootScope.getNumberAsArrayPlanningRapportage = function (num) {
						    return new Array(num);
						};
						$rootScope.numberOfPagesPlanningRapportage = function() {
						    return Math.ceil($rootScope.planningExportDetails.length / $rootScope.planningRapportagePageSize);
						};
						//Set the pagination button active based on the current page variable
						$rootScope.getCurrentPaginationPagePlanningRapportage = function() {
							$('.pagination-button-planning-rapportage').removeClass('active');
							$timeout(function() {
								$('.pagination-button-planning-rapportage').each(function() {
									if ($(this).text() == ($rootScope.currentPlanningRapportagePage + 1)) {
										$(this).addClass('active');
									}
								});
							});
						}
						//Recalculate amount of pages needed on loaded users/search, so the pagination doesn't show too many buttons
						$rootScope.recalculatePaginationPlanningRapportage = function() {
							//Set active pagination button to first
							$rootScope.currentPlanningRapportagePage = 0;
							$('.pagination-button-planning-rapportage').removeClass('active');
							$('.pagination-button-planning-rapportage').first().addClass('active');
							
							//Get pagination amount based on loaded (filtered) users
							$rootScope.planningRapportagePaginationAmount = $rootScope.getNumberAsArrayPlanningRapportage($scope.getNumberAsArrayPlanningRapportage(Math.ceil($rootScope.planningExportDetails.length / $rootScope.planningRapportagePageSize)));
						}
						$rootScope.recalculatePaginationPlanningRapportage();

						$rootScope.planningRapportagePaginationActive = function(event) {
							$(event.currentTarget).find('span').toggleClass('active');
							if ($(event.currentTarget).find('span').hasClass('active')) {
								$rootScope.showRapportagePagination = true;
								$(event.currentTarget).find('span').html('&#x2191;');
							} else {
								$rootScope.showRapportagePagination = false;
								$(event.currentTarget).find('span').html('&#x2193;');
							}
						}
						/* ========== END PLANNING RAPPORTAGE PAGINATION ========== */

						/* ========== EXPORT PLANNING TO CSV ========== */
						// prepare CSV data
						$rootScope.exportPlanningToCsv = function() {
							var csvData = new Array();
								csvData.push('"Datum";"Regio";"Route";"Koerier";"Huisartsenpraktijk";"Materiaal opgehaald";"Reden geen materiaal opgehaald";"Kits afgeleverd";"Kits afgeleverd details";"Afwijkende verzendmethode";"Reden afwijkende verzendmethode";"Notitie";"Monsters opgehaald";"Monsters afgeleverd"');
								$scope.planningExportDetailsFiltered.forEach(function(item, index, array) {
									//Check if variables are null or undefined

									//Get materiaal opgehaald value
									if (item.materiaal_opgehaald == '1') {
										var materiaalOpgehaald = 'Ja';
									} else if (item.materiaal_opgehaald == '0') {
										var materiaalOpgehaald = 'Nee';
									} else if (item.materiaal_opgehaald == undefined || item.materiaal_opgehaald == null) {
										var materiaalOpgehaald = '-';
									}

									//Get kits afgeleverd value
									if (item.kits_afgeleverd == '1') {
										var kitsAfgeleverd = 'Ja';
									} else if (item.kits_afgeleverd == '0') {
										var kitsAfgeleverd = 'Nee';
									} else if (item.kits_afgeleverd == undefined || item.kits_afgeleverd == null) {
										var kitsAfgeleverd = '-';
									}

									//Check no materiaal opgehaald reason
									if (item.noMateriaalGivenReason_string == undefined || item.noMateriaalGivenReason_string == null) {
										var noMateriaalGivenReason = '-';
									} else {
										var noMateriaalGivenReason = item.noMateriaalGivenReason_string;
									}
									if (item.anders == undefined || item.anders == null) {
										var noMateriaalGivenReasonElse = '';
									} else {
										var noMateriaalGivenReasonElse = item.anders;
									}

									//Check afwijkende verzendmethode
									if (item.afwijkendeVerzendmethode_string == undefined || item.afwijkendeVerzendmethode_string == null) {
										var afwijkendeVerzendmethode = '-';
									} else {
										var afwijkendeVerzendmethode = item.afwijkendeVerzendmethode_string;
									}
									if (item.afwijkendeVerzendmethodeReason_string == undefined || item.afwijkendeVerzendmethodeReason_string == null) {
										var afwijkendeVerzendmethodeReason = '-';
									} else {
										var afwijkendeVerzendmethodeReason = item.afwijkendeVerzendmethodeReason_string;
									}

									//Check notitie
									if (item.notitie == undefined || item.notitie == null) {
										var notitie = '-';
									} else {
										var notitie = item.notitie;
									}

									//Monsters opgehaald (koerier scans)
									if (item.koerierScan == undefined || item.koerierScan == null) {
										var koerierScan = '-';
									} else {
										var koerierScan = item.koerierScan;
									}

									//Monsters afgeleverd (laboratorium scans)
									if (item.laboratoriumScan == undefined || item.laboratoriumScan == null) {
										var laboratoriumScan = '-';
									} else {
										var laboratoriumScan = item.laboratoriumScan;
									}

								  csvData.push('"' + $rootScope.convertDatabaseDate(item.datum) + '";"' + item.regionName + '";"' + item.routeNaam + '";"' + item.koerier + '";"' + item.huisartsenpraktijk + '";"' + materiaalOpgehaald + '";"' + noMateriaalGivenReason + ' ' + noMateriaalGivenReasonElse + '";"' + kitsAfgeleverd + '";"' + item.bestellingen_string + '";"' + afwijkendeVerzendmethode + '";"' + afwijkendeVerzendmethodeReason + '";"' + notitie + '";"' + koerierScan + '";"' + laboratoriumScan + '"');
							});
							// download stuff
							var fileName = "Commpanionz route rapportage " + $.date(Date.parse($scope.selectRapportageDateStart)) + ' - ' + $.date(Date.parse($scope.selectRapportageDateEnd)) + ".csv";
							var buffer = csvData.join("\n");
							var blob = new Blob([buffer], {
							  "type": "text/csv;charset=utf8;"			
							});
							var link = document.createElement("a");
										
							if(link.download !== undefined) { // feature detection
							  // Browsers that support HTML5 download attribute
							  link.setAttribute("href", window.URL.createObjectURL(blob));
							  link.setAttribute("download", fileName);
							} else if(navigator.msSaveBlob) { // IE 10+
								  link.setAttribute("href", "#");
								  link.addEventListener("click", function(event) {
								    navigator.msSaveBlob(blob, fileName);
								  }, false);
							} else {
							  // it needs to implement server side export
							  link.setAttribute("href", "https://www.bvobaarmoederhals.nl/export");
							}
							link.innerHTML = "Export to CSV";
							link.click();
						}
						/* ========== END EXPORT PLANNING TO CSV ========== */

						//Get percentages of materiaal opgehaald vs praktijk visited
						$rootScope.getPercentagesMateriaalOpgehaald();
					});
				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});

		//Set variable for JSON data to get from database (for GetMeldingen (klachten))
		if (koerier && koerier.length) {
			var klachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:$scope.dateStart,einddatum:$scope.dateEnd,klacht_user_id:koerier,klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}});
		} else if (route && route.length) {
			var klachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:route,regionId:'',datum:'',startdatum:$scope.dateStart,einddatum:$scope.dateEnd,klacht_user_id:'',klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}});
		} else if (region && region.length) {
			var klachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:region,datum:'',startdatum:$scope.dateStart,einddatum:$scope.dateEnd,klacht_user_id:'',klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}});
		} else {
			var klachtenData = JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:'',ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:$scope.dateStart,einddatum:$scope.dateEnd,klacht_user_id:'',klacht_van_user_id:'',typemelding:'1',meldingId:'',ShowOntvangers:''}});
		}

		//Get klachten for selected period
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + klachtenData,
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.klachtenRapportage = response.data.meldingen;

						for (var k in $rootScope.klachtenRapportage) {

							//Set empty object items to null
							for (var n in $rootScope.klachtenRapportage[k]) {
								if ($rootScope.klachtenRapportage[k][n] == '') {
									$rootScope.klachtenRapportage[k][n] = null;
								}
							}

							//Add route names to klachtenRapportage's routeId array
							for (var r in $rootScope.klachtenRapportage[k].routeId) {
								for (var ro in $rootScope.routesDetails) {
									if ($rootScope.routesDetails[ro].routeId == $rootScope.klachtenRapportage[k].routeId) {
										$rootScope.klachtenRapportage[k]['routeNaam'] = $rootScope.routesDetails[ro].routeNaam;
									}
								}
							}

							//Add region names to klachtenRapportage array
							for (var r in $rootScope.klachtenRapportage[k].regionId) {
								for (var re in $rootScope.regionsDetails) {
									if ($rootScope.regionsDetails[re].id == $rootScope.klachtenRapportage[k].regionId) {
										$rootScope.klachtenRapportage[k]['regionName'] = $rootScope.regionsDetails[re].region;
									}
								}
							}

							//Add klacht_user names to klachtenRapportage array
							for (var r in $rootScope.klachtenRapportage[k].klacht_user_id) {
								for (var u in $rootScope.usersDetails) {
									if ($rootScope.usersDetails[u].user_id == $rootScope.klachtenRapportage[k].klacht_user_id) {
										$rootScope.klachtenRapportage[k]['klacht_user_name'] = $rootScope.usersDetails[u].identity;
									}
								}
							}

							//Add klacht_van_user names to klachtenRapportage array
							for (var r in $rootScope.klachtenRapportage[k].klacht_van_user_id) {
								for (var u in $rootScope.usersDetails) {
									if ($rootScope.usersDetails[u].user_id == $rootScope.klachtenRapportage[k].klacht_van_user_id) {
										$rootScope.klachtenRapportage[k]['klacht_van_user_name'] = $rootScope.usersDetails[u].identity;
									}
								}
							}

							//Add verzender names to klachtenRapportage array
							for (var r in $rootScope.klachtenRapportage[k].verzender) {
								for (var u in $rootScope.usersDetails) {
									if ($rootScope.usersDetails[u].user_id == $rootScope.klachtenRapportage[k].verzender) {
										$rootScope.klachtenRapportage[k]['verzender_name'] = $rootScope.usersDetails[u].identity;
									}
								}
							}

							//Add reden klacht string to klachtenRapportage array
							for (var r in $rootScope.klachtenRapportage[k].redenklacht) {
								for (var u in $rootScope.klachtReasons) {
									if ($rootScope.klachtReasons[u].klachtId == $rootScope.klachtenRapportage[k].redenklacht) {
										$rootScope.klachtenRapportage[k]['redenklacht_string'] = $rootScope.klachtReasons[u].klachtType;
									}
								}
							}

						}//End loop

						/* ========== KLACHTEN RAPPORTAGE PAGINATION ========== */
						$rootScope.klachtenRapportagePageSize = 50;
						//Hide pagination by default
						$rootScope.showKlachtenPagination = false;
						$rootScope.setCurrentPageKlachtenRapportage = function(event,currentPage) {
						    $rootScope.currentKlachtenRapportagePage = currentPage;
						    $('.pagination-button-klachten-rapportage').removeClass('active');
							$(event.currentTarget).addClass('active');

							//Scroll back to top of users
							$(".datagrid").scrollTop(0);
						}
						$rootScope.getNumberAsArrayKlachtenRapportage = function (num) {
						    return new Array(num);
						};
						$rootScope.numberOfPagesKlachtenRapportage = function() {
						    return Math.ceil($rootScope.klachtenRapportage.length / $rootScope.klachtenRapportagePageSize);
						};
						//Set the pagination button active based on the current page variable
						$rootScope.getCurrentPaginationPageKlachtenRapportage = function() {
							$('.pagination-button-klachten-rapportage').removeClass('active');
							$timeout(function() {
								$('.pagination-button-klachten-rapportage').each(function() {
									if ($(this).text() == ($rootScope.currentKlachtenRapportagePage + 1)) {
										$(this).addClass('active');
									}
								});
							});
						}
						//Recalculate amount of pages needed on loaded users/search, so the pagination doesn't show too many buttons
						$rootScope.recalculatePaginationKlachtenRapportage = function() {
							//Set active pagination button to first
							$rootScope.currentKlachtenRapportagePage = 0;
							$('.pagination-button-klachten-rapportage').removeClass('active');
							$('.pagination-button-klachten-rapportage').first().addClass('active');
							
							//Get pagination amount based on loaded (filtered) users
							$rootScope.klachtenRapportagePaginationAmount = $rootScope.getNumberAsArrayKlachtenRapportage($scope.getNumberAsArrayKlachtenRapportage(Math.ceil($rootScope.klachtenRapportage.length / $rootScope.klachtenRapportagePageSize)));
						}
						$rootScope.recalculatePaginationKlachtenRapportage();

						$rootScope.klachtenRapportagePaginationActive = function(event) {
							$(event.currentTarget).find('span').toggleClass('active');
							if ($(event.currentTarget).find('span').hasClass('active')) {
								$rootScope.showKlachtenPagination = true;
								$(event.currentTarget).find('span').html('&#x2191;');
							} else {
								$rootScope.showKlachtenPagination = false;
								$(event.currentTarget).find('span').html('&#x2193;');
							}
						}
						/* ========== END KLACHTEN RAPPORTAGE PAGINATION ========== */

						/* ========== EXPORT KLACHTEN TO CSV ========== */
						// prepare CSV data
						$rootScope.exportKlachtenToCsv = function() {
							var csvData = new Array();
								csvData.push('"Datum";"Tijd";"Type melding";"Regio";"Route";"Klacht door";"Klacht over";"Verstuurd door";"Reden klacht";"Bericht";"Follow-ups"');
								$scope.klachtenRapportageFiltered.forEach(function(item, index, array) {
								  csvData.push('"' + $rootScope.convertDatabaseDate(item.datum) + '";"' + item.tijd + '";"' + 'Klacht' + '";"' + item.regionName + '";"' + item.routeNaam + '";"' + item.klacht_van_user_name + '";"' + item.klacht_user_name + '";"' + item.verzender_name + '";"' + item.redenklacht_string + ' ' + item.anders + '";"' + item.bericht + '";"' + item.followups_string + '"');
							});
							// download stuff
							var fileName = "Commpanionz klachten rapportage " + $.date(Date.parse($scope.selectRapportageDateStart)) + ' - ' + $.date(Date.parse($scope.selectRapportageDateEnd)) + ".csv";
							var buffer = csvData.join("\n");
							var blob = new Blob([buffer], {
							  "type": "text/csv;charset=utf8;"			
							});
							var link = document.createElement("a");
										
							if(link.download !== undefined) { // feature detection
							  // Browsers that support HTML5 download attribute
							  link.setAttribute("href", window.URL.createObjectURL(blob));
							  link.setAttribute("download", fileName);
							} else if(navigator.msSaveBlob) { // IE 10+
								  link.setAttribute("href", "#");
								  link.addEventListener("click", function(event) {
								    navigator.msSaveBlob(blob, fileName);
								  }, false);
							} else {
							  // it needs to implement server side export
							  link.setAttribute("href", "https://www.bvobaarmoederhals.nl/export");
							}
							link.innerHTML = "Export to CSV";
							link.click();
						}
						/* ========== END EXPORT KLACHTEN TO CSV ========== */
					});
				}
			},
			error: function(response) {
				console.log(response);	
			}
		});//End get klachten for selected period

		//Get all huisartsenpraktijken based on selected region/route
		if ($('.rapportage-route-filter').val() || $('.rapportage-region-filter').val()) {
			if (!$('.rapportage-route-filter').val()) {
				route = '';
			}
			if (!$('.rapportage-region-filter').val()) {
				region = '';
			}
			if ($('.rapportage-route-filter').val() && $('.rapportage-region-filter').val()) {
				region = '';
			}

			$.ajax({
				method:'POST',
				dataType:'json',
				data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:region,routeId:route,roleId:'2'}}}),
				url:$rootScope.ajaxURL,
				success: function(response) {

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});

						if (response.state.message == 'Session Expired') {
							$rootScope.removeSession();
						}

					} else if (response.state.result == 'Success') {
						$timeout(function() {
							$rootScope.rapportageHuisartsenpraktijken = response.users;
						});

					} else if (response.state.result == 'Empty') {
						$rootScope.rapportageHuisartsenpraktijken = '';
					}
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
		//End get all huisartsenpraktijken based on selected region/route

		//Show export table
		$('#dvData').fadeIn();

		//Make table heads fixed
		$(".datagrid table").tableHeadFixer();

		//Show rapportage
		$('#rapportage').fadeIn();
	}

    //Toggle rapportage card
    $scope.toggleRapportageItemHeight = function(event) {
		$(event.currentTarget).parents('.rapportage-card').toggleClass('full-height');
		if ($(event.currentTarget).parents('.rapportage-card').hasClass('full-height')) {
			$(event.currentTarget).find('span').text('-');
			//$(event.currentTarget).qtip({content:{text:'Kaart sluiten'},show:{event:false,ready:true},hide:false,position:{my:'top center',at:'bottom center',effect:false,adjust:{y:5}},style:{tip:{corner:'top center'}}});
		} else {
			$(event.currentTarget).find('span').text('+');
			//$(event.currentTarget).qtip('destroy');
		}
	}

	//Get percentages of praktijken that gave 'materiaal'
	//In template: <!-- AANTAL PRAKTIJKEN MATERIAAL OPGEHAALD PERCENTAGES  -->
	$rootScope.getPercentagesMateriaalOpgehaald = function() {
		$rootScope.percentage0to20 = 0;
		$rootScope.percentage20to40 = 0;
		$rootScope.percentage40to60 = 0;
		$rootScope.percentage60to80 = 0;
		$rootScope.percentage80to100 = 0;

		$timeout(function() {
			$('.praktijken-bezocht').each(function() {
				var percentageMateriaalOpgehaald = $(this).find('.percentage-materiaal-opgehaald').text().replace(/ /g,'').replace('%','');

				if (percentageMateriaalOpgehaald >= 0 && percentageMateriaalOpgehaald < 20) {
					$rootScope.percentage0to20 += 1;
				} else if (percentageMateriaalOpgehaald >= 20 && percentageMateriaalOpgehaald < 40) {
					$rootScope.percentage20to40 += 1;
				} else if (percentageMateriaalOpgehaald >= 40 && percentageMateriaalOpgehaald < 60) {
					$rootScope.percentage40to60 += 1;
				} else if (percentageMateriaalOpgehaald >= 60 && percentageMateriaalOpgehaald < 80) {
					$rootScope.percentage60to80 += 1;
				} else if (percentageMateriaalOpgehaald >= 80 && percentageMateriaalOpgehaald <= 100) {
					$rootScope.percentage80to100 += 1;
				}
			});

			//Set vars for chart
			$rootScope.chartLabels = ['0-20%', '20-40%', '40-60%', '60-80%', '80-100%'];
			$rootScope.chartData = [$rootScope.percentage0to20, $rootScope.percentage20to40, $rootScope.percentage40to60, $rootScope.percentage60to80, $rootScope.percentage80to100];
		});
	}

	/* ========== LOAD MORE FUNCTIONS, FOR LAZY LOADING CONTENT IN THE RAPPORTAGE PAGE ========== */

}])

//Get total kits amount per praktijk
.controller('GetRapportageFollowups', ['$scope','$rootScope', '$timeout', function($scope, $rootScope, $timeout) {
	//Get follow-ups
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetFollowup",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{meldingId:$scope.item.meldingId}}),
		url:$rootScope.ajaxURL,
		success: function(response) {			

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.followUps = response.data.followups;

					//Add follow-up strings to klachtenRapportage array
					for (var k in $rootScope.klachtenRapportage) {
						$rootScope.klachtenRapportage[k]['followups_string'] = '';
						for (var u in $scope.followUps) {
							if ($scope.followUps[u].meldingId == $rootScope.klachtenRapportage[k].meldingId) {
								//Get user sender name from follow-up
								for (var r in $rootScope.usersDetails) {
									if ($scope.followUps[u].user_id == $rootScope.usersDetails[r].user_id) {
										$rootScope.klachtenRapportage[k]['followups_string'] += 'Datum: ' + $rootScope.convertDatabaseDate($scope.followUps[u].datum) + '\nVan: ' + $rootScope.usersDetails[r].identity + '\nBericht: ' + $scope.followUps[u].bericht + '\n\n';
									}
								}
							}
						}
					}
				});
			}
		},
		error: function(response) {
			console.log(response);	
		}
	});
	//End get follow-ups
}])

//Get scans for every praktijk
.controller('GetPraktijkScans', ['$scope','$rootScope', '$timeout', function($scope,$rootScope,$timeout) {
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetScans",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{RouteId:"",praktijkId:$scope.item.praktijk_id,barCode:"",scanKoerierDatum:$scope.item.datum,datumvan:"",datumtot:""}}),
		url:$rootScope.ajaxURL,
		success: function(response) {
			if (response.scans) {
				$timeout(function() {
					$scope.monstersScanned = response.scans;

					//Add amount of scanned by koerier to planning details (for rapportage)
					var countKoerierScans = 0;
					for(var i = 0; i < $scope.monstersScanned.length; i++){
					    if($scope.monstersScanned[i].koerierScan == '1') {
					        countKoerierScans++;

					        //Add to total koerier scans
					        $rootScope.koerierScansTotal += 1;
					    }
					}
					$scope.$apply(function() {
						$scope.item['koerierScan'] = countKoerierScans;
					});

					//Add amount of scanned by laboratorium to planning details (for rapportage)
					var countLaboratoriumScans = 0;
					for(var i = 0; i < $scope.monstersScanned.length; i++){
					    if($scope.monstersScanned[i].laboratoriumScan == '1') {
					        countLaboratoriumScans++;

					        //Add to total laboratorium scans
					        $rootScope.laboratoriumScansTotal += 1;
					    }
					}
					$scope.$apply(function() {
						$scope.item['laboratoriumScan'] = countLaboratoriumScans;
					});
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}])

//Get total kits amount per praktijk
.controller('totalKitsAmountPerPraktijk', ['$scope','$rootScope', function($scope, $rootScope) {
	//Get total amount of kits per praktijk for all dates summed up
	$scope.kitsTotalSummed = 0;
	$scope.setTotal = function(kitsTotalSummed) {
		$scope.kitsTotalSummed += parseInt(kitsTotalSummed);
	}
}])

//Get if praktijk has kits (is in planningExportDetailsHasKits array)
.controller('ifPraktijkHasKits', ['$scope','$rootScope', function($scope, $rootScope) {
	
	//Set var if praktijk has kits, so it can be excluded in the rapportage where praktijken do not have kits
	$scope.hasKits = function(praktijkId) {
		var id = $scope.planningExportDetailsHasKits.length + 1;
		var found = $scope.planningExportDetailsHasKits.some(function (el) {
			return el.praktijk_id === praktijkId;
		});

		if (found) { 
			$scope.praktijkHasKits = true;
		}
	}
}])

//Total amount of kits delivered
.controller('totalAmountOfKitsDelivered', ['$scope','$rootScope', function($scope, $rootScope) {
	$scope.calculateTotal = function(kitsTotalAmount){
		return kitsTotalAmount.reduce( function(totalKits, kitDeliveredDate){
	      return totalKits + kitDeliveredDate.kitsTotal
	    }, 0);
	}
}])



































/* ---------- ADRESWIJZIGINGEN ---------- */
.controller('Adreswijzigingen', ['$scope', '$rootScope', '$timeout', '$state', function($scope, $rootScope, $timeout, $state) {
	
	//User updates/changes are AJAX loaded in the menu controller, so the logged in user can see a badge in the user updates/changes menu item containing the number of unprocessed changes
	//This means the changes do not have to be loaded again in this controller

	//toggle update item
	$scope.toggleUpdateItemHeight = function(event) {
		$(event.currentTarget).parents('.update-card').toggleClass('full-height');
		if ($(event.currentTarget).parents('.update-card').hasClass('full-height')) {
			$(event.currentTarget).find('span').text('-');
			//$(event.currentTarget).qtip({content:{text:'Kaart sluiten'},show:{event:false,ready:true},hide:false,position:{my:'top center',at:'bottom center',effect:false,adjust:{y:5}},style:{tip:{corner:'top center'}}});
		} else {
			$(event.currentTarget).find('span').text('+');
			//$(event.currentTarget).qtip('destroy');
		}
	}

	//Update adresgegevens
	$scope.saveUserChanges = function(id) {
		$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"ProcessUpdateRequest",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{updateId:id}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				swal({
				  title: 'Gelukt!',
				  text: 'Gebruikersgegevens zijn gewijzigd.',
				  type: 'success',
				  confirmButtonColor: '#009999'
				});

				//Reload view to see changes
				$state.reload();
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
	}//End update adresgegevens

	//Delete update
    $scope.deleteUpdate = function(id) {
    	swal({
			title: "Verwijderen", 
			text: "Weet je zeker dat je deze wijziging wilt verwijderen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#e74c3c", 
			confirmButtonText: "Verwijder"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DestroyUpdateRequest",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{updateId:id}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'Wijziging is verwijderd.',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});

							//Reload view to see changes
							$state.reload();
						}
					},
					error: function(response) {
						console.log(response);
					}
				});
			});
    }//End delete update
}])

//Get update user
.controller('getUpdateUser', ['$scope', '$rootScope', '$timeout', function($scope, $rootScope, $timeout) {
	//Get user details for user to be updated
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.update.verzender}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.updateUserDetails = response.users;
				});
			}

		},
		error: function(response) {
			console.log(response);
		}
	});//End get user details for user to be updated

	//Get user details for user that requested the update
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.update.invoke_id}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Fail') {
				swal({
				  title: 'Oops...',
				  text: response.state.message,
				  type: 'error',
				  confirmButtonColor: '#009999'
				});

				if (response.state.message == 'Session Expired') {
					$rootScope.removeSession();
				}

			} else if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.updateUserDetailsRequester = response.users;
				});
			}

		},
		error: function(response) {
			console.log(response);
		}
	});//End get user details for user that requested the update
}])






























/* ---------- SYSTEEMLOG ---------- */
.controller('Systeemlog', ['$scope', '$rootScope', '$timeout', '$state', function($scope, $rootScope, $timeout, $state) {

	//Get log
	$scope.getLog = function() {

		//Parse input date
		$scope.dateStart = $.reverseDate(Date.parse($scope.selectLogDateStart));
		$scope.dateEnd = $.reverseDate(Date.parse($scope.selectLogDateEnd));

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetLog",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:$scope.dateStart,einddatum:$scope.dateEnd,userId:""}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.systeemLog = response.data.aktiviteiten;
					});
				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Oops...',
					  text: 'Geen data gevonden!',
					  type: 'error',
					  confirmButtonColor: '#009999'
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});

		//Show log container
		$('#logContainer').fadeIn(0);

	}//End get log

	//Exort to CSV
	function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td),tr:has(th)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '";"',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td,th');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // This must be a hyperlink
    $(".export-log").on('click', function (event) {
        // CSV
        exportTableToCSV.apply(this, [$('.systeem-log>table'), 'Commpanionz systeem log ' + $.date(Date.parse($scope.selectLogDateStart)) + ' - ' + $.date(Date.parse($scope.selectLogDateEnd)) +'.csv']);
        
        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
}])




























/* ---------- KIT SETTINGS ---------- */
.controller('KitSettings', ['$scope', '$rootScope', '$timeout', '$state', function($scope, $rootScope, $timeout, $state) {

	//Get users for specific kit
	$scope.getKitUsers = function(kitId,regioId,routeId) {

		//Do not accept undefined
		if (routeId == undefined) {
			routeId = '';
		}
		if (regioId == undefined) {
			regioId = '';
		}

		//Only route OR region can be selected
		if (routeId != '') {
			regioId = '';
		} else if (regioId != '') {
			routeId = '';
		}

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUserKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:'',regioId:regioId,routeId:routeId,kitId:kitId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					
					//Set users in variable
					$scope.$apply(function() {
						if ($.isEmptyObject(response.users[0])) {
							$scope.kitUsers = [];
						} else {
							$scope.kitUsers = response.users;
							
							//Calculate pagination
							$scope.paginationNumberForKitUsers = $scope.numberOfPages();
						}
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Oops...',
					  text: 'Geen data gevonden!',
					  type: 'error',
					  confirmButtonColor: '#009999'
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get users for specific kit

	

	/////////////////////////////////////////
    /////// START KITUSERS PAGINATION //////
    ///////////////////////////////////////
	$scope.kitUsersPageSize = 50;
	$scope.currentKitUsersPage = 0;
	$scope.setCurrentPage = function(event,currentPage) {
		$scope.currentKitUsersPage = currentPage;
	    $('.pagination-button').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of users
		$(".menuContent").scrollTo(".kitUsersListAnchor");
	}
	$scope.numberOfPagesAsArray = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPages = function() {
	    return $scope.numberOfPagesAsArray(Math.ceil($scope.kitUsers.length / $scope.kitUsersPageSize));
	};
	/////////////////////////////////////////
    //////// END KITUSERS PAGINATION ///////
    ///////////////////////////////////////

	

	 //Delete kit from user(s)
    $scope.deleteUserKits = function(userId,regioId,routeId,kitId) {
    	swal({
			title: "Verwijderen", 
			text: "Weet je zeker dat je deze kit van deze huisartsenpraktijk wilt loskoppelen?", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#e74c3c", 
			confirmButtonText: "Verwijder"
			}).then(function(){
				$.ajax({
					method:'POST',
					dataType:'json',
					data:'data=' + JSON.stringify({init:{module:"DeleteUserKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,regioId:regioId,routeId:routeId,kitId:kitId}}),
					url:$rootScope.ajaxURL,
					success: function(response) {

						if (response.state.result == 'Fail') {
							swal({
							  title: 'Oops...',
							  text: response.state.message,
							  type: 'error',
							  confirmButtonColor: '#009999'
							});

							if (response.state.message == 'Session Expired') {
								$rootScope.removeSession();
							}

						} else if (response.state.result == 'Success') {
							swal({
							  title: 'Gelukt!',
							  text: 'De kit is verwijderd bij de gebruiker(s).',
							  type: 'success',
							  confirmButtonColor: '#009999'
							});

							//Remove user from kitUsers array
							for(var i = 0; i < $scope.kitUsers.length; i++) {
							    var obj = $scope.kitUsers[i];

							    if(userId.indexOf(obj.userid) !== -1) {
							    	$scope.$apply(function() {
							        	$scope.kitUsers.splice(i, 1);
							        	i--;
							        });
							    }
							}

						}

					},
					error: function(response) {
						console.log(response);
					}
				});
			}
		);
    }//End delete kit from user(s)




}])






;