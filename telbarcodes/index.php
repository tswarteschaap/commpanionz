<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////////// DATABASE MUTATIE ///////// ///////////////////
//BY MARTIJN WENNEKES, 'T SWARTE SCHAAP, HEERLEN, NL//
////////////////////////20-2-2018//////////////////////////////
//////////////////////////////////////////////////////////////
*/

// LINKT DATABASE
require_once 'includes/init.php';

$results = $db->query("SELECT bar_code FROM monster_scans");
$row = $results->fetchAll(PDO::FETCH_ASSOC);

foreach( $row as $value ){
	$barcode = $value['bar_code'];
	$stringlength = strlen($barcode);

	if ($stringlength == 8){
		echo $barcode . ' ----- LENGTE 8 <br><br>';
	} elseif ($stringlength == 0){
		echo $barcode . ' ----- LENGTE 0 <br><br>';
	} elseif ($stringlength == 10){
		echo $barcode . ' ----- LENGTE 10 <br><br>';
	} elseif ($stringlength == 11){
		echo $barcode . ' ----- LENGTE 11 <br><br>';
	} elseif ($stringlength == 13){
		echo $barcode . ' ----- LENGTE 13 <br><br>';
	} elseif ($stringlength == 14){
		echo $barcode . ' ----- LENGTE 14 <br><br>';
	} elseif ($stringlength == 19){
		echo $barcode . ' ----- LENGTE 19 <br><br>';
	} 
}

?>

