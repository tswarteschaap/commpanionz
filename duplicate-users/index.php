<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////// DATABASE DUPLICATES CHECK ////////////////////////
//////// BY NIELS KERSIC, 'T SWARTE SCHAAP, HEERLEN, NL ////////
/////////////////////// 19-3-2018 /////////////////////////////
//////////////////////////////////////////////////////////////
*/

// LINKT DATABASE
require_once 'includes/init.php';

$results = $db->query("SELECT identity FROM users WHERE user_id IN (SELECT user_id FROM user_routes WHERE route_id IN (104,110, 111, 112, 113))");
$row = $results->fetchAll(PDO::FETCH_ASSOC);

foreach( $row as $value ){
	$identity = $value['identity'];
  
  $results = $db->query("SELECT * FROM users WHERE identity = '$identity'");
  $row = $results->fetchAll(PDO::FETCH_ASSOC);
  $count = $results->rowCount();
  if ($count > 1) {
    foreach ($row as $value) {
      $row_identity = $value['identity'];
      $row_id = $value['user_id'];
      $row_postcode = $value['postcode'];
      $row_city = $value['city'];
      echo '<b>Identity: </b>' . $row_identity;
      echo ' <b>ID: </b>' . $row_id;
      echo ' <b>City: </b>' . $row_city;
      echo ' <b>Postcode: </b>' . $row_postcode;
      
      echo '<br />';
    }
  } 
}

?>

