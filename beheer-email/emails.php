<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
///////////////////// ADD EMAILS TO USERS ///////////////////////
///////BY NIELS KERSIC, 'T SWARTE SCHAAP, HEERLEN, NL///////////
////////////////////////20-3-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/

require_once 'includes/init.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


$mail = new PHPMailer(true); 
$GLOBALS['mail'] = $mail;


if (isset($_POST[action]) && isset($_POST[loggedInUser])) {
  
  $action = $_POST[action];

  $GLOBALS['db'] = $db;
  $GLOBALS['loggedInUser'] = $_POST[loggedInUser];

  if(isset($_POST[emailToDelete])){
    $GLOBALS['emailToDelete'] = $_POST[emailToDelete];
  }

  if(isset($_POST[emailToAdd])){
    $GLOBALS['emailToAdd'] = $_POST[emailToAdd];
  }

  
  // GET ALL EMAIL ADDRESSES FOR THIS USER
  function getUserEmails(){
    $db = $GLOBALS['db'];
    $user = $GLOBALS['loggedInUser'];
    
    // // Haal de emailadressen op die bij de ingelogde praktijk horen
    $results = $db->query("SELECT email FROM praktijk_email WHERE user_id = '$user' ");
    $row = $results->fetchAll(PDO::FETCH_ASSOC);

    $emailArray = [];

    foreach($row as $value) {
      $email = $value;
      array_push($emailArray, $email);
    }

    echo '{"state": "success", "email": ' . json_encode($emailArray) . '}';
  }


  // REMOVE GIVEN EMAIL FROM TABLE
  function removeUserEmail(){
    $db = $GLOBALS['db'];
    $user = $GLOBALS['loggedInUser'];    
    $emailToDelete = $GLOBALS['emailToDelete'];

    $sql = "DELETE FROM praktijk_email WHERE email = '$emailToDelete' AND user_id = '$user'";
		$removeEmail = $db->prepare($sql);						
    $removeEmail->execute();
  
    echo '{"state": "deleted"}';

  }
  
  function sendActivationMail($email, $token) {
    $db = $GLOBALS['db'];
    $user = $GLOBALS['loggedInUser'];  
    $mail = $GLOBALS['mail'];  

    // Get name for user in database
    $results = $db->query("SELECT identity FROM users WHERE user_id = '$user' ");
    $row = $results->fetchAll(PDO::FETCH_ASSOC);
    $identity = $row[0][identity];

    $linkVoorBevestiging = "https://bvobaarmoederhals.nl/beheer-email/?user=".$user."&email=".$email."&token=".$token;

    try {
        //Server settings
        // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        // $mail->ClearAddresses(); // remove previous email addresses
        $mail->IsSMTP(); // telling the class to use SMTP
				$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
				$mail->Host       = "mail.antagonist.nl";      // sets GMAIL as the SMTP server
				$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "noreply@bvobaarmoederhals.nl";  // GMAIL username
				$mail->Password   = "0Myjmk";            // GMAIL password
				$mail->SetFrom('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				$mail->AddReplyTo("noreply@bvobaarmoederhals.nl","BVO Baarmoederhalskanker Mailer (Commpanionz)");
				$mail->Subject    = "Uw emailadres is toegevoegd voor " . $identity;
				$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->addAddress($email);     // Add a recipient
        
        $message = "Uw e-mailadres is zojuist toegevoegd aan de praktijk '.$identity.'. Dit betekent dat u na het bevestigen van uw e-mailadres ook mailtjes zult ontvangen waarin u aan kunt geven of er de volgende dag een koerier moet komen. <br />
                    Wanneer u dit liever niet heeft, kunt u deze mail negeren en zult u verder geen berichten meer ontvangen. <br /><br /><a href='".$linkVoorBevestiging."' style='width:200px; height: 75px; padding:10px 20px; text-decoration: none; background-color: #009999; border-radius: 5px; color:#ffffff;'>Activeer uw e-mailadres</a>";

        //Content
        $mail->Body    = 		    					 '<!DOCTYPE html>
        <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
        <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
        <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
        <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Commpanionz mailer</title>
        <meta name="description" content="Commpanionz mailer">
        <meta name="keywords" content="">

        <!-- Mobile viewport -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

        <link rel="shortcut icon" href="images/favicon.ico"  type="image/x-icon">

        <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/normalize.css">
        <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/js/flexslider/flexslider.css">
        <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/basic-style.css">

        <!-- end CSS-->
            
        <!-- JS-->
        <script src="js/libs/modernizr-2.6.2.min.js"></script>
        <!-- end JS-->
        </style>
        </head>

        <body id="home">
          
        <!-- header area -->
            <header class="wrapper clearfix">
                    
                <div id="banner">        
                  <div id="logo"><img src="http://www.bvobaarmoederhals.nl/img/logo.png" alt="logo"></div> 
                </div>
          
            </header><!-- end header -->
          
        <!-- main content area -->   
        <div id="main" class="wrapper">
            
        <!-- content area -->    
          <section id="content" class="wide-content">
              <div class="row">	
                
                    <p>'.$message.'</p>
                
            </div><!-- end row -->
          </section><!-- end content area -->   
        <!-- footer area -->    
        <footer>
          <div id="colophon" class="wrapper clearfix">
              Contactgegevens: Commpanionz - Zomerkade 102 - 1273 SP - Huizen - The Netherlands - Telefoon: +31 341 495 254 - Website: www.commpanionz.org - Mail: info@commpanionz.org
            </div>
        </footer><!-- #end footer area --> 
        <!-- jQuery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script>window.jQuery || document.write("<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>")</script>

        <script defer src="js/flexslider/jquery.flexslider-min.js"></script>

        <!-- fire ups - read this file!  -->   
        <script src="js/main.js"></script>

        </body>
        </html>';
        $mail->isHTML(true);                                  // Set email format to HTML
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->send();
    } 
        catch (Exception $e) {
          echo $e;
    }
  }

  // REMOVE GIVEN EMAIL FROM TABLE
  function addUserEmail(){
    $db = $GLOBALS['db'];
    $user = $GLOBALS['loggedInUser'];    
    $emailToAdd = $GLOBALS['emailToAdd'];

    $token = RAND();

    $sql = "INSERT INTO praktijk_email (user_id, email, token) VALUES ('$user', '$emailToAdd', '$token');";
		$addEmail = $db->prepare($sql);						
    $addEmail->execute();
  
    sendActivationMail($emailToAdd, $token);

  }

  switch ($action) {
    case "getUserEmails":
      getUserEmails();
      break;
    case 'removeUserEmail':
      removeUserEmail();
      break;
    case 'addUserEmail':
      addUserEmail();
      break;
    default:
      echo '{"state": "fail"}';
  }



}


?>
