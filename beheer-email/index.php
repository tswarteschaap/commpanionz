<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
///////////////// MAILFUNCTIE - ADRES TOEVOEGEN /////////////////
////////// NIELS KERSIC, 'T SWARTE SCHAAP, HEERLEN, NL//////////
////////////////////////20-3-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/

// LINKT DATABASE
require_once 'includes/init.php';

// HAALT VARIABELEN UIT DE URL
$user_id = $_GET['user'];
$email = $_GET['email'];
$token = $_GET['token'];


// TELT LENGTE VAN VARIABELEN
$user_id_count = strlen($user_id);
$email_count = strlen($email);
$token_count = strlen($token);

// GEEFT FOUTMELDING INDIEN ÉÉN OF MEER VARIABELEN NIET (CORRECT) IN DE URL AANWEZIG ZIJN
if(($user_id_count > 0) && ($email_count > 0) && ($token_count > 0)){

  // Get name for user in database
  $results = $db->query("SELECT token FROM praktijk_email WHERE user_id = '$user_id' AND email = '$email'");
  $row = $results->fetchAll(PDO::FETCH_ASSOC);
  $row_token = $row[0][token];

  if($row_token !== $token){
    echo '<html>
		<head>
			<title>Bevestig e-mailadres</title>

			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <link rel="stylesheet" href="https://bvobaarmoederhals.nl/beheer-email/style.css">

		</head>
		<body>

			<div id="page-container">
				<main>
					<span class="top-icon">
						<i class="material-icons warn">report</i>
						<h1> Er ging iets fout </h1>
					</span>

					<div class="card-content">

						Er is iets mis gegaan tijdens het bevestigen van uw e-mailadres. Probeer het nog eens, of neem contact op met een beheerder..
						<hr />
						<img src="https://bvobaarmoederhals.nl/staging/app/img/logo.png">
					</div>
				</main>
			</div>

		</body>
		</html>';
  }else{

    $sql = "UPDATE praktijk_email SET confirmation = '1' WHERE user_id = '$user_id' AND email = '$email'";
    $extra_email = $db->prepare($sql);
    $extra_email->execute() or die(msql_error());
    
    // Get name for user in database
    $results = $db->query("SELECT identity FROM users WHERE user_id = '$user_id' ");
    $row = $results->fetchAll(PDO::FETCH_ASSOC);
    $identity = $row[0][identity];

    echo '
      <html>
      <head>
        <title>Bevestig e-mailadres</title>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://bvobaarmoederhals.nl/beheer-email/style.css">


      </head>
      <body>

        <div id="page-container">
          <main>
            <span class="top-icon">
              <i class="material-icons">check</i>
              <h1> Gelukt </h1>
            </span>

            <div class="card-content">

              U bent succesvol aangemeld voor <b>' . $identity . '</b><br />
              Deze pagina kan nu worden gesloten.
              <hr />
              <img src="https://bvobaarmoederhals.nl/staging/app/img/logo.png">
            </div>
          </main>
        </div>

      </body>
      </html>
    ';

  }


} else {

	echo '
		<html>
		<head>
			<title>Bevestig e-mailadres</title>

			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <link rel="stylesheet" href="https://bvobaarmoederhals.nl/beheer-email/style.css">

		</head>
		<body>

			<div id="page-container">
				<main>
					<span class="top-icon">
						<i class="material-icons warn">report</i>
						<h1> Er ging iets fout </h1>
					</span>

					<div class="card-content">

						Er is iets mis gegaan tijdens het bevestigen van uw e-mailadres. Probeer het nog eens, of neem contact op met een beheerder..
						<hr />
						<img src="https://bvobaarmoederhals.nl/staging/app/img/logo.png">
					</div>
				</main>
			</div>

		</body>
		</html>
	';

}


?>

