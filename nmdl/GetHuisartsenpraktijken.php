<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
///GET ALL HUISARTSENPRAKTIJKEN THAT BELONG TO THE NMDL REGION///
///////BY ROGIER WIJNANDS, 'T SWARTE SCHAAP, HEERLEN, NL////////
////////////////////////20-1-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/
/*
echo "hallo";
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
require_once 'includes/init.php';

$results = 	$db->query("SELECT users.identity, users.user_id, users.postcode, users.address, users.city FROM users WHERE users.region_id = '2' and users.role_id = '2' ORDER BY users.identity ASC");
/*
echo "results -> ";
print_r($results);
echo "<br><br><br>";
*/
$num_rows = $results->rowCount();
$row = $results->fetchAll(PDO::FETCH_ASSOC);
/*
echo "row -> ";
print_r($row);
echo "<br><br><br>";
*/
foreach($row as $field) {
	
	$details[] = array(
		'huisartsenpraktijk' 	=>	$field['identity'],
		'adres' 				=>	utf8_encode($field['address']),
		'plaatsnaam' 			=>	$field['city'],
		'postcode' 				=>	$field['postcode'],
		'id'					=> $field['user_id']
	);
}
/*
echo "details -> ";
print_r($details);
echo "<br><br><br>";
*/
echo json_encode($details);

?>