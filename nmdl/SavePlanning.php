<?php 
/*
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
///////////SET PRAKTIJK TO AFWEZIG ON NEXT VISIT DATE////////////
///////BY ROGIER WIJNANDS, 'T SWARTE SCHAAP, HEERLEN, NL////////
////////////////////////20-1-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/

require_once 'includes/init.php';

if (isset($_POST['user_id'])) {
	$user_id = $_POST['user_id'];

	//Check if user exists in database
	$user_exists_query = $db->query("SELECT 1 FROM users WHERE users.user_id = '$user_id'");
	$user_exists_count = count($user_exists_query->fetchAll(PDO::FETCH_ASSOC));

	if ($user_exists_count == 0) {
		echo '{"response":"Deze praktijk bestaat niet in onze database."}';
	} else if ($user_exists_count > 0) {
		//Check which day the route of the user will be driven
		//Check if the input date is at least a day before the day the route will be driven
			//Get route from user
			$user_route_id_query = $db->query("SELECT user_routes.route_id FROM user_routes WHERE user_routes.user_id = '$user_id'");
			$user_route_id_row = $user_route_id_query->fetchAll(PDO::FETCH_ASSOC);
			$user_route_id = str_replace('"', "", json_encode($user_route_id_row[0]['route_id']));
			//echo 'User route id is: '. $user_route_id;

			//Get day of route
			$route_vaste_dag_query = $db->query("SELECT routes.vaste_dag FROM routes WHERE routes.route_id = '$user_route_id'");
			$route_vaste_dag_row = $route_vaste_dag_query->fetchAll(PDO::FETCH_ASSOC);
			$route_vaste_dag = str_replace('"', "", json_encode($route_vaste_dag_row[0]['vaste_dag']));

			//Convert dutch visit date to English date
			if ($route_vaste_dag == 'maandag') {
				$route_vaste_dag = 'monday';
			} else if ($route_vaste_dag == 'dinsdag') {
				$route_vaste_dag = 'tuesday';
			} else if ($route_vaste_dag == 'woensdag') {
				$route_vaste_dag = 'wednesday';
			} else if ($route_vaste_dag == 'donderdag') {
				$route_vaste_dag = 'thursday';
			} else if ($route_vaste_dag == 'vrijdag') {
				$route_vaste_dag = 'friday';
			} else if ($route_vaste_dag == 'zaterdag') {
				$route_vaste_dag = 'saturday';
			} else if ($route_vaste_dag == 'zondag') {
				$route_vaste_dag = 'sunday';
			}
			//echo '<br />Route vaste dag is: '. $route_vaste_dag;

			//Get current day
			$date = date('Y-m-d');
			$timestamp = strtotime($date);
			$days = array('sunday', 'monday', 'tuesday', 'wednesday','thursday','friday','saturday');
			$currentWeekDayNumber = date( "w", $timestamp);
			$currentWeekDay = $days[$currentWeekDayNumber];
			//echo '<br />De dag van vandaag is: '.$currentWeekDay;

			//Get date of next visit
			$nextVisitDateTimestamp = strtotime('next '.$route_vaste_dag);
			$nextVisitDate = date('Y-m-d', $nextVisitDateTimestamp);
			$nextVisitDateWeekLaterTimestamp = $nextVisitDateTimestamp + (86400 * 7);
			$nextVisitDateWeekLater = date('Y-m-d', $nextVisitDateWeekLaterTimestamp);
			//echo '<br />De volgende bezoekdag is: '.$nextVisitDate;
			//echo '<br />Het bezoek een week later vindt plaats op: '.$nextVisitDateWeekLater;

		//If at least the day before, set afwezig on next visit. If the day before or later, set afwezig a week later		
			//Check if next visit date is at least a day away
			$dayDifference = ($nextVisitDateTimestamp - $timestamp) / (60 * 60 * 24);
			if ($dayDifference > 0) {
				$databaseDate = str_replace('-', "", $nextVisitDate);
				//echo '<br />De database datum is: '.$databaseDate;

				//Check if user is already planned, if so, update value. If not, set value
				$afwezig_exists_query = $db->query("SELECT 1 FROM aanwezig WHERE aanwezig.user_id = '$user_id' and aanwezig.datum = '$databaseDate'");
				$afwezig_exists_count = count($afwezig_exists_query->fetchAll(PDO::FETCH_ASSOC));

				if ($afwezig_exists_count == 0) {
					$db->query("INSERT INTO aanwezig (`user_id`, `datum`, `afwezig`) VALUES ('$user_id', '$databaseDate', '1')");
				} else if ($afwezig_exists_count > 0) {
					$db->query("UPDATE aanwezig set afwezig = '1' WHERE datum = '$databaseDate' and user_id = '$user_id'");
				}

				echo '{"response":"De praktijk is afgemeld voor het bezoek van de koerier op '.date('d-m-Y', $nextVisitDateTimestamp).'"}';
			} else if ($dayDifference == 0) {
				//If visit date is less than a day away, add afwezigheid a week later
				$databaseDate = str_replace('-', "", $nextVisitDateWeekLater);
				//echo '<br />De database datum is: '.$databaseDate;

				//Check if user is already planned, if so, update value. If not, set value
				$afwezig_exists_query = $db->query("SELECT 1 FROM aanwezig WHERE aanwezig.user_id = '$user_id' and aanwezig.datum = '$databaseDate'");
				$afwezig_exists_count = count($afwezig_exists_query->fetchAll(PDO::FETCH_ASSOC));

				if ($afwezig_exists_count == 0) {
					$db->query("INSERT INTO aanwezig (`user_id`, `datum`, `afwezig`) VALUES ('$user_id', '$databaseDate', '1')");
				} else if ($afwezig_exists_count > 0) {
					$db->query("UPDATE aanwezig set afwezig = '1' WHERE datum = '$databaseDate' and user_id = '$user_id'");
				}

				echo '{"response":"De praktijk is afgemeld voor het bezoek van de koerier op '.date('d-m-Y', $nextVisitDateWeekLaterTimestamp).'"}';
			}
		}
	} else {
		echo '{"response":"U heeft niet de juiste gegevens verstuurd. Controleer of u alle velden hebt ingevuld."}';
	}

?>