<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////////// GET BARCODE SCANNED BY LAB ///////////////////
///////BY MARTIJN WENNEKES, 'T SWARTE SCHAAP, HEERLEN, NL////////
////////////////////////20-1-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/
/*
echo "hallo";
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
require_once 'includes/init.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


// $results = 	$db->query("SELECT users.identity, users.user_id, users.postcode, users.address, users.city FROM users WHERE users.region_id = '2' and users.role_id = '2' ORDER BY users.identity ASC");
$mail = new PHPMailer(true); 
$date = date('Ymd');
$dateHistory = new DateTime('-3 days');
$dateHistoryDisplay = $dateHistory->format('Ymd');
// $count = 0;
// $mismatches = [];
// $testarray = array();

$array_geslaagd = array();
$array_koerierfaal = array();
$array_labfaal = array();
$array_notexist = array();
$array_monstermissing = array();
$testarray = array();


$succesmail_headers = array("Barcode", "Praktijk", "Route", "Sessiekenmerk");
$succesmail_array = array();
array_push($succesmail_array, $succesmail_headers);

if (isset($_POST['sessie'])){

	$sessie_array = $_POST['sessie'];	
	$sessie_array_decode = json_decode($sessie_array, true);
	$route_id_totaal = $sessie_array_decode[0]['route_id'];
	$sessie_kenmerk_totaal = $sessie_array_decode[0]['sessie_kenmerk'];
  // array_push($testarray, $sessie_kenmerk_totaal);
  

  // GET REGION ID BASED ON ROUTE ID
  $results = $db->query("SELECT region_id FROM routes WHERE route_id = '$route_id_totaal' ");
  $row = $results->fetchAll(PDO::FETCH_ASSOC);
  $region = $row[0][region_id];

  // GET EMAIL OF LAB IN REGION
  $results = $db->query("SELECT email FROM users WHERE region_id = '$region' AND role_id = '3'");
  $row = $results->fetchAll(PDO::FETCH_ASSOC);
  $succesmail_lab_mail = $row[0][email];




		// $testpush = array_search($barcode_totaal, array_column($sessie_array_decode, 'bar_code'));
		// array_push($testarray, $testpush);
	$barcodecount_totaal = 0;
	$results_totaal = $db->query("SELECT * FROM monster_scans WHERE route_id = '$route_id_totaal' AND scan_koerier_datum BETWEEN '$dateHistoryDisplay' AND '$date' AND sessieKenmerk = '$sessie_kenmerk_totaal' ");
	foreach ($results_totaal as $result_totaal){
		$barcode_totaal = $result_totaal['bar_code'];



		$testpush = array_search($barcode_totaal, array_column($sessie_array_decode, 'bar_code'));		
		array_push($testarray, $testpush);


		if(array_search($barcode_totaal, array_column($sessie_array_decode, 'bar_code')) === false){
			array_push($array_monstermissing, $barcode_totaal);
		}
	}



	foreach ($sessie_array_decode as $value){

    $temp_array = array();

		$route_id = $value['route_id'];
		$bar_code = $value['bar_code'];
		$praktijk_id = $value['praktijk_id'];
		$sessie_kenmerk = $value['sessie_kenmerk'];
		

		$results = $db->query("SELECT * FROM monster_scans WHERE bar_code = '$bar_code' AND sessieKenmerk = '$sessie_kenmerk' ");
		$row = $results->fetchAll(PDO::FETCH_ASSOC);
		$row_barcode = $row[0][bar_code];
		$row_route_id = $row[0][route_id];
		$row_praktijk_id = $row[0][praktijk_id];
		$row_koeriercheck = $row[0][koerier_scan];
		$row_laboratoriumcheck = $row[0][laboratorium_scan];
    $row_sessie_kenmerk = $row[0][sessieKenmerk];


    // GET REGION ID BASED ON ROUTE ID
    $results = $db->query("SELECT name FROM routes WHERE route_id = '$route_id' ");
    $row = $results->fetchAll(PDO::FETCH_ASSOC);
    $route_name = $row[0][name];

    // GET REGION ID BASED ON ROUTE ID
    $results = $db->query("SELECT identity FROM users WHERE user_id = '$praktijk_id' ");
    $row = $results->fetchAll(PDO::FETCH_ASSOC);
    $identity = $row[0][identity];

		// array_push($testarray, $row_laboratoriumcheck);

		if(($row_koeriercheck == 1) && ($row_laboratoriumcheck == 1)){
      array_push($array_geslaagd, $row_barcode);
      
      array_push($temp_array, $bar_code);
      array_push($temp_array, $identity);
      array_push($temp_array, $route_name);
      array_push($temp_array, $sessie_kenmerk);

      array_push($succesmail_array, $temp_array);
		
		} elseif (($row_koeriercheck != 1) && ($row_laboratoriumcheck == 1)){
			array_push($array_koerierfaal, $row_barcode);

		} elseif (($row_koeriercheck == 1) && ($row_laboratoriumcheck != 1)){
			array_push($array_labfaal, $row_barcode);

		} elseif (($row_koeriercheck != 1) && ($row_laboratoriumcheck != 1)){
			array_push($array_notexist, $row_barcode);

		}

	}




	// ========================================
	// CHECK OF EEN KOERIERSCAN MIST IN DE ARRAY
	// ========================================
	$count_koerierfaal = count($array_koerierfaal);
	if($count_koerierfaal >= 1){
		foreach($array_koerierfaal as $value){

			$bar_code = $value;
			$results_barcode = $db->query("SELECT monster_scans.praktijk_id, monster_scans.route_id, monster_scans.bar_code FROM monster_scans WHERE bar_code = '$bar_code'");
			$row_barcode = $results_barcode->fetchAll(PDO::FETCH_ASSOC);
			$route_id = $row_barcode[0][route_id];
			$praktijk_id = $row_barcode[0][praktijk_id];

			// RESULTATEN VOOR DE ROUTE
			$results_route = $db->query("SELECT * FROM routes WHERE route_id = '$route_id'");
			$row_route = $results_route->fetchAll(PDO::FETCH_ASSOC);
			$route_name = $row_route[0][name];
			$route_region = $row_route[0][region_id];
			$route_vaste_dag = $row_route[0][vaste_dag];

			// RESULTATEN VOOR DE REGION (NAAM)
			$results_region = $db->query("SELECT regions.id, regions.region FROM regions WHERE id = '$route_region'");
			$row_region = $results_region->fetchAll(PDO::FETCH_ASSOC);
			$region = $row_region[0][region];

			// RESULTATEN VOOR DE BETREFFENDE PRAKTIJK
			$results_praktijk = $db->query("SELECT users.identity, users.email, users.address, users.postcode, users.city, users.telephone FROM users WHERE user_id = '$praktijk_id' ");
			$row_count_praktijk = $results_praktijk->rowCount();

			if($row_count_praktijk > 0){

				$row_praktijk = $results_praktijk->fetchAll(PDO::FETCH_ASSOC);
				$praktijk_name = $row_praktijk[0][identity];
				$praktijk_email = $row_praktijk[0][email];
				$praktijk_adres = $row_praktijk[0][address];
				$praktijk_postcode = $row_praktijk[0][postcode];
				$praktijk_city = $row_praktijk[0][city];
				$praktijk_telefoon = $row_praktijk[0][telephone];

				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>koerierscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					<b>Praktijk:</b> <br/>
				    					Naam: ".$praktijk_name." <br/>
				    					Email: ".$praktijk_email." <br/>
				    					Adres: ".$praktijk_adres." <br/>
				    					Postcode: ".$praktijk_postcode." <br/>
				    					Stad: ".$praktijk_city." <br/>
				    					Telefoon:  ".$praktijk_telefoon." ";
				
			} else {
				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>koerierscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					 ";
			}

		try {
				    //Server settings
				    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'mail.antagonist.nl';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'noreply@bvobaarmoederhals.nl';                 // SMTP username
				    $mail->Password = '0Myjmk';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `tls` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->ClearAllRecipients();
            
				    $mail->setFrom('info@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
				    $mail->addAddress('ruud@commpanionz.org');
				    $mail->addAddress('suzanne@2mc.nl');
				    $mail->addAddress('toon@2mc.nl');
				    $mail->addAddress('barcodes@tswarteschaap.nl');
				    $mail->addReplyTo('info@bvobaarmoederhals.nl', 'Information');
				    // $mail->addCC('cc@example.com');
				    // $mail->addBCC('bcc@example.com');

				    //Attachments
				    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->Subject = 'Er zijn monsters geconstateerd waarbij de koerierscan mist';
				    $mail->Body    = $mailbody;
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->AltBody = 'Er zijn monsters geconstateerd waarbij de koerierscan mist.';
				    $mail->send();
				} 
				    catch (Exception $e) {
				}

		}
	} 
	// ===============================================
				// EINDE KOERIERCHECK
	// ===============================================







	// ========================================================
	// CHECK OF EEN LABORATORIUM SCAN MIST IN DE ARRAY
	// ========================================================
	$count_labfaal = count($array_labfaal);
	if($count_labfaal >= 1){
		foreach($array_labfaal as $value){

			$bar_code = $value;
			$results_barcode = $db->query("SELECT monster_scans.praktijk_id, monster_scans.route_id, monster_scans.bar_code FROM monster_scans WHERE bar_code = '$bar_code'");
			$row_barcode = $results_barcode->fetchAll(PDO::FETCH_ASSOC);
			$route_id = $row_barcode[0][route_id];
			$praktijk_id = $row_barcode[0][praktijk_id];

			// RESULTATEN VOOR DE ROUTE
			$results_route = $db->query("SELECT * FROM routes WHERE route_id = '$route_id'");
			$row_route = $results_route->fetchAll(PDO::FETCH_ASSOC);
			$route_name = $row_route[0][name];
			$route_region = $row_route[0][region_id];
			$route_vaste_dag = $row_route[0][vaste_dag];

			// RESULTATEN VOOR DE REGION (NAAM)
			$results_region = $db->query("SELECT regions.id, regions.region FROM regions WHERE id = '$route_region'");
			$row_region = $results_region->fetchAll(PDO::FETCH_ASSOC);
			$region = $row_region[0][region];

			// RESULTATEN VOOR DE BETREFFENDE PRAKTIJK
			$results_praktijk = $db->query("SELECT users.identity, users.email, users.address, users.postcode, users.city, users.telephone FROM users WHERE user_id = '$praktijk_id' ");
			$row_count_praktijk = $results_praktijk->rowCount();

			if($row_count_praktijk > 0){

				$row_praktijk = $results_praktijk->fetchAll(PDO::FETCH_ASSOC);
				$praktijk_name = $row_praktijk[0][identity];
				$praktijk_email = $row_praktijk[0][email];
				$praktijk_adres = $row_praktijk[0][address];
				$praktijk_postcode = $row_praktijk[0][postcode];
				$praktijk_city = $row_praktijk[0][city];
				$praktijk_telefoon = $row_praktijk[0][telephone];

				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>laboratoriumscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					<b>Praktijk:</b> <br/>
				    					Naam: ".$praktijk_name." <br/>
				    					Email: ".$praktijk_email." <br/>
				    					Adres: ".$praktijk_adres." <br/>
				    					Postcode: ".$praktijk_postcode." <br/>
				    					Stad: ".$praktijk_city." <br/>
				    					Telefoon:  ".$praktijk_telefoon." ";
				
			} else {
				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>laboratoriumscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					 ";
			}

		try {
				    //Server settings
				    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'mail.antagonist.nl';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'noreply@bvobaarmoederhals.nl';                 // SMTP username
				    $mail->Password = '0Myjmk';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `tls` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->ClearAllRecipients();
            
				    $mail->setFrom('info@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
				    $mail->addAddress('ruud@commpanionz.org');
				    $mail->addAddress('suzanne@2mc.nl');
				    $mail->addAddress('toon@2mc.nl');
				    $mail->addAddress('barcodes@tswarteschaap.nl');
				    $mail->addReplyTo('info@bvobaarmoederhals.nl', 'Information');
				    // $mail->addCC('cc@example.com');
				    // $mail->addBCC('bcc@example.com');

				    //Attachments
				    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->Subject = 'Er zijn monsters geconstateerd waarbij de laboratoriumscan mist';
				    $mail->Body    = $mailbody;
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->AltBody = 'Er zijn monsters geconstateerd waarbij de laboratoriumscan mist.';
				    $mail->send();
				} 
				    catch (Exception $e) {
				}

		}
	} 

	
	// ===============================================
				// EINDE LABCHECK
	// ===============================================












	// ================================================================================
	// NOTEXIST ARRAY CHECK -> RESULTATEN DIE GEEN KOERIERCHECK OF LABCHECK HEBBEN
	// ================================================================================
	$count_notexist = count($array_notexist);
	if($count_notexist >= 1){
		foreach($array_notexist as $value){

			$bar_code = $value;
			$results_barcode = $db->query("SELECT monster_scans.praktijk_id, monster_scans.route_id, monster_scans.bar_code FROM monster_scans WHERE bar_code = '$bar_code'");
			$row_barcode = $results_barcode->fetchAll(PDO::FETCH_ASSOC);
			$route_id = $row_barcode[0][route_id];
			$praktijk_id = $row_barcode[0][praktijk_id];

			// RESULTATEN VOOR DE ROUTE
			$results_route = $db->query("SELECT * FROM routes WHERE route_id = '$route_id'");
			$row_route = $results_route->fetchAll(PDO::FETCH_ASSOC);
			$route_name = $row_route[0][name];
			$route_region = $row_route[0][region_id];
			$route_vaste_dag = $row_route[0][vaste_dag];

			// RESULTATEN VOOR DE REGION (NAAM)
			$results_region = $db->query("SELECT regions.id, regions.region FROM regions WHERE id = '$route_region'");
			$row_region = $results_region->fetchAll(PDO::FETCH_ASSOC);
			$region = $row_region[0][region];

			// RESULTATEN VOOR DE BETREFFENDE PRAKTIJK
			$results_praktijk = $db->query("SELECT users.identity, users.email, users.address, users.postcode, users.city, users.telephone FROM users WHERE user_id = '$praktijk_id' ");
			$row_count_praktijk = $results_praktijk->rowCount();

			if($row_count_praktijk > 0){

				$row_praktijk = $results_praktijk->fetchAll(PDO::FETCH_ASSOC);
				$praktijk_name = $row_praktijk[0][identity];
				$praktijk_email = $row_praktijk[0][email];
				$praktijk_adres = $row_praktijk[0][address];
				$praktijk_postcode = $row_praktijk[0][postcode];
				$praktijk_city = $row_praktijk[0][city];
				$praktijk_telefoon = $row_praktijk[0][telephone];

				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>laboratoriumscan</b> en <b>koerierscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					<b>Praktijk:</b> <br/>
				    					Naam: ".$praktijk_name." <br/>
				    					Email: ".$praktijk_email." <br/>
				    					Adres: ".$praktijk_adres." <br/>
				    					Postcode: ".$praktijk_postcode." <br/>
				    					Stad: ".$praktijk_city." <br/>
				    					Telefoon:  ".$praktijk_telefoon." ";
				
			} else {
				$mailbody = "Beste beheerder, <br/><br/>

				    					Er zijn monsters geconstateerd waarbij de <b>laboratoriumscan</b> en <b>koerierscan</b> mist. Het betreft de volgende monster: <br/><br/>

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					 ";
			}

		try {
				    //Server settings
				    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'mail.antagonist.nl';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'noreply@bvobaarmoederhals.nl';                 // SMTP username
				    $mail->Password = '0Myjmk';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `tls` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->ClearAllRecipients();
				    $mail->setFrom('info@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
				    $mail->addAddress('ruud@commpanionz.org');
				    $mail->addAddress('suzanne@2mc.nl');
				    $mail->addAddress('toon@2mc.nl');
				    $mail->addAddress('barcodes@tswarteschaap.nl');
				    $mail->addReplyTo('info@bvobaarmoederhals.nl', 'Information');
				    // $mail->addCC('cc@example.com');
				    // $mail->addBCC('bcc@example.com');

				    //Attachments
				    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->Subject = 'Er zijn monsters geconstateerd waarbij de laboratoriumscan en koerierscan mist';
				    $mail->Body    = $mailbody;
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->AltBody = 'Er zijn monsters geconstateerd waarbij de laboratoriumscan en koerierscan mist.';
				    $mail->send();
				} 
				    catch (Exception $e) {
				}

		}
	} 
	// ===============================================
				// EINDE NOTEXIST CHECK
	// ===============================================






			    					













	// ================================================================================================================
	// CONTROLE OF ER IN DE TOTALITEIT MONSTERS MISSEN -> TOTALE LIJST DATABASE ENTRIES NAAST LIJST SCANSESSIE LEGGEN 
	// ================================================================================================================
	$count_monstermissing = count($array_monstermissing);
	if($count_monstermissing >= 1){

		$mailbody = "Beste beheerder, <br/><br/>

					Er zijn <b>missende monsters</b> geconstateerd. Het betreft de volgende monster: <br/><br/>";


		foreach($array_monstermissing as $value){

			$bar_code = $value;
			$results_barcode = $db->query("SELECT monster_scans.praktijk_id, monster_scans.route_id, monster_scans.bar_code, monster_scans.sessieKenmerk FROM monster_scans WHERE bar_code = '$bar_code'");
			$row_barcode = $results_barcode->fetchAll(PDO::FETCH_ASSOC);
			$route_id = $row_barcode[0][route_id];
			$praktijk_id = $row_barcode[0][praktijk_id];
			$monstermissing_sessiekenmerk = $row_barcode[0][sessieKenmerk];

			// RESULTATEN VOOR DE ROUTE
			$results_route = $db->query("SELECT * FROM routes WHERE route_id = '$route_id'");
			$row_route = $results_route->fetchAll(PDO::FETCH_ASSOC);
			$route_name = $row_route[0][name];
			$route_region = $row_route[0][region_id];
			$route_vaste_dag = $row_route[0][vaste_dag];

			// RESULTATEN VOOR DE REGION (NAAM)
			$results_region = $db->query("SELECT regions.id, regions.region FROM regions WHERE id = '$route_region'");
			$row_region = $results_region->fetchAll(PDO::FETCH_ASSOC);
			$region = $row_region[0][region];

			// RESULTATEN VOOR DE BETREFFENDE PRAKTIJK
			$results_praktijk = $db->query("SELECT users.identity, users.email, users.address, users.postcode, users.city, users.telephone FROM users WHERE user_id = '$praktijk_id' ");
			$row_count_praktijk = $results_praktijk->rowCount();

			if($row_count_praktijk > 0){

				$row_praktijk = $results_praktijk->fetchAll(PDO::FETCH_ASSOC);
				$praktijk_name = $row_praktijk[0][identity];
				$praktijk_email = $row_praktijk[0][email];
				$praktijk_adres = $row_praktijk[0][address];
				$praktijk_postcode = $row_praktijk[0][postcode];
				$praktijk_city = $row_praktijk[0][city];
				$praktijk_telefoon = $row_praktijk[0][telephone];

				$mailbody .= "

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Sessiekenmerk</b>: <br/>
				    					".$monstermissing_sessiekenmerk." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>

				    					<b>Praktijk:</b> <br/>
				    					Naam: ".$praktijk_name." <br/>
				    					Email: ".$praktijk_email." <br/>
				    					Adres: ".$praktijk_adres." <br/>
				    					Postcode: ".$praktijk_postcode." <br/>
				    					Stad: ".$praktijk_city." <br/>
				    					Telefoon:  ".$praktijk_telefoon." <br/><br/>
				    					<hr><br/><br/>";
				
			} else {
				$mailbody .= "

				    					<b>Barcode</b>: <br/>
				    					".$bar_code." <br/><br/>

				    					<b>Sessiekenmerk</b>: <br/>
				    					".$monstermissing_sessiekenmerk." <br/><br/>

				    					<b>Route:</b> <br/>
				    					Routenaam: ".$route_name." <br/>
				    					Route regio: ".$region." <br/>
				    					Route vaste dag: ".$route_vaste_dag." <br/><br/>
				    					<hr><br/><br/>

				    					 ";
			}

		}



		try {
				    //Server settings
				    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'mail.antagonist.nl';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'noreply@bvobaarmoederhals.nl';                 // SMTP username
				    $mail->Password = '0Myjmk';                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `tls` also accepted
				    $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->ClearAllRecipients();
				    $mail->setFrom('info@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
				    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
				    $mail->addAddress('ruud@commpanionz.org');
				    $mail->addAddress('suzanne@2mc.nl');
				    $mail->addAddress('toon@2mc.nl');
				    $mail->addAddress('martijn@tswarteschaap.nl');
				    $mail->addAddress('barcodes@tswarteschaap.nl');
				    $mail->addReplyTo('info@bvobaarmoederhals.nl', 'Information');
				    // $mail->addCC('cc@example.com');
				    // $mail->addBCC('bcc@example.com');

				    //Attachments
				    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->Subject = 'Er zijn '.$count_monstermissing.' missende monsters geconstateerd';
				    $mail->Body    = $mailbody;
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->AltBody = 'Er zijn '.$count_monstermissing.' missende monsters geconstateerd.';
				    $mail->send();
				} 
				    catch (Exception $e) {
				}

	} 
	// ===============================================
				// EINDE MATCH CHECK
	// ===============================================



			    					
// START SUCCESMAIL NAAR BEHEERDERS EN LAB
  
  // MAKES SURE MULTIPLE EXPORTS CAN BE CREATED SIMULTANEOUSLY
  $counter = 0;
  $filename_template = "export_";
  $filename = $filename_template . $counter;

  while(file_exists("files/".$filename.".csv")) {
    $counter++;
    $filename = $filename_template . $counter;
  }


  // FILE IS OPENED/CREATED
  $file = fopen("files/" . $filename . ".csv","w");

  // File contents are added
  foreach($succesmail_array as $line){
    fputcsv($file, $line, ";");
  }

  fclose($file);
  // FILE IS CLOSED


  $aantal_succesvol = sizeof($array_geslaagd);

  try {
    //Server settings
    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.antagonist.nl';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@bvobaarmoederhals.nl';                 // SMTP username
    $mail->Password = '0Myjmk';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `tls` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->ClearAllRecipients();
    $mail->setFrom('info@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
    $mail->addAddress('ruud@commpanionz.org');
    $mail->addAddress('suzanne@2mc.nl');
    $mail->addAddress('toon@2mc.nl');
    $mail->addAddress('barcodes@tswarteschaap.nl');

    // SUCCESMAIL NAAR LAB
    // $mail->addAddress($succesmail_lab_mail);

    $mail->addReplyTo('info@bvobaarmoederhals.nl', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    $mail->addAttachment('files/'. $filename . '.csv', 'Overzicht gescande barcodes.csv');    // Optional name

    //Content

    $title = 'Er zijn ' . $aantal_succesvol . ' monsters succesvol gescand';

    if($aantal_succesvol == 1){
      $title = 'Er is 1 monster succesvol gescand';
    }



    $mail->Subject = $title;
    $message = 'Geachte heer/mevrouw, <br /><br />
                Er zijn zojuist ' . $aantal_succesvol . ' monsters succesvol tegengescand. In het bijgevoegde bestand kunt u zien om welke monsters het gaat. <br /><br />

                Met vriendelijke groet, <br />
                Team Commpanionz';

    $mail->Body    = '<!DOCTYPE html>
					<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
					<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
					<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
					<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
					<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

					<title>Commpanionz mailer</title>
					<meta name="description" content="Commpanionz mailer">
					<meta name="keywords" content="">

					<!-- Mobile viewport -->
					<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

					<link rel="shortcut icon" href="images/favicon.ico"  type="image/x-icon">

					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/normalize.css">
					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/js/flexslider/flexslider.css">
					<link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/basic-style.css">

					<!-- end CSS-->
					    
					<!-- JS-->
					<script src="js/libs/modernizr-2.6.2.min.js"></script>
					<!-- end JS-->
					</style>
					</head>

					<body id="home">
					  
					<!-- header area -->
					    <header class="wrapper clearfix">
							       
					        <div id="banner">        
					        	<div id="logo"><img src="http://www.bvobaarmoederhals.nl/img/logo.png" alt="logo"></div> 
					        </div>
					  
					    </header><!-- end header -->
					 
					<!-- main content area -->   
					<div id="main" class="wrapper">
					    
					<!-- content area -->    
						<section id="content" class="wide-content">
					      <div class="row">	
					        
					            <p>'.$message.'</p>
					        
						  </div><!-- end row -->
						</section><!-- end content area -->   
					<!-- footer area -->    
					<footer>
						<div id="colophon" class="wrapper clearfix">
					    	Contactgegevens: Commpanionz - Zomerkade 102 - 1273 SP - Huizen - The Netherlands - Telefoon: +31 341 495 254 - Website: www.commpanionz.org - Mail: info@commpanionz.org
					    </div>
					</footer><!-- #end footer area --> 
					<!-- jQuery -->
					<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
					<script>window.jQuery || document.write("<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>")</script>

					<script defer src="js/flexslider/jquery.flexslider-min.js"></script>

					<!-- fire ups - read this file!  -->   
					<script src="js/main.js"></script>

					</body>
					</html>';
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->AltBody = $title;
    $mail->send();
  } 
    catch (Exception $e) {
  }

  $delete_error = '';
  if (!unlink("files/" . $filename . ".csv")){
    $delete_error = "Error deleting " . $filename;
  }else{
    $delete_error = "Deleted " . $filename;
  }




// =================================================
		// RESPONSE BERICHT EINDE MATCHING
// =================================================

$responsebericht = '';

	// =========== RESPONSE NAAR GEBRUIKER MET UITSLAG SCANSESSIE ===============

	if ($count_monstermissing == 0 && $count_labfaal == 0 && $count_koerierfaal == 0 && $count_notexist == 0){
		$responsebericht .= 'De scansessie is succesvol uitgevoerd, en er zijn géén missende monsters geconstateerd!';
	}

	if ($count_monstermissing == 1){
		$responsebericht .= 'Er ontbrak '.$count_monstermissing.' monster gedurende de scansessie.';
	} elseif ($count_monstermissing > 1){
		$responsebericht .= 'Er ontbraken '.$count_monstermissing.' monsters gedurende de scansessie.';
	}

	if ($count_labfaal == 1){
		$responsebericht .= ' Bij '.$count_labfaal.' monster ontbrak de scan voor het laboratorium.';
	} elseif ($count_labfaal > 1){
		$responsebericht .= ' Bij '.$count_labfaal.' monsters ontbrak de scan voor het laboratorium.';
	}

	if ($count_notexist == 1){
		$responsebericht .= ' Bij '.$count_notexist.' monster ontbrak zowel de scan voor de koerier als de scan van het laboratorium.';
	} elseif ($count_notexist > 1){
		$responsebericht .= ' Bij '.$count_notexist.' monsters ontbrak zowel de scan voor de koerier als de scan van het laboratorium.';
	}

	if ($count_koerierfaal == 1){
		$responsebericht .= ' Bij '.$count_koerierfaal.' monster ontbrak de scan van de koerier.';
	} elseif ($count_koerierfaal > 1){
		$responsebericht .= ' Bij '.$count_koerierfaal.' monsters ontbrak de scan van de koerier.';
	}

	if ($count_monstermissing > 0 || $count_labfaal > 0 || $count_koerierfaal > 0 || $count_notexist > 0 ){
		$responsebericht .= " De beheerders zijn per mail ingelicht over de missende monsters. Bel direct naar +31 (0)6 12 99 76 02 om dit kort te sluiten.";
	}


echo '{"response": "U heeft de sessie beëindigd. '.$responsebericht.'"}';


}

?>

