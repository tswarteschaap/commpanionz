<?php 
/* 
COMMPANIONZ APP, http://bvobaarmoederhals.nl/
//////////////////////////////////////////////////////////////////
////////////////// GET BARCODE SCANNED BY LAB ///////////////////
///////BY MARTIJN WENNEKES, 'T SWARTE SCHAAP, HEERLEN, NL////////
////////////////////////20-1-2017//////////////////////////////
//////////////////////////////////////////////////////////////
*/
/*
echo "hallo";
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
require_once 'includes/init.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


// $results = 	$db->query("SELECT users.identity, users.user_id, users.postcode, users.address, users.city FROM users WHERE users.region_id = '2' and users.role_id = '2' ORDER BY users.identity ASC");

$mail = new PHPMailer(true); 

$date = date('Ymd');


if (isset($_POST['sessiekenmerk'])){
	$sessiekenmerk = $_POST['sessiekenmerk'];

	if (isset($_POST['scan'])) {
		$scanned_barcode = $_POST['scan'];

		$results = $db->query("SELECT monster_scans.id, monster_scans.route_id, monster_scans.praktijk_id, monster_scans.koerier_scan, monster_scans.bar_code, monster_scans.sessieKenmerk FROM monster_scans WHERE monster_scans.bar_code = '$scanned_barcode' ");


		$num_rows = $results->rowCount();
		$row = $results->fetchAll(PDO::FETCH_ASSOC);
		// $row_id = $row[0][id];
		$row_barcode = $row[0][bar_code];
		$row_praktijk_id = $row[0][praktijk_id];
		$row_route_id = $row[0][route_id];
		$row_sessie_kenmerk = $row[0][sessieKenmerk];

		// echo '{"response:" "'.$num_rows.'"}';

		

		if ($num_rows !== 0){
			//succes: er is een resultaat (een match met de database).

				if($row_sessie_kenmerk != $sessiekenmerk){


					// echo '{"response": "$bestaat"}'; 
					echo '{"response": {"bestaat": "'.$row_sessie_kenmerk.'",
										"bestaat_barcode": "'.$row_barcode.'"
										}}';
					die();
				}
				elseif($row[0][koerier_scan] == '1'){
					$db->query("UPDATE monster_scans SET laboratorium_scan = '1', laboratorium_scan_datum = '$date' WHERE bar_code = '$row_barcode'");
					echo '{"response": {
						    "route_id": "'.$row_route_id.'",
						    "praktijk_id": "'.$row_praktijk_id.'",
						    "bar_code": "'.$row_barcode.'",
						    "sessie_kenmerk": "'.$row_sessie_kenmerk.'"
						 	}
						}';	 

				}    
				elseif($row[0][koerier_scan] != '1'){
					echo '{"response": "onjuist"}';
				} 

		} else {

				//failure
				try {
			    //Server settings
			    $mail->IsSMTP(); // telling the class to use SMTP
				//$mail->Host       = "mail.yourdomain.com"; // SMTP server
				$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
														   // 1 = errors and messages
														   // 2 = messages only
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
				$mail->Host       = "mail.antagonist.nl";      // sets GMAIL as the SMTP server
				$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
				$mail->Username   = "noreply@bvobaarmoederhals.nl";  // GMAIL username
				$mail->Password   = "0Myjmk";            // GMAIL password

			    //Recipients
			    $mail->setFrom('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
			    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
			    $mail->addAddress('ruud@commpanionz.org');
			    $mail->addAddress('suzanne@2mc.nl');
			    $mail->addAddress('toon@2mc.nl');
			    $mail->addAddress('barcodes@tswarteschaap.nl');
			    $mail->addReplyTo('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
			    // $mail->addCC('cc@example.com');
			    // $mail->addBCC('bcc@example.com');

			    //Attachments
			    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    //Content
			    $mail->Subject = 'Een gescand monster komt niet voor in de database!';
			    $mail->Body    = "Beste beheerder, <br/><br/>

			    					Er is een monster geconstateerd dat niet in de <b>database</b> voorkomt. Het betreft het volgende monster: <br/><br/>

			    					<b>Barcode</b>: <br/>
			    					".$scanned_barcode." <br/><br/>

			    					<b>Sessiekenmerk</b>: <br/>
			    					".$sessiekenmerk."
			    					 ";
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
			    $mail->send();
			} 
			    catch (Exception $e) {
			}

			echo '{"response": "missend"}';
		}
	}
}


if (isset($_POST['nieuwesessie'])){
	$nieuwe_sessie = $_POST['nieuwesessie'];
	$bestaat_barcode = $_POST['bestaat_barcode'];

	$results_oudesessie = $db->query("SELECT sessieKenmerk FROM monster_scans WHERE bar_code = '$bestaat_barcode'");
	$row_oudesessie = $results_oudesessie->fetchAll(PDO::FETCH_ASSOC);
	$oude_sessiecode = $row_oudesessie[0][sessieKenmerk];

	$db->query("UPDATE monster_scans SET laboratorium_scan = '1', laboratorium_scan_datum = '$date' WHERE bar_code = '$bestaat_barcode'");

	$db->query("UPDATE monster_scans SET sessieKenmerk = '$nieuwe_sessie' WHERE bar_code = '$bestaat_barcode'");


	$results = $db->query("SELECT monster_scans.id, monster_scans.route_id, monster_scans.praktijk_id, monster_scans.koerier_scan, monster_scans.bar_code, monster_scans.sessieKenmerk FROM monster_scans WHERE monster_scans.bar_code = '$bestaat_barcode' ");
	$num_rows = $results->rowCount();
	$row = $results->fetchAll(PDO::FETCH_ASSOC);
	$row_barcode = $row[0][bar_code];
	$row_praktijk_id = $row[0][praktijk_id];
	$row_route_id = $row[0][route_id];
	$row_sessie_kenmerk = $row[0][sessieKenmerk];

	//SEND MAIL FOR CHANGE IN SESSIEBARCODE
		try {
	    //Server settings
	    $mail->IsSMTP(); // telling the class to use SMTP
		//$mail->Host       = "mail.yourdomain.com"; // SMTP server
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "mail.antagonist.nl";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = "noreply@bvobaarmoederhals.nl";  // GMAIL username
		$mail->Password   = "0Myjmk";            // GMAIL password

	    //Recipients
	    $mail->setFrom('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
	    $mail->addAddress('tanja@commpanionz.org');     // Add a recipient
	    $mail->addAddress('ruud@commpanionz.org');
	    $mail->addAddress('suzanne@2mc.nl');
	    $mail->addAddress('toon@2mc.nl');
	    // $mail->addAddress('martijn@tswarteschaap.nl');
	    $mail->addAddress('barcodes@tswarteschaap.nl');
	    $mail->addReplyTo('noreply@bvobaarmoederhals.nl', 'BVO Baarmoederhalskanker Mailer (Commpanionz)');
	    // $mail->addCC('cc@example.com');
	    // $mail->addBCC('bcc@example.com');

	    //Attachments
	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	    //Content
	    $mail->Subject = 'Er is een sessiekenmerk gewijzigd binnen de database dmv. de API';
	    $mail->Body    = "Beste beheerder, <br/><br/>

	    					Er is een <b>sessiekenmerk gewijzigd</b> door middel van de API binnen de database! Het betreft de volgende wijziging: <br/><br/>

	    					<b>Barcode</b>: <br/>
	    					".$bestaat_barcode." <br/><br/>

	    					<b>Nieuwe sessiekenmerk</b>: <br/>
	    					".$nieuwe_sessie." <br/><br/>

	    					<b>Oude sessiekenmerk</b>: <br/>
	    					".$oude_sessiecode." 
	    					 

	    					 ";
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	    $mail->send();
		} 
		    catch (Exception $e) {
		}




	//SEND RESPONSE
	echo '{"response": {
		    "route_id": "'.$row_route_id.'",
		    "praktijk_id": "'.$row_praktijk_id.'",
		    "bar_code": "'.$row_barcode.'",
		    "sessie_kenmerk": "'.$row_sessie_kenmerk.'"
		 	}
		}';	 

}

// echo "results -> ";
// print_r($results);
// echo "<br><br><br>";

// $num_rows = $results->rowCount();
// $row = $results->fetchAll(PDO::FETCH_ASSOC);
// /*
// echo "row -> ";
// print_r($row);
// echo "<br><br><br>";
// */
// foreach($row as $field) {
	
// 	$details[] = array(
// 		'huisartsenpraktijk' 	=>	$field['identity'],
// 		'adres' 				=>	utf8_encode($field['address']),
// 		'plaatsnaam' 			=>	$field['city'],
// 		'postcode' 				=>	$field['postcode'],
// 		'id'					=>  $field['user_id']
// 	);
// }
/*
echo "details -> ";
print_r($details);
echo "<br><br><br>";
*/
// echo json_encode($details);

?>
